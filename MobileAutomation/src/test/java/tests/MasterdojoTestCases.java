package tests;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import others.Constants;
import others.leftMenu;
import pages.*;
import testBase.TestBase;

import java.io.IOException;
import java.net.MalformedURLException;

public class MasterdojoTestCases extends TestBase {

    public ExtentHtmlReporter htmlReporter;
    public ExtentReports report;
    public ExtentTest mdTest;

    TestBase testBase = new TestBase();
    LoginPage login = new LoginPage();
    ReferralsPage referral = new ReferralsPage();
    leftMenu lm = new leftMenu();
    PaymentsPage mPayment = new PaymentsPage();
    FavouritePage fav = new FavouritePage();
    ProfilePage profile = new ProfilePage();
    TestimonialPage review = new TestimonialPage();
    EducatorPage edu = new EducatorPage();
    MyNetworkPage mnPage = new MyNetworkPage();
    CommunityPage cmnPage = new CommunityPage();
    FilterPage filter = new FilterPage();
    OfferingDetailsPage offering = new OfferingDetailsPage();
    MessagesPage msgPage = new MessagesPage();
    NotificationPage nPage = new NotificationPage();
    ContentConsumptionPage ccPage = new ContentConsumptionPage();
    ExplorePage ePage = new ExplorePage();

    AndroidDriver driver = (AndroidDriver) testBase.initializeChromeDriver();
    //String parent = driver.getWindowHandle();
    WebDriverWait wait = new WebDriverWait(driver, 90, 1000);

    public MasterdojoTestCases() throws MalformedURLException, InterruptedException {
    }

    @BeforeClass(alwaysRun = true)
    public void startReport() {
        htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") + "\\Reports\\AutomationReport_Android.html");
        System.out.println(System.getProperty("user.dir"));
        report = new ExtentReports();
        report.attachReporter(htmlReporter);
        report.setSystemInfo("Published By", "MVP1Ventures");
        report.setSystemInfo("Environment", "Test");
        report.setSystemInfo("Made By", "Kanak Bhuva");
        htmlReporter.config().setDocumentTitle("Masterdojo");
        htmlReporter.config().setReportName("Masterdojo Test Cases Report");
    }

    @Test(priority = 1, enabled = false, groups = {"Login"})
    public void signUp() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: SignUP");
        login.SignUp(driver, mdTest);
        mdTest.log(Status.INFO, "Sign Up successfully.");
        //lm.displayLeftMenu(driver, mdTest);
        //lm.logout(driver, mdTest);
        //mdTest.addScreenCaptureFromPath("E:\\Knk\\MasterdojoAutomation\\WebLearnerAutomation\\Screenshots\\Test.png","test");
    }

    @Test(priority = 2, enabled = true, groups = {"Login"})
    public void login() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Login");
        login.login1(driver, mdTest);
        //login.login2(driver, mdTest);
        mdTest.log(Status.INFO, "Login successfully.");
    }

    @Test(priority = 3, enabled = true, groups = {"Referral"})
    public void sendReferrals() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Send business referral from educator details screen via SMS");
        referral.sendBusinessReferralFromEducatorDetailsPage_SMS(driver, mdTest);
        mdTest.log(Status.INFO, "Referral sent successfully.");
    }

    @Test(priority = 4, enabled = true, groups = {"Referral"})
    public void sendEmailReferrals() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Send business referral from educator details screen via Email");
        referral.sendBusinessReferralFromEducatorDetailsPage_Mail(driver, mdTest);
        mdTest.log(Status.INFO, "Referral sent successfully.");
    }

    @Test(priority = 5, enabled = true, groups = {"Referral"})
    public void sendReferralsFromExpPage() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Send business referral from explore page via SMS");
        referral.sendBusinessReferralFromExploreDetailsPage_SMS(driver, mdTest);
        mdTest.log(Status.INFO, "Referral sent successfully.");
    }

    @Test(priority = 6, enabled = true, groups = {"Referral"})
    public void sendEmailReferralsFromExpPage() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Send business referral from explore page via Email");
        referral.sendBusinessReferralFromExploreDetailsPage_Mail(driver, mdTest);
        mdTest.log(Status.INFO, "Referral sent successfully.");
    }

    @Test(priority = 7, enabled = true, groups = {"Referral"})
    public void sendReferralsForOfferingEP() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Send offering referral from educator details screen via SMS");
        referral.sendOfferingReferralFromEducatorDetailsPage_SMS(driver, mdTest);
        mdTest.log(Status.INFO, "Referral sent successfully.");
    }

    @Test(priority = 8, enabled = true, groups = {"Referral"})
    public void sendEmailReferralsForOfferingEP() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Send offering referral from educator details screen via Email");
        referral.sendOfferingReferralFromEducatorDetailsPage_Mail(driver, mdTest);
        mdTest.log(Status.INFO, "Referral sent successfully.");
    }

    @Test(priority = 9, enabled = true, groups = {"Referral"})
    public void sendReferralsForOffering() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Send offering referral from offering details screen via SMS");
        referral.sendOfferingReferralFromOfferingDetailsPage_SMS(driver, mdTest);
        mdTest.log(Status.INFO, "Referral sent successfully.");
    }

    @Test(priority = 10, enabled = true, groups = {"Referral"})
    public void sendEmailReferralsForOffering() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Send offering referral from offering details screen via Email");
        referral.sendOfferingReferralFromOfferingDetailsPage_Mail(driver, mdTest);
        mdTest.log(Status.INFO, "Referral sent successfully.");
    }

    @Test(priority = 11, enabled = true, groups = {"Referral"})
    public void sendReferralsForOfferingM() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Send offering referral from more offering details screen via SMS");
        referral.sendOfferingReferralFromOfferingDetailsMorePage_SMS(driver, mdTest);
        mdTest.log(Status.INFO, "Referral sent successfully.");
    }

    @Test(priority = 12, enabled = true, groups = {"Referral"})
    public void sendEmailReferralsForOfferingM() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Send offering referral from more offering details screen via Email");
        referral.sendOfferingReferralFromOfferingDetailsMorePage_Mail(driver, mdTest);
        mdTest.log(Status.INFO, "Referral sent successfully.");
    }

    @Test(priority = 13, enabled = true, groups = {"Referral"})
    public void sendReferralsForOfferingFromUpcomingSection() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Send offering referral from 'Upcoming section' via SMS");
        referral.sendOfferingReferralFromUpcomingSection_SMS(driver, mdTest);
        mdTest.log(Status.INFO, "Referral sent successfully.");
    }

    @Test(priority = 14, enabled = true, groups = {"Referral"})
    public void sendEmailReferralsForOfferingFromUpcomingSection() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Send offering referral from 'Upcoming section' via Email");
        referral.sendOfferingReferralFromUpcomingSectionPage_Mail(driver, mdTest);
        mdTest.log(Status.INFO, "Referral sent successfully.");
    }

    @Test(priority = 15, enabled = true, groups = {"Referral"})
    public void reSendBusinessReferral() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Resend business referral");
        lm.displayLeftMenu(driver, mdTest);
        lm.displayReferralHistory(driver, mdTest);
        referral.reSendBusinessReferral(driver, mdTest);
        mdTest.log(Status.INFO, "Referral sent successfully.");
    }

    @Test(priority = 16, enabled = true, groups = {"Referral"})
    public void reSendOfferingReferral() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Resend offering referral");
        lm.displayLeftMenu(driver, mdTest);
        lm.displayReferralHistory(driver, mdTest);
        referral.reSendOfferingReferral(driver, mdTest);
        mdTest.log(Status.INFO, "Referral sent successfully.");
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @Test(priority = 13, enabled = true, groups = {"Profile"})
    public void addProfilePicture() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Add profile picture");
        //lm.displayLeftMenu(driver, mdTest);
        //lm.displayProfile(driver, mdTest);
        testBase.openProfile(driver, mdTest);
        profile.addUpdateProfilePic(driver, mdTest);
        mdTest.log(Status.INFO, "Profile picture added successfully.");
    }

    @Test(priority = 14, enabled = true, groups = {"Profile"})
    public void addProfileDetails() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Add profile details");
        //lm.displayLeftMenu(driver, mdTest);
        //lm.displayProfile(driver, mdTest);
        //testBase.openProfile(driver, mdTest);
        profile.addUpdatePersonalDetails(driver, mdTest);
        profile.addUpdateBusinessDetails(driver, mdTest);
        mdTest.log(Status.INFO, "Profile added successfully.");
    }

    @Test(priority = 15, enabled = true, groups = {"Profile"})
    public void addSkill() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Add skill");
        //lm.displayLeftMenu(driver, mdTest);
        //lm.displayProfile(driver, mdTest);
        profile.addSkill(driver, mdTest);
        mdTest.log(Status.INFO, "Skill added successfully.");
    }

    @Test(priority = 16, enabled = true, groups = {"Profile"})
    public void addSuggestedSkill() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Add skill from suggested skill");
        //lm.displayLeftMenu(driver, mdTest);
        //lm.displayProfile(driver, mdTest);
        profile.addSuggestedSkill(driver, mdTest);
        mdTest.log(Status.INFO, "Skill added successfully.");
    }

    @Test(priority = 17, enabled = true, groups = {"Profile"})
    public void removeSkill() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Remove skill");
        //lm.displayLeftMenu(driver, mdTest);
        //lm.displayProfile(driver, mdTest);
        profile.removeSkill(driver, mdTest);
        testBase.goToHomeInAndroid(driver, mdTest);
        mdTest.log(Status.INFO, "Skill removed successfully.");
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @Test(priority = 18, enabled = true, groups = {"Offering"})
    public void registerInOnlineOngoingFreeOffering() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Register in online ongoing free offering");
        offering.registerInOnlineOngoingFreeOffering(driver, mdTest);
        mdTest.log(Status.INFO, "Registered successfully.");
    }

    @Test(priority = 19, enabled = true, groups = {"Offering"})
    public void registerInOnlineSpecificPeriodFreeOffering() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Register in online specific period free offering");
        offering.registerInOnlineSpecificPeriodFreeOffering(driver, mdTest);
        mdTest.log(Status.INFO, "Registered successfully.");
    }

    @Test(priority = 20, enabled = true, groups = {"Offering"})
    public void registerInOfflineOneTimeFreeOffering() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Register in offline one time free offering");
        offering.registerInOfflineOneTimeFreeOffering(driver, mdTest);
        mdTest.log(Status.INFO, "Registered successfully.");
    }

    @Test(priority = 21, enabled = true, groups = {"Offering"})
    public void registerInOfflineRecurringFreeOffering() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Register in offline recurring free offering");
        offering.registerInOfflineRecurringFreeOffering(driver, mdTest);
        mdTest.log(Status.INFO, "Registered successfully.");
    }

    @Test(priority = 22, enabled = true, groups = {"Offering"})
    public void registerInOnlineOngoingPaidOffering() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Register in online ongoing paid offering");
        offering.registerInOnlineOngoingPaidOffering(driver, mdTest);
        mdTest.log(Status.INFO, "Registered successfully.");
    }

    @Test(priority = 23, enabled = true, groups = {"Offering"})
    public void registerInOnlineSpecificPeriodPaidOffering() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Register in online specific period paid offering");
        offering.registerInOnlineSpecificPeriodPaidOffering(driver, mdTest);
        mdTest.log(Status.INFO, "Registered successfully.");
    }

    @Test(priority = 24, enabled = true, groups = {"Offering"})
    public void registerInOnlineOngoingSubscriptionOffering() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Register in online ongoing subscription offering");
        offering.registerInOnlineOngoingSubscriptionOffering(driver, mdTest);
        mdTest.log(Status.INFO, "Registered successfully.");
    }

    @Test(priority = 25, enabled = true, groups = {"Offering"})
    public void registerInOnlineSpecificPeriodSubscriptionOffering() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Register in online specific period subscription offering");
        offering.registerInOnlineSpecificPeriodSubscriptionOffering(driver, mdTest);
        mdTest.log(Status.INFO, "Registered successfully.");
    }

    @Test(priority = 26, enabled = true, groups = {"Offering"})
    public void registerInOfflineOneTimePaidOffering() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Register in offline one time paid offering");
        offering.registerInOfflineOneTimePaidOffering(driver, mdTest);
        mdTest.log(Status.INFO, "Registered successfully.");
    }

    @Test(priority = 27, enabled = true, groups = {"Offering"})
    public void registerInOfflineRecurringPaidOffering() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Register in offline recurring paid offering");
        offering.registerInOfflineRecurringPaidOffering(driver, mdTest);
        mdTest.log(Status.INFO, "Registered successfully.");
    }

    @Test(priority = 28, enabled = true, groups = {"Offering"})
    public void logout() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Logout");
        lm.displayLeftMenu(driver, mdTest);
        lm.logout(driver, mdTest);
        mdTest.log(Status.INFO, "Logout successfully.");
    }

    @Test(priority = 29, enabled = true, groups = {"Offering"})
    public void login2() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Login");
        login.login2(driver, mdTest);
        mdTest.log(Status.INFO, "Login successfully.");
    }

    @Test(priority = 30, enabled = true, groups = {"Offering"})
    public void registerInOnlineOngoingFreeOffering_UpcomingSection() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Register in online ongoing free offering from upcoming section");
        offering.registerInOnlineOngoingFreeOffering_UpcomingSection(driver, mdTest);
        mdTest.log(Status.INFO, "Registered successfully.");
    }

    @Test(priority = 31, enabled = true, groups = {"Offering"})
    public void registerInOnlineSpecificPeriodFreeOffering_UpcomingSection() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Register in online specific period free offering from upcoming section");
        offering.registerInOnlineSpecificPeriodFreeOffering_UpcomingSection(driver, mdTest);
        mdTest.log(Status.INFO, "Registered successfully.");
    }

    @Test(priority = 32, enabled = true, groups = {"Offering"})
    public void registerInOfflineOneTimeFreeOffering_UpcomingSection() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Register in offline one time free offering from upcoming section");
        offering.registerInOfflineOneTimeFreeOffering_UpcomingSection(driver, mdTest);
        mdTest.log(Status.INFO, "Registered successfully.");
    }

    @Test(priority = 33, enabled = true, groups = {"Offering"})
    public void registerInOfflineRecurringFreeOffering_UpcomingSection() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Register in offline recurring free offering from upcoming section");
        offering.registerInOfflineRecurringFreeOffering_UpcomingSection(driver, mdTest);
        mdTest.log(Status.INFO, "Registered successfully.");
    }

    @Test(priority = 34, enabled = true, groups = {"Offering"})
    public void registerInOnlineOngoingPaidOffering_UpcomingSection() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Register in online ongoing paid offering from upcoming section");
        offering.registerInOnlineOngoingPaidOffering_UpcomingSection(driver, mdTest);
        mdTest.log(Status.INFO, "Registered successfully.");
    }

    @Test(priority = 35, enabled = true, groups = {"Offering"})
    public void registerInOnlineSpecificPeriodPaidOffering_UpcomingSection() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Register in online specific period paid offering from upcoming section");
        offering.registerInOnlineSpecificPeriodPaidOffering_UpcomingSection(driver, mdTest);
        mdTest.log(Status.INFO, "Registered successfully.");
    }

    @Test(priority = 36, enabled = true, groups = {"Offering"})
    public void registerInOnlineOngoingSubscriptionOffering_UpcomingSection() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Register in online ongoing subscription offering from upcoming section");
        offering.registerInOnlineOngoingSubscriptionOffering_UpcomingSection(driver, mdTest);
        mdTest.log(Status.INFO, "Registered successfully.");
    }

    @Test(priority = 37, enabled = true, groups = {"Offering"})
    public void registerInOnlineSpecificPeriodSubscriptionOffering_UpcomingSection() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Register in online specific period subscription offering from upcoming section");
        offering.registerInOnlineSpecificPeriodSubscriptionOffering_UpcomingSection(driver, mdTest);
        mdTest.log(Status.INFO, "Registered successfully.");
    }

    @Test(priority = 38, enabled = true, groups = {"Offering"})
    public void registerInOfflineOneTimePaidOffering_UpcomingSection() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Register in offline one time paid offering from upcoming section");
        offering.registerInOfflineOneTimePaidOffering_UpcomingSection(driver, mdTest);
        mdTest.log(Status.INFO, "Registered successfully.");
    }

    @Test(priority = 39, enabled = true, groups = {"Offering"})
    public void registerInOfflineRecurringPaidOffering_UpcomingSection() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Register in offline recurring paid offering from upcoming section");
        offering.registerInOfflineRecurringPaidOffering_UpcomingSection(driver, mdTest);
        mdTest.log(Status.INFO, "Registered successfully.");
    }

    @Test(priority = 40, enabled = true, groups = {"Offering"})
    public void addToCalendar_Offering() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Add to calendar");
        offering.addToCalendar_Offering(driver, mdTest);
        mdTest.log(Status.INFO, "Offering added to calendar successfully.");
    }

    @Test(priority = 41, enabled = true, groups = {"Offering"})
    public void addToCalendar_Offering_MoreDetailsPage() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Add to calendar from offering more details screen");
        offering.addToCalendar_Offering_MoreDetailsPage(driver, mdTest);
        mdTest.log(Status.INFO, "Offering added to calendar successfully.");
    }

    @Test(priority = 41, enabled = true, groups = {"PromoCode"})
    public void registerInOfflineOneTimePaidOffering_PromoCode() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Register in offline one time paid offering by applying promo code");
        offering.registerInOfflineOneTimePaidOffering_PromoCode(driver, mdTest);
        mdTest.log(Status.INFO, "Registered successfully.");
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @Test(priority = 42, enabled = true, groups = {"Payment"})
    public void addCard() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Add Card");
        lm.displayLeftMenu(driver, mdTest);
        lm.displayPayment(driver, mdTest);
        mPayment.addCard(driver, mdTest);
        mdTest.log(Status.INFO, "Card added successfully.");
    }

    @Test(priority = 43, enabled = true, groups = {"Payment"})
    public void changeCard() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Change Card");
        mPayment.changeCard(driver, mdTest);
        mdTest.log(Status.INFO, "Card changed successfully.");
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @Test(priority = 36, enabled = true, groups = {"Community"})
    public void sendConnectionRequestCommunity() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Send connection request from community screen");
        cmnPage.sendConnectionRequest(driver, mdTest);
        mdTest.log(Status.INFO, "Connection request sent successfully");
    }

    @Test(priority = 37, enabled = true, groups = {"Community"})
    public void sendConnectionRequestAgainCommunity() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Send connection request from community screen");
        cmnPage.sendConnectionRequestAgain(driver, mdTest);
        testBase.backFromEducatorScreen(driver, mdTest);
        testBase.backFromEducatorScreen(driver, mdTest);
        waitUntilAndroidElementVisibleByID(driver, Constants.btnHomeTB);
        mdTest.log(Status.INFO, "Verified: Request already sent");
    }

    @Test(priority = 38, enabled = true, groups = {"Community"})
    public void logout2() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Logout");
        lm.displayLeftMenu(driver, mdTest);
        lm.logout(driver, mdTest);
        mdTest.log(Status.INFO, "Logout successfully.");
    }

    @Test(priority = 39, enabled = true, groups = {"Community"})
    public void login3() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Login");
        login.login2(driver, mdTest);
        mdTest.log(Status.INFO, "Login successfully.");
    }

    @Test(priority = 40, enabled = true, groups = {"Community"})
    public void sendConnectionRequestCommunity_ReceivedRequest() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Send connection request from community screen");
        cmnPage.sendConnectionRequest_ReceivedRequest(driver, mdTest);
        testBase.backFromEducatorScreen(driver, mdTest);
        testBase.backFromEducatorScreen(driver, mdTest);
        waitUntilAndroidElementVisibleByID(driver, Constants.btnHomeTB);
        mdTest.log(Status.INFO, "Verified: Request already received");
    }

    @Test(priority = 41, enabled = true, groups = {"Community"})
    public void deleteConnectionRequest() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Delete connection request");
        lm.displayLeftMenu(driver, mdTest);
        lm.displayMessages(driver, mdTest);
        msgPage.deleteConnectionRequest(driver, mdTest);
        mdTest.log(Status.INFO, "Connection request deleted successfully.");
    }

    @Test(priority = 42, enabled = true, groups = {"Community"})
    public void logout3() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Logout");
        lm.displayLeftMenu(driver, mdTest);
        lm.logout(driver, mdTest);
        mdTest.log(Status.INFO, "Logout successfully.");
    }

    @Test(priority = 43, enabled = true, groups = {"Community"})
    public void login4() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Login");
        login.login1(driver, mdTest);
        mdTest.log(Status.INFO, "Login successfully.");
    }

    @Test(priority = 44, enabled = true, groups = {"Community"})
    public void sendConnectionRequestCommunity2() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Send connection request from community screen");
        cmnPage.sendConnectionRequest(driver, mdTest);
        testBase.backFromEducatorScreen(driver, mdTest);
        testBase.backFromEducatorScreen(driver, mdTest);
        waitUntilAndroidElementVisibleByID(driver, Constants.btnHomeTB);
        mdTest.log(Status.INFO, "Connection request sent successfully");
    }

    @Test(priority = 45, enabled = true, groups = {"Community"})
    public void logout4() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Logout");
        lm.displayLeftMenu(driver, mdTest);
        lm.logout(driver, mdTest);
        mdTest.log(Status.INFO, "Logout successfully.");
    }

    @Test(priority = 46, enabled = true, groups = {"Community"})
    public void login5() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Login");
        login.login2(driver, mdTest);
        mdTest.log(Status.INFO, "Login successfully.");
    }

    @Test(priority = 47, enabled = true, groups = {"Community"})
    public void acceptConnectionRequest() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Accept connection request");
        lm.displayLeftMenu(driver, mdTest);
        lm.displayMessages(driver, mdTest);
        msgPage.acceptConnectionRequest(driver, mdTest);
        testBase.goToHomeInAndroid(driver, mdTest);
        mdTest.log(Status.INFO, "Connection Request accepted successfully");
    }

    @Test(priority = 48, enabled = true, groups = {"Community"})
    public void sendConnectionRequestCommunity_InMyNetwork() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Send connection request from community screen");
        cmnPage.sendConnectionRequest_MyNetwork(driver, mdTest);
        testBase.backFromEducatorScreen(driver, mdTest);
        testBase.backFromEducatorScreen(driver, mdTest);
        waitUntilAndroidElementVisibleByID(driver, Constants.btnHomeTB);
        mdTest.log(Status.INFO, "Verified: Learner already in network");
    }

    @Test(priority = 49, enabled = true, groups = {"Community"})
    public void sendMessageFromMyNetwork() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Send Message from my network");
        lm.displayLeftMenu(driver, mdTest);
        lm.displayMyNetwork(driver, mdTest);
        mnPage.sendMessage_MyNetwork(driver, mdTest);
        mdTest.log(Status.INFO, "Message sent.");
    }

    @Test(priority = 50, enabled = true, groups = {"Community"})
    public void removeUserFromMyNetwork() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Remove user from my network");
        //lm.displayLeftMenu(driver, mdTest);
        //lm.displayMyNetwork(driver, mdTest);
        mnPage.removeConnection_MyNetwork(driver, mdTest);
        mdTest.log(Status.INFO, "User removed from my network.");
    }

    @Test(priority = 51, enabled = true, groups = {"Community"})
    public void blockUserCommunity() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Block user from community screen");
        cmnPage.blockUser(driver, mdTest);
        testBase.backFromEducatorScreen(driver, mdTest);
        testBase.backFromEducatorScreen(driver, mdTest);
        waitUntilAndroidElementVisibleByID(driver, Constants.btnHomeTB);
        mdTest.log(Status.INFO, "User blocked successfully.");
    }

    @Test(priority = 52, enabled = true, groups = {"Community"})
    public void logout5() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Logout");
        lm.displayLeftMenu(driver, mdTest);
        lm.logout(driver, mdTest);
        mdTest.log(Status.INFO, "Logout successfully.");
    }

    @Test(priority = 53, enabled = true, groups = {"Community"})
    public void login6() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Login");
        login.login1(driver, mdTest);
        mdTest.log(Status.INFO, "Login successfully.");
    }

    @Test(priority = 54, enabled = true, groups = {"Community"})
    public void sendConnectionRequestCommunity_BlockedUser() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Send connection request from community screen");
        cmnPage.sendConnectionRequest_BlockedUser(driver, mdTest);
        testBase.backFromEducatorScreen(driver, mdTest);
        testBase.backFromEducatorScreen(driver, mdTest);
        waitUntilAndroidElementVisibleByID(driver, Constants.btnHomeTB);
        mdTest.log(Status.INFO, "Verified: Can't able to sent request if user blocked you.");
    }

    @Test(priority = 55, enabled = true, groups = {"Community"})
    public void logout6() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Logout");
        lm.displayLeftMenu(driver, mdTest);
        lm.logout(driver, mdTest);
        mdTest.log(Status.INFO, "Logout successfully.");
    }

    @Test(priority = 56, enabled = true, groups = {"Community"})
    public void login7() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Login");
        login.login2(driver, mdTest);
        mdTest.log(Status.INFO, "Login successfully.");
    }

    @Test(priority = 57, enabled = true, groups = {"Community"}) ///////////////////////////////////////// review
    public void unblockUserCommunity() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Unblock user from community screen");
        cmnPage.unblockUser(driver, mdTest);
        testBase.backFromEducatorScreen(driver, mdTest);
        testBase.backFromEducatorScreen(driver, mdTest);
        waitUntilAndroidElementVisibleByID(driver, Constants.btnHomeTB);
        mdTest.log(Status.INFO, "User unblocked successfully.");
    }

    @Test(priority = 58, enabled = true, groups = {"Community"})
    public void sendMessageCommunity() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Send a message from community screen");
        cmnPage.sendMessage(driver, mdTest);
        testBase.backFromEducatorScreen(driver, mdTest);
        testBase.backFromEducatorScreen(driver, mdTest);
        waitUntilAndroidElementVisibleByID(driver, Constants.btnHomeTB);
        mdTest.log(Status.INFO, "Message sent successfully.");
    }

    @Test(priority = 59, enabled = true, groups = {"Community"})
    public void logout7() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Logout");
        lm.displayLeftMenu(driver, mdTest);
        lm.logout(driver, mdTest);
        mdTest.log(Status.INFO, "Logout successfully.");
    }

    @Test(priority = 60, enabled = true, groups = {"Community"})
    public void login8() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Login");
        login.login1(driver, mdTest);
        mdTest.log(Status.INFO, "Login successfully.");
    }

    @Test(priority = 61, enabled = true, groups = {"Community"})
    public void sendConnectionRequestCommunity_profile() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Send connection request from profile screen of community");
        cmnPage.sendConnectionRequest_profile(driver, mdTest);
        mdTest.log(Status.INFO, "Request sent.");
    }

    @Test(priority = 62, enabled = true, groups = {"Community"})
    public void sendConnectionRequestAgainCommunity_profile() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Send connection request from profile screen of community");
        cmnPage.sendConnectionRequestAgain_profile(driver, mdTest);
        testBase.backFromEducatorScreen(driver, mdTest);
        testBase.backFromEducatorScreen(driver, mdTest);
        waitUntilAndroidElementVisibleByID(driver, Constants.btnHomeTB);
        mdTest.log(Status.INFO, "Request already sent.");
    }

    @Test(priority = 63, enabled = true, groups = {"Community"})
    public void logout8() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Logout");
        lm.displayLeftMenu(driver, mdTest);
        lm.logout(driver, mdTest);
        mdTest.log(Status.INFO, "Logout successfully.");
    }

    @Test(priority = 64, enabled = true, groups = {"Community"})
    public void login9() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Login");
        login.login2(driver, mdTest);
        mdTest.log(Status.INFO, "Login successfully.");
    }

    @Test(priority = 65, enabled = true, groups = {"Community"})
    public void sendConnectionRequestCommunity_profile_ReceivedRequest() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Send connection request from profile screen of community");
        cmnPage.sendConnectionRequestAgain_profile_ReceivedRequest(driver, mdTest);
        testBase.backFromEducatorScreen(driver, mdTest);
        testBase.backFromEducatorScreen(driver, mdTest);
        waitUntilAndroidElementVisibleByID(driver, Constants.btnHomeTB);
        mdTest.log(Status.INFO, "Request already received.");
    }

    @Test(priority = 66, enabled = true, groups = {"Community"})
    public void acceptConnectionRequest2() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Accept connection request");
        lm.displayLeftMenu(driver, mdTest);
        lm.displayMessages(driver, mdTest);
        msgPage.acceptConnectionRequest(driver, mdTest);
        testBase.goToHomeInAndroid(driver, mdTest);
        mdTest.log(Status.INFO, "Connection Request accepted successfully");
    }

    @Test(priority = 67, enabled = true, groups = {"Community"})
    public void SendMessageCommunity_profile() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Send a message from profile screen of community");
        cmnPage.sendMessage_profile(driver, mdTest);
        testBase.backFromEducatorScreen(driver, mdTest);
        testBase.backFromEducatorScreen(driver, mdTest);
        waitUntilAndroidElementVisibleByID(driver, Constants.btnHomeTB);
        mdTest.log(Status.INFO, "Message sent successfully.");
    }

    @Test(priority = 68, enabled = true, groups = {"Community"})
    public void blockUserFromMyNetwork() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Block user from my network");
        lm.displayLeftMenu(driver, mdTest);
        lm.displayMyNetwork(driver, mdTest);
        mnPage.blockUser_MyNetwork(driver, mdTest);
        //testBase.goToHomeInAndroid(driver, mdTest);
        mdTest.log(Status.INFO, "User blocked successfully.");
    }

    @Test(priority = 69, enabled = true, groups = {"Community"})
    public void unblockUserFromMyNetwork() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Unblcok user from my network");
        //lm.displayLeftMenu(driver, mdTest);
        //lm.displayMyNetwork(driver, mdTest);
        mnPage.unblockUser_MyNetwork(driver, mdTest);
        testBase.goToHomeInAndroid(driver, mdTest);
        mdTest.log(Status.INFO, "User unblocked successfully.");
    }

    @Test(priority = 70, enabled = true, groups = {"Community"})
    public void removeFromNetworkCommunity_profile() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Remove user from profile screen of community");
        cmnPage.RemoveFromCommunity_profile(driver, mdTest);
        testBase.backFromEducatorScreen(driver, mdTest);
        testBase.backFromEducatorScreen(driver, mdTest);
        waitUntilAndroidElementVisibleByID(driver, Constants.btnHomeTB);
        mdTest.log(Status.INFO, "User removed from my network.");
    }

    @Test(priority = 71, enabled = true, groups = {"Community"})
    public void blockUserCommunity_profile() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Block user from community screen");
        cmnPage.blockUser_profile(driver, mdTest);
        testBase.backFromEducatorScreen(driver, mdTest);
        testBase.backFromEducatorScreen(driver, mdTest);
        waitUntilAndroidElementVisibleByID(driver, Constants.btnHomeTB);
        mdTest.log(Status.INFO, "User blocked successfully.");
    }

    @Test(priority = 72, enabled = true, groups = {"Community"})
    public void logout9() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Logout");
        lm.displayLeftMenu(driver, mdTest);
        lm.logout(driver, mdTest);
        mdTest.log(Status.INFO, "Logout successfully.");
    }

    @Test(priority = 73, enabled = true, groups = {"Community"})
    public void login10() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Login");
        login.login1(driver, mdTest);
        mdTest.log(Status.INFO, "Login successfully.");
    }

    @Test(priority = 74, enabled = true, groups = {"Community"})
    public void sendConnectionRequestCommunity_profile_BlockedUser() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Send connection request from profile screen of community");
        cmnPage.sendConnectionRequestAgain_profile_BlockedUser(driver, mdTest);
        testBase.backFromEducatorScreen(driver, mdTest);
        testBase.backFromEducatorScreen(driver, mdTest);
        waitUntilAndroidElementVisibleByID(driver, Constants.btnHomeTB);
        mdTest.log(Status.INFO, "User removed from my network.");
    }

    @Test(priority = 75, enabled = true, groups = {"Community"})
    public void logout10() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Logout");
        lm.displayLeftMenu(driver, mdTest);
        lm.logout(driver, mdTest);
        mdTest.log(Status.INFO, "Logout successfully.");
    }

    @Test(priority = 76, enabled = true, groups = {"Community"})
    public void login11() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Login");
        login.login2(driver, mdTest);
        mdTest.log(Status.INFO, "Login successfully.");
    }

    @Test(priority = 77, enabled = true, groups = {"Community"})
    public void unblockUserCommunity_profile() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Unblock user from community screen");
        cmnPage.unblockUser_profile(driver, mdTest);
        testBase.backFromEducatorScreen(driver, mdTest);
        testBase.backFromEducatorScreen(driver, mdTest);
        waitUntilAndroidElementVisibleByID(driver, Constants.btnHomeTB);
        mdTest.log(Status.INFO, "User unblocked.");
    }

    @Test(priority = 78, enabled = true, groups = {"Community"})
    public void logout11() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Logout");
        lm.displayLeftMenu(driver, mdTest);
        lm.logout(driver, mdTest);
        mdTest.log(Status.INFO, "Logout successfully.");
    }

    @Test(priority = 79, enabled = true, groups = {"Community"})
    public void login12() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Login");
        login.login1(driver, mdTest);
        mdTest.log(Status.INFO, "Login successfully.");
    }

    @Test(priority = 80, enabled = true, groups = {"Community"})
    public void sendConnectionRequestCommunity3() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Send connection request from community page");
        cmnPage.sendConnectionRequest(driver, mdTest);
        testBase.backFromEducatorScreen(driver, mdTest);
        testBase.backFromEducatorScreen(driver, mdTest);
        waitUntilAndroidElementVisibleByID(driver, Constants.btnHomeTB);
        mdTest.log(Status.INFO, "Connection request sent successfully");
    }

    @Test(priority = 81, enabled = true, groups = {"Community"})
    public void logout12() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Logout");
        lm.displayLeftMenu(driver, mdTest);
        lm.logout(driver, mdTest);
        mdTest.log(Status.INFO, "Logout successfully.");
    }

    @Test(priority = 82, enabled = true, groups = {"Community"})
    public void login13() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Login");
        login.login2(driver, mdTest);
        mdTest.log(Status.INFO, "Login successfully.");
    }

    @Test(priority = 83, enabled = true, groups = {"Community"})
    public void acceptConnectionRequest3() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Accept connection request");
        lm.displayLeftMenu(driver, mdTest);
        lm.displayMessages(driver, mdTest);
        msgPage.acceptConnectionRequest(driver, mdTest);
        testBase.goToHomeInAndroid(driver, mdTest);
        mdTest.log(Status.INFO, "Connection Request accepted successfully");
    }

    @Test(priority = 84, enabled = true, groups = {"Community"})
    public void sendMessageFromMyNetwork_profile() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Send Message from profile screen in my network");
        lm.displayLeftMenu(driver, mdTest);
        lm.displayMyNetwork(driver, mdTest);
        mnPage.sendMessage_MyNetwork_Profile(driver, mdTest);
        //testBase.goToHomeInAndroid(driver, mdTest);
        mdTest.log(Status.INFO, "Message sent.");
    }

    @Test(priority = 85, enabled = true, groups = {"Community"})
    public void blockUserFromMyNetwork_profile() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Block user from profile screen in my network");
        //lm.displayLeftMenu(driver, mdTest);
        //lm.displayMyNetwork(driver, mdTest);
        mnPage.blockUser_MyNetwork_Profile(driver, mdTest);
        //testBase.goToHomeInAndroid(driver, mdTest);
        mdTest.log(Status.INFO, "User blocked successfully.");
    }

    @Test(priority = 86, enabled = true, groups = {"Community"})
    public void unblockUserFromMyNetwork_profile() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Unblcok user from profile screen in my network");
        //lm.displayLeftMenu(driver, mdTest);
        //lm.displayMyNetwork(driver, mdTest);
        mnPage.unblockUser_MyNetwork_Profile(driver, mdTest);
        //testBase.goToHomeInAndroid(driver, mdTest);
        mdTest.log(Status.INFO, "User unblocked successfully.");
    }

    @Test(priority = 87, enabled = true, groups = {"Community"})
    public void removeUserFromMyNetwork_profile() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Remove user from profile screen in my network");
        //lm.displayLeftMenu(driver, mdTest);
        //lm.displayMyNetwork(driver, mdTest);
        mnPage.removeConnection_MyNetwork_Profile(driver, mdTest);
        //testBase.goToHomeInAndroid(driver, mdTest);
        mdTest.log(Status.INFO, "User removed from my network.");
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @Test(priority = 11, enabled = true, groups = {"Filter"})
    public void communityFilterByBusinessName() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Filter by Business name");
        filter.filterByBusinessName(driver, mdTest);
    }

    @Test(priority = 12, enabled = true, groups = {"Filter"})
    public void communityFilterByIndustry() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Filter by Industry");
        filter.filterByIndustry(driver, mdTest);
    }

    @Test(priority = 13, enabled = true, groups = {"Filter"})
    public void communityFilterByCity() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Filter by City");
        filter.filterByCity(driver, mdTest);
    }

    @Test(priority = 14, enabled = true, groups = {"Filter"})
    public void communityFilterByCountry() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Filter by Country");
        filter.filterByCountry(driver, mdTest);
    }

    @Test(priority = 15, enabled = true, groups = {"Filter"})
    public void communityFilterBySkill() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Filter by Skill");
        filter.filterBySkill(driver, mdTest);
    }

    @Test(priority = 16, enabled = true, groups = {"Filter"})
    public void communityFilterByParticipationYear() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Filter by Participation year");
        filter.filterByParticipationYear(driver, mdTest);
    }

    @Test(priority = 17, enabled = true, groups = {"Filter"})
    public void communityFilterByKeyword() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Filter by Keyword");
        filter.filterByKeyword(driver, mdTest);
    }

    @Test(priority = 18, enabled = true, groups = {"Filter"})
    public void communityFilterByProductAndServices() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Filter by Product&services(");
        filter.filterByProductAndServices(driver, mdTest);
    }

    @Test(priority = 19, enabled = true, groups = {"Filter"})
    public void communityFilterByOnlineProgram() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Filter by Online program");
        filter.filterByOnlineOffering(driver, mdTest);
    }

    @Test(priority = 20, enabled = true, groups = {"Filter"})
    public void communityFilterByOfflineEvent() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Filter by Offline event");
        filter.filterByOfflineOffering(driver, mdTest);
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @Test(priority = 13, enabled = true, groups = {"Review"})
    public void addTestimonialFromEduPage() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Add testimonial from educator page");
        review.addTestimonialFromEducatorPage(driver, mdTest);
        mdTest.log(Status.INFO, "Testimonial added successfully.");
    }

    @Test(priority = 14, enabled = true, groups = {"Review"})
    public void addTestimonialFromList() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Add testimonial from list");
        lm.displayLeftMenu(driver, mdTest);
        lm.displayReviews(driver, mdTest);
        review.addTestimonialFromReviewPage(driver, mdTest);
        mdTest.log(Status.INFO, "Testimonial added successfully.");
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @Test(priority = 17, enabled = true, groups = {"Favourite"})
    public void favouriteEducatorFromHomePage() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Favourite a educator from home page");
        fav.favouriteEducator_Home(driver, mdTest);
        mdTest.log(Status.INFO, "Educator favourited successfully.");
    }

    @Test(priority = 18, enabled = true, groups = {"Favourite"})
    public void unavouriteEducatorFromHomePage() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Unfavourite a educator from home page");
        fav.unfavouriteEducator_Home(driver, mdTest);
        mdTest.log(Status.INFO, "Educator unfavourited successfully.");
    }

    @Test(priority = 19, enabled = true, groups = {"Favourite"})
    public void favouriteEduFromEducatorPage() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Favourite a educator from home page");
        fav.favouriteEduFromEducatorPage(driver, mdTest);
        mdTest.log(Status.INFO, "Educator favourited successfully.");
    }

    @Test(priority = 20, enabled = true, groups = {"Favourite"})
    public void unavouriteEduFromEducatorPage() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Unfavourite a educator from home page");
        fav.unfavouriteEduFromEducatorPage(driver, mdTest);
        mdTest.log(Status.INFO, "Educator unfavourited successfully.");
    }

    @Test(priority = 21, enabled = true, groups = {"Favourite"})
    public void favouriteEduFromExplorePage() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Favourite a educator from explore page");
        fav.favouriteEduFromExplorePage(driver, mdTest);
        mdTest.log(Status.INFO, "Educator favourited successfully.");
    }

    @Test(priority = 22, enabled = true, groups = {"Favourite"})
    public void unavouriteEduFromExplorePage() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Unfavourite a educator from explore page");
        fav.unfavouriteEduFromExplorePage(driver, mdTest);
        mdTest.log(Status.INFO, "Educator unfavourited successfully.");
    }

    @Test(priority = 23, enabled = true, groups = {"Favourite"})
    public void favouriteOfferingFromEducatorPage() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Favourite a offering from educator page");
        fav.favouriteOfferingFromEducatorPage(driver, mdTest);
        mdTest.log(Status.INFO, "Offering favourited successfully.");
    }

    @Test(priority = 24, enabled = true, groups = {"Favourite"})
    public void unfavouriteOfferingFromEducatorPage() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Unfavourite a offering from educator page");
        fav.unfavouriteOfferingFromEducatorPage(driver, mdTest);
        mdTest.log(Status.INFO, "Offering unfavourited successfully.");
    }

    @Test(priority = 25, enabled = true, groups = {"Favourite"})
    public void favouriteOfferingFromOfferingPage() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Favourite a offering from offering details page");
        fav.favouriteOfferingFromOfferingPage(driver, mdTest);
        mdTest.log(Status.INFO, "Offering favourited successfully.");
    }

    @Test(priority = 26, enabled = true, groups = {"Favourite"})
    public void unfavouriteOfferingFromOfferingPage() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Unfavourite a offering from offering details page");
        fav.unfavouriteOfferingFromOfferingPage(driver, mdTest);
        mdTest.log(Status.INFO, "Offering unfavourited successfully.");
    }

    @Test(priority = 27, enabled = true, groups = {"Favourite"})
    public void favouriteOfferingFromOfferingMorePage() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Favourite a offering from offering more details page");
        fav.favouriteOfferingFromOfferingMorePage(driver, mdTest);
        mdTest.log(Status.INFO, "Offering favourited successfully.");
    }

    @Test(priority = 28, enabled = true, groups = {"Favourite"})
    public void unfavouriteOfferingFromOfferingMorePage() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Unfavourite a offering from offering more details page");
        fav.unfavouriteOfferingFromOfferingMorePage(driver, mdTest);
        mdTest.log(Status.INFO, "Offering unfavourited successfully.");
    }

    @Test(priority = 29, enabled = true, groups = {"Favourite"})
    public void favouriteOnlineOfferingFromHomePage() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Favourite a Online Offering from home page");
        fav.favouriteOnlineOffering_Home(driver, mdTest);
        mdTest.log(Status.INFO, "Offering favourited successfully.");
    }

    @Test(priority = 30, enabled = true, groups = {"Favourite"})
    public void unavouriteOnlineOfferingFromHomePage() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Unfavourite a Online Offering from home page");
        fav.unfavouriteOnlineOffering_Home(driver, mdTest);
        mdTest.log(Status.INFO, "Offering unfavourited successfully.");
    }

    @Test(priority = 31, enabled = true, groups = {"Favourite"})
    public void favouriteEventFromHomePage() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Favourite a Offline Offering from home page");
        fav.favouriteEvent_Home(driver, mdTest);
        mdTest.log(Status.INFO, "Offering favourited successfully.");
    }

    @Test(priority = 32, enabled = true, groups = {"Favourite"})
    public void unavouriteEventFromHomePage() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Unfavourite a Offline Offering from home page");
        fav.unfavouriteEvent_Home(driver, mdTest);
        mdTest.log(Status.INFO, "Offering unfavourited successfully.");
    }

    @Test(priority = 33, enabled = true, groups = {"Favourite"})
    public void favouriteContent() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Favourite a Content from consumption page");
        fav.favouriteContent(driver, mdTest);
        mdTest.log(Status.INFO, "Content favourited successfully.");
    }

    @Test(priority = 34, enabled = true, groups = {"Favourite"})
    public void unfavouriteContent() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Unfavourite a Content from consumption page");
        fav.unfavouriteContent(driver, mdTest);
        mdTest.log(Status.INFO, "Content unfavourited successfully.");
    }

    @Test(priority = 35, enabled = true, groups = {"Favourite"})
    public void favouriteLastViewedContent() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Favourite a Content from 'Last Viewed Content' section");
        fav.favouriteLastViewedContent(driver, mdTest);
        mdTest.log(Status.INFO, "Content favourited successfully.");
    }

    @Test(priority = 36, enabled = true, groups = {"Favourite"})
    public void unavouriteLastViewedContent() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Unfavourite a Content from 'Last Viewed Content' section");
        fav.unfavouriteLastViewedContent(driver, mdTest);
        mdTest.log(Status.INFO, "Content unfavourited successfully.");
    }

    @Test(priority = 37, enabled = true, groups = {"Favourite"})
    public void favouriteEducatorFromHomePage2() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Favourite a educator from home page");
        fav.favouriteEducator_Home(driver, mdTest);
        mdTest.log(Status.INFO, "Educator favourited successfully.");
    }

    @Test(priority = 38, enabled = true, groups = {"Favourite"})
    public void educatorUnfavouritedFromFavouriteScreen() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Unfavourite a educator from favourite screen");
        lm.displayLeftMenu(driver, mdTest);
        lm.displayFavourites(driver, mdTest);
        fav.UnFavouriteEducator(driver, mdTest);
        mdTest.log(Status.INFO, "Educator unfavourited successfully.");
    }

    @Test(priority = 39, enabled = true, groups = {"Favourite"})
    public void favouriteOnlineOfferingFromHomePage2() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Favourite a Online Offering from home page");
        fav.favouriteOnlineOffering_Home(driver, mdTest);
        mdTest.log(Status.INFO, "Offering favourited successfully.");
    }

    @Test(priority = 40, enabled = true, groups = {"Favourite"})
    public void offeringUnfavouritedFromFavouriteScreen() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Unfavourite a offering from favourite screen");
        lm.displayLeftMenu(driver, mdTest);
        lm.displayFavourites(driver, mdTest);
        fav.UnFavouriteOffering(driver, mdTest);
        mdTest.log(Status.INFO, "Offering unfavourited successfully.");
    }

    @Test(priority = 41, enabled = true, groups = {"Favourite"})
    public void favouriteContent2() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Favourite a Content from consumption page");
        fav.favouriteContent(driver, mdTest);
        mdTest.log(Status.INFO, "Content favourited successfully.");
    }

    @Test(priority = 42, enabled = true, groups = {"Favourite"})
    public void contentUnfavouritedFromFavouriteScreen() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Unfavourite a content from favourite screen");
        lm.displayLeftMenu(driver, mdTest);
        lm.displayFavourites(driver, mdTest);
        fav.UnFavouriteContent(driver, mdTest);
        mdTest.log(Status.INFO, "Content unfavourited successfully.");
    }

    @Test(priority = 43, enabled = true, groups = {"Favourite"})
    public void favouriteOffering_UpcomingSection() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Favourite Offering from upcoming section");
        fav.favouriteOffering_UpcomingSection(driver, mdTest);
        mdTest.log(Status.INFO, "Offering favourited successfully.");
    }

    @Test(priority = 44, enabled = true, groups = {"Favourite"})
    public void unfavouriteOffering_UpcomingSection() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Unfavourite a Offering from upcoming section");
        fav.unfavouriteOffering_UpcomingSection(driver, mdTest);
        mdTest.log(Status.INFO, "Offering unfavourited successfully.");
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @Test(priority = 2, enabled = true, groups = {"ProfileVisibility"})
    public void profileVisibilityOff() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Profile visibility - OFF");
        //lm.displayLeftMenu(driver, mdTest);
        //lm.displayProfile(driver, mdTest);
        testBase.openProfile(driver, mdTest);
        profile.changeProfileVisibility(driver, mdTest, "OFF");
        mdTest.log(Status.INFO, "Profile visibility set to on successfully.");
    }

    @Test(priority = 3, enabled = true, groups = {"ProfileVisibility"})
    public void logout13() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Logout");
        lm.displayLeftMenu(driver, mdTest);
        lm.logout(driver, mdTest);
        mdTest.log(Status.INFO, "Logout successfully.");
    }

    @Test(priority = 4, enabled = true, groups = {"ProfileVisibility"})
    public void login14() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Login");
        login.login2(driver, mdTest);
        mdTest.log(Status.INFO, "Login successfully.");
    }

    @Test(priority = 5, enabled = true, groups = {"ProfileVisibility"})
    public void verify_UserNotExistInCommunity() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Verify - User does not exist in community");
        cmnPage.verify_UserNotExistInCommunity(driver, mdTest);
        testBase.backFromEducatorScreen(driver, mdTest);
        testBase.backFromEducatorScreen(driver, mdTest);
        waitUntilAndroidElementVisibleByID(driver, Constants.btnHomeTB);
        mdTest.log(Status.INFO, "Verified: User is not in community");
    }

    @Test(priority = 6, enabled = true, groups = {"ProfileVisibility"})
    public void logout14() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Logout");
        lm.displayLeftMenu(driver, mdTest);
        lm.logout(driver, mdTest);
        mdTest.log(Status.INFO, "Logout successfully.");
    }

    @Test(priority = 7, enabled = true, groups = {"ProfileVisibility"})
    public void login15() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Login");
        login.login1(driver, mdTest);
        mdTest.log(Status.INFO, "Login successfully.");
    }

    @Test(priority = 8, enabled = true, groups = {"ProfileVisibility"})
    public void profileVisibilityOn() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Profile Visibility - On");
        //lm.displayLeftMenu(driver, mdTest);
        //lm.displayProfile(driver, mdTest);
        testBase.openProfile(driver, mdTest);
        profile.changeProfileVisibility(driver, mdTest, "On");
        mdTest.log(Status.INFO, "Profile visibility set to off successfully.");
    }

    @Test(priority = 9, enabled = true, groups = {"ProfileVisibility"})
    public void logout15() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Logout");
        lm.displayLeftMenu(driver, mdTest);
        lm.logout(driver, mdTest);
        mdTest.log(Status.INFO, "Logout successfully.");
    }

    @Test(priority = 10, enabled = true, groups = {"ProfileVisibility"})
    public void login16() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Login");
        login.login2(driver, mdTest);
        mdTest.log(Status.INFO, "Login successfully.");
    }

    @Test(priority = 11, enabled = true, groups = {"ProfileVisibility"})
    public void verify_UserExistInCommunity() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Verify - User exist in community");
        cmnPage.verify_UserExistInCommunity(driver, mdTest);
        testBase.backFromEducatorScreen(driver, mdTest);
        testBase.backFromEducatorScreen(driver, mdTest);
        waitUntilAndroidElementVisibleByID(driver, Constants.btnHomeTB);
        mdTest.log(Status.INFO, "Verified: User is in community");
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @Test(priority = 2, enabled = true, groups = {"Other"})
    public void verifyNextSectionButton() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Verify 'Next Section' button");
        ccPage.verifyNextSectionButton(driver, mdTest);
        mdTest.log(Status.INFO, "Verified successfully.");
    }

    @Test(priority = 3, enabled = true, groups = {"Other"})
    public void downloadContent_Offering() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Download Content");
        ccPage.downloadContent(driver, mdTest);
        mdTest.log(Status.INFO, "Downloaded successfully.");
        testBase.backFromEducatorScreen(driver, mdTest);
        waitUntilAndroidElementVisibleByID(driver, Constants.btnHomeTB);
    }

    @Test(priority = 4, enabled = true, groups = {"Other"})
    public void verifyReadMoreButton() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Verify read more button on educator screen");
        edu.checkReadMoreButton(driver, mdTest);
        testBase.backFromEducatorScreen(driver, mdTest);
        waitUntilAndroidElementVisibleByID(driver, Constants.btnHomeTB);
        mdTest.log(Status.INFO, "Verified successfully.");
    }

    @Test(priority = 5, enabled = false, groups = {"Other"})
    public void deleteNotification() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: ");
        lm.displayLeftMenu(driver, mdTest);
        lm.displayNotification(driver, mdTest);
        nPage.deleteNotification(driver, mdTest);
        testBase.goToHomeInAndroid(driver, mdTest);
        mdTest.log(Status.INFO, "");
    }

    @Test(priority = 6, enabled = false, groups = {"Other"})
    public void sentSupportRequest_ExplorePage() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: ");
        ePage.sentSupportRequest_ExplorePage(driver, mdTest);
        testBase.goToHomeInAndroid(driver, mdTest);
        mdTest.log(Status.INFO, "");
    }

    @Test(priority = 7, enabled = false, groups = {"Other"})
    public void sendMessageFromMessageScreen() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Register in offeringSend message");
        lm.displayLeftMenu(driver, mdTest);
        lm.displayMessages(driver, mdTest);
        msgPage.sendMessage(driver, mdTest);
        testBase.goToHomeInAndroid(driver, mdTest);
        mdTest.log(Status.INFO, "");
    }

    @Test(priority = 8, enabled = true, groups = {"Other"})
    public void openPrivatePolicy() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Open Private Policy");
        lm.displayLeftMenu(driver, mdTest);
        lm.openPrivatePolicy(driver, mdTest);
        testBase.goToHomeInAndroid(driver, mdTest);
        mdTest.log(Status.INFO, "Private Policy page opened successfully.");
    }

    @Test(priority = 9, enabled = true, groups = {"Other"})
    public void changePassword() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Change Password");
        //lm.displayLeftMenu(driver, mdTest);
        //lm.displayProfile(driver, mdTest);
        testBase.openProfile(driver, mdTest);
        profile.changePassword(driver, mdTest);
        mdTest.log(Status.INFO, "Password changed successfully.");
    }

    @Test(priority = 10, enabled = true, groups = {"Other"})
    public void forgotPassword() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Forgot Password");
        login.ForgotPassword(driver, mdTest);
        mdTest.log(Status.INFO, "Mail sent successfully.");
    }

    @Test(priority = 11, enabled = true, groups = {"Other"})
    public void login17() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Login");
        login.login1(driver, mdTest);
        //login.login2(driver, mdTest);
        mdTest.log(Status.INFO, "Login successfully.");
    }

    @Test(priority = 12, enabled = true, groups = {"Other"})
    public void logout_Final() throws InterruptedException, IOException {
        mdTest = report.createTest("Test Case: Logout");
        lm.displayLeftMenu(driver, mdTest);
        lm.logout(driver, mdTest);
        mdTest.log(Status.INFO, "Logout successfully.");
        //mdTest.addScreenCaptureFromPath("E:\\Knk\\MasterdojoAutomation\\WebLearnerAutomation\\Screenshots\\Test.png","test");
    }

    @AfterMethod(alwaysRun = true)
    public void getResult(ITestResult result) throws Exception {
        if (result.getStatus() == ITestResult.FAILURE) {
            mdTest.log(Status.FAIL, MarkupHelper.createLabel(result.getName() + " - Test Case Failed", ExtentColor.RED));
            mdTest.log(Status.FAIL, MarkupHelper.createLabel(result.getThrowable() + " - Test Case Failed", ExtentColor.RED));
            testBase.goToHomeInAndroid(driver, mdTest);
            // mdTest.fail("Test Case Failed Snapshot is below " + mdTest.addScreenCaptureFromPath(System.getProperty("user.dir") + "\\Screenshots\\"));
        } else if (result.getStatus() == ITestResult.SUCCESS) {
            //mdTest.log(Status.PASS, MarkupHelper.createLabel(result.getName()+" Test Case PASSED", ExtentColor.GREEN));
            mdTest.log(Status.PASS, result.getName() + " Test Case PASSED");
        } else if (result.getStatus() == ITestResult.SKIP) {
            mdTest.log(Status.SKIP, MarkupHelper.createLabel(result.getName() + " - Test Case Skipped", ExtentColor.ORANGE));
        }
    }

    @AfterClass(alwaysRun = true)
    public void endReport() {
        report.flush();
    }

}
