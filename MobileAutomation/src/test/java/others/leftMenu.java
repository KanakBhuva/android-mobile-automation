package others;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import testBase.TestBase;

public class leftMenu extends TestBase {

    public void displayLeftMenu(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        waitUntilAndroidElementVisibleByxPath(driver, Constants.btnLeftMenu);
        driver.findElementByXPath(Constants.btnLeftMenu).click();
    }

    public void displayHome(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        waitUntilAndroidElementVisibleByID(driver, Constants.btnHome);
        driver.findElementById(Constants.btnHome).click();
    }

    public void displayProfile(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        waitUntilAndroidElementVisibleByID(driver, Constants.btnProfile);
        driver.findElementById(Constants.btnProfile).click();
    }

    public void displayFavourites(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        waitUntilAndroidElementVisibleByID(driver, Constants.btnFavourites);
        if (driver.findElementsByXPath(Constants.favCount).size() > 0) {
            logger.log(Status.INFO, "Favourite Count: " + driver.findElementByXPath(Constants.favCount).getText());
        }
        driver.findElementById(Constants.btnFavourites).click();
    }

    public void displayReviews(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        waitUntilAndroidElementVisibleByID(driver, Constants.btnReview);
        driver.findElementById(Constants.btnReview).click();
    }

    public void displayReferralHistory(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        waitUntilAndroidElementVisibleByID(driver, Constants.btnReferralHistory);
        driver.findElementById(Constants.btnReferralHistory).click();
    }

    public void displayNotification(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        waitUntilAndroidElementVisibleByID(driver, Constants.btnNotification);
        driver.findElementById(Constants.btnNotification).click();
    }

    public void displayMessages(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        waitUntilAndroidElementVisibleByID(driver, Constants.btnMessages);
        driver.findElementById(Constants.btnMessages).click();
    }

    public void displayPayment(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        waitUntilAndroidElementVisibleByID(driver, Constants.btnPayment);
        driver.findElementById(Constants.btnPayment).click();
    }

    public void displayMyNetwork(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        waitUntilAndroidElementVisibleByID(driver, Constants.btnMyNetwork);
        driver.findElementById(Constants.btnMyNetwork).click();
    }

    public void openPrivatePolicy(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        waitUntilAndroidElementVisibleByID(driver, Constants.btnPrivacyPolicy);
        driver.findElementById(Constants.btnPrivacyPolicy).click();
        logger.log(Status.INFO,"Private policy opened");
        driver.pressKey(new KeyEvent().withKey(AndroidKey.BACK));
        logger.log(Status.INFO,"Back to home page");
    }

    public void logout(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        waitUntilAndroidElementVisibleByID(driver, Constants.btnLogout);
        driver.findElementById(Constants.btnLogout).click();
    }


}
