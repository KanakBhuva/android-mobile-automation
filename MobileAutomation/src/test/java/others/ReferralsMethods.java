package others;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import io.appium.java_client.android.AndroidDriver;
import testBase.TestBase;

public class ReferralsMethods extends TestBase {

    public void postReferralProcess(AndroidDriver driver, ExtentTest logger, String refType, String msg, String SelectNames) throws InterruptedException {

        waitUntilAndroidElementVisibleByID(driver, Constants.refProceed);
        driver.findElementById(Constants.refProceed).click();
        waitUntilAndroidElementVisibleByID(driver, Constants.btnNext);
        logger.log(Status.INFO,"Adding referral message");

        if (refType.toLowerCase().equals("sms")) {
            driver.findElementById(Constants.refSMSBtn).click();
        } else if (refType.toLowerCase().equals("mail")) {
            driver.findElementById(Constants.refMailBtn).click();
        }

        if (!msg.equals("")) {
            driver.findElementById(Constants.txtReferral).clear();
            Thread.sleep(1000);
            driver.findElementById(Constants.txtReferral).sendKeys(msg);
            logger.log(Status.INFO,"Referral message changed");
        }

        driver.findElementById(Constants.btnNext).click();

        waitUntilAndroidElementVisibleByID(driver, Constants.btnSelect);
        logger.log(Status.INFO,"Contact screen displayed");
        if (SelectNames.equals("All")) {
            driver.findElementById(Constants.selectAllContact).click();
            logger.log(Status.INFO,"All contact selected");
        } else {
            driver.findElementById(Constants.searchBox).sendKeys(SelectNames);
            waitUntilAndroidElementVisibleByID(driver, Constants.searchBox); //Thread.sleep(3000);
            searchAndClick(driver, Constants.contactList, Constants.titleName, SelectNames);
            logger.log(Status.INFO,"Contact selected");
        }

        waitUntilAndroidElementVisibleByID(driver, Constants.btnSelect);
        //driver.hideKeyboard();
        driver.findElementById(Constants.btnSelect).click();
        waitInSeconds(5);

        if (refType.equals("SMS")) {
            waitUntilAndroidElementVisibleByID(driver, Constants.btnSendMsg);
            driver.findElementById(Constants.btnSendMsg).click();
            logger.log(Status.INFO,"SMS Sent successfully");
            //driver.findElementById(Constants.btnBack).click();
            logger.log(Status.INFO,"Message sent");
        } else if (refType.equals("Mail")) {
            waitUntilAndroidElementVisibleByID(driver, Constants.btnSendMail);
            waitInSeconds(3);
            driver.findElementById(Constants.btnSendMail).click();
            logger.log(Status.INFO,"Email sent successfully");
        }

        waitUntilAndroidElementVisibleByID(driver, Constants.btnSkip);
        driver.findElementById(Constants.btnSkip).click();
        logger.log(Status.INFO,"Back to home screen");
    }

    public void postResendReferralProcess(AndroidDriver driver, ExtentTest logger, String refType, String msg, String SelectNames) throws InterruptedException {

        waitUntilAndroidElementVisibleByID(driver, Constants.btnResend);
        if (SelectNames.equals("All")) {
            driver.findElementById(Constants.selectAllContact).click();
            logger.log(Status.INFO,"All contact selected");
        } else {
            driver.findElementById(Constants.searchBox).sendKeys(SelectNames);
            waitUntilAndroidElementVisibleByID(driver, Constants.searchBox);
            searchAndClick(driver, Constants.contactList, Constants.titleName, SelectNames);
            logger.log(Status.INFO,"Contact selected");
        }

        driver.findElementById(Constants.btnResend).click();
        logger.log(Status.INFO,"Adding a referral message");

        if (refType.equals("SMS")) {
            waitUntilAndroidElementVisibleByID(driver, Constants.refSMSBtn);
            driver.findElementById(Constants.refSMSBtn).click();
        } else if (refType.equals("Mail")) {
            waitUntilAndroidElementVisibleByID(driver, Constants.refMailBtn);
            driver.findElementById(Constants.refMailBtn).click();
        }

        if (!msg.equals("")) {
            driver.findElementById(Constants.txtReferral).clear();
            Thread.sleep(1000);
            driver.findElementById(Constants.txtReferral).sendKeys(msg);
            logger.log(Status.INFO,"Referral message changed");
        }

        driver.findElementById(Constants.btnNext).click();
        waitInSeconds(5);

        if (refType.equals("SMS")) {
            waitUntilAndroidElementVisibleByID(driver, Constants.btnSendMsg);
            driver.findElementById(Constants.btnSendMsg).click();
            logger.log(Status.INFO,"Message sent");
        } else if (refType.equals("Mail")) {
            waitUntilAndroidElementVisibleByID(driver, Constants.btnSendMail);
            driver.findElementById(Constants.btnSendMail).click();
            logger.log(Status.INFO,"Mail sent");
        }

        waitUntilAndroidElementVisibleByID(driver, Constants.btnSkip);
        driver.findElementById(Constants.btnSkip).click();
    }

}
