package others;

public class Constants {

    public static final String jsonPath = "E:\\Knk\\Data\\Jsons\\WebEducator.json";
    public static final String title = "com.mvp1ventures.masterdojo:id/tvTitle";
    public static final String messagePopUp = "com.mvp1ventures.masterdojo:id/tvMessage";
    public static final String btnOK = "com.mvp1ventures.masterdojo:id/btnOk";
    public static final String searchBox = "com.mvp1ventures.masterdojo:id/etSearch";
    public static final String btnSearch = "com.mvp1ventures.masterdojo:id/imgSearch";
    public static final String btnSearch2 = "com.mvp1ventures.masterdojo:id/btnSearch";
    public static final String titleName = "com.mvp1ventures.masterdojo:id/tvName";
    public static final String btnBack = "//android.view.ViewGroup/android.widget.ImageButton";
    public static final String btnSysBack = "miui:id/up";
    public static final String eleScrollAndFind = "//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mvp1ventures.masterdojo:id/recyclerViewLastSearchHistory']/android.widget.FrameLayout";
    public static final String eleScrollAndFind2 = "//android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.FrameLayout/android.widget.LinearLayout";
    public static final String progressBar = "com.mvp1ventures.masterdojo:id/progressbar";
    public static final String countryCodeDD = "com.mvp1ventures.masterdojo:id/imageView_arrow";
    public static final String searchCountryCC = "com.mvp1ventures.masterdojo:id/editText_search";
    public static final String searchCountryList = "//androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout";
    public static final String userId = "com.mvp1ventures.masterdojo:id/etUserName";
    public static final String password = "com.mvp1ventures.masterdojo:id/etPassword";
    public static final String btnLogin = "com.mvp1ventures.masterdojo:id/btnLogin";
    public static final String txtTitle = "com.mvp1ventures.masterdojo:id/txtTitle";
    public static final String btnFavourite = "com.mvp1ventures.masterdojo:id/imgFavourite";
    public static final String btnFavourite2 = "com.mvp1ventures.masterdojo:id/imgIsFavorite";
    public static final String btnFavourite3 = "com.mvp1ventures.masterdojo:id/imgFavourite";
    public static final String txtCompanyName = "com.mvp1ventures.masterdojo:id/tvCoachCompany";
    public static final String btnOnlinePrograms = "com.mvp1ventures.masterdojo:id/llOnlinePrograms";
    public static final String btnOfflineEvent = "com.mvp1ventures.masterdojo:id/llEvents";
    public static final String btnEducator = "com.mvp1ventures.masterdojo:id/llEducator";

    //com.mvp1ventures.masterdojo:id/txtSubText
    //
    //com.mvp1ventures.masterdojo:id/imgLogo

    public static final String btnHomeTB = "com.mvp1ventures.masterdojo:id/navigation_home";
    public static final String businessRef = "com.mvp1ventures.masterdojo:id/navigation_share";
    public static final String businessRefExp = "com.mvp1ventures.masterdojo:id/navigation_explore";
    public static final String btnCommunityEdu = "com.mvp1ventures.masterdojo:id/navigation_followers";
    public static final String btnNotificationEdu = "com.mvp1ventures.masterdojo:id/navigation_notification";
    public static final String btnSetting = "com.mvp1ventures.masterdojo:id/navigation_setting";

    //String str = "androidx.recyclerview.widget.RecyclerView/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.TextView"; //OR id- com.mvp1ventures.masterdojo:id/txtTitle";
    //com.mvp1ventures.masterdojo:id/imgIsFavorite OR androidx.recyclerview.widget.RecyclerView/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[3]/android.widget.ImageView

    public static final String btnLeftMenu = "//android.widget.ImageButton[@content-desc='Navigate up']";
    public static final String btnCloseLM = "com.mvp1ventures.masterdojo:id/imgClose";
    public static final String btnHome = "com.mvp1ventures.masterdojo:id/nav_home";
    public static final String btnProfile = "com.mvp1ventures.masterdojo:id/nav_profile";
    public static final String btnFavourites = "com.mvp1ventures.masterdojo:id/nav_favourite";
    public static final String btnReview = "com.mvp1ventures.masterdojo:id/nav_testimonials";
    public static final String btnReferralHistory = "com.mvp1ventures.masterdojo:id/nav_referral_history";
    public static final String btnNotification = "com.mvp1ventures.masterdojo:id/nav_notification";
    public static final String btnMessages = "com.mvp1ventures.masterdojo:id/nav_messages";
    public static final String btnPayment = "com.mvp1ventures.masterdojo:id/nav_payment";
    public static final String btnMyNetwork = "com.mvp1ventures.masterdojo:id/nav_my_network";
    public static final String btnPrivacyPolicy = "com.mvp1ventures.masterdojo:id/nav_privacy";
    public static final String btnLogout = "com.mvp1ventures.masterdojo:id/nav_logout";

    /* ----------------------- Start: Referrals --------------------------------------------------- */

    public static final String refProceed = "com.mvp1ventures.masterdojo:id/btnProceed";
    public static final String refSMSBtn = "com.mvp1ventures.masterdojo:id/btnTabSms";
    public static final String refMailBtn = "com.mvp1ventures.masterdojo:id/btnTabEmail";
    public static final String txtReferral = "com.mvp1ventures.masterdojo:id/etMessage";
    //public static final String txtMail = "com.mvp1ventures.masterdojo:id/etMessage";
    //public static final String refLink = "com.mvp1ventures.masterdojo:id/tvLink";
    public static final String btnNext = "com.mvp1ventures.masterdojo:id/btnSelectContact";
    public static final String selectAllContact = "com.mvp1ventures.masterdojo:id/tvSelectAll";
    public static final String btnSelect = "com.mvp1ventures.masterdojo:id/btnSelect";
    public static final String btnSendMsg = "com.android.mms:id/send_button";
    public static final String btnSendMail = "com.google.android.gm:id/send";
    public static final String btnSkip = "com.mvp1ventures.masterdojo:id/tvSkip";
    public static final String btnGoToRefHistory = "com.mvp1ventures.masterdojo:id/btnReferHistory";
    public static final String btnReferSocial = "com.mvp1ventures.masterdojo:id/btnReferSocial";
    public static final String btnResend = "com.mvp1ventures.masterdojo:id/btnReSend";
    public static final String contactList = "//androidx.recyclerview.widget.RecyclerView/android.widget.FrameLayout//android.widget.LinearLayout/android.widget.TextView";
    public static final String refOffPage = "com.mvp1ventures.masterdojo:id/imgShareOffering";
    /* ----------------------- End: Referrals --------------------------------------------------- */

    /* ----------------------- Start: Payment --------------------------------------------------- */
    public static final String btnAddCard = "com.mvp1ventures.masterdojo:id/btnAdd";
    public static final String btnPaymentM = "com.mvp1ventures.masterdojo:id/btnTabPaymentMethod";
    public static final String btnPaymentH = "com.mvp1ventures.masterdojo:id/btnTabPaymentHistory";
    public static final String cardList = "//androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout";
    public static final String txtCardNumber = "com.mvp1ventures.masterdojo:id/et_card_number";
    public static final String txtMonthYear = "com.mvp1ventures.masterdojo:id/et_expiry_date";
    public static final String txtCVV = "com.mvp1ventures.masterdojo:id/et_cvc_number";
    public static final String cardNumberList = "com.mvp1ventures.masterdojo:id/tvCardNumber";
    public static final String cardCVVList = "com.mvp1ventures.masterdojo:id/tvExpiry";
    public static final String isCardSelected = "com.mvp1ventures.masterdojo:id/ivSelected"; //androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout/android.widget.ImageView";
    public static final String btnSaveCard = "com.mvp1ventures.masterdojo:id/btnSave";
    public static final String pHistoryList = "//androidx.recyclerview.widget.RecyclerView/android.widget.FrameLayout";
    public static final String pHistoryOffList = "com.mvp1ventures.masterdojo:id/tvOfferingTitle";
    /* ----------------------- End: Payment --------------------------------------------------- */

    /* ----------------------- Start: Favourite --------------------------------------------------- */
    public static final String btnMaster = "com.mvp1ventures.masterdojo:id/btnTabMaster";
    public static final String btnOffering = "com.mvp1ventures.masterdojo:id/btnTabOfferings";
    public static final String btnContent = "com.mvp1ventures.masterdojo:id/btnTabContent";
    public static final String favList = "//androidx.recyclerview.widget.RecyclerView/android.widget.FrameLayout//android.widget.FrameLayout";
    public static final String contentFavList = "//androidx.recyclerview.widget.RecyclerView//android.widget.LinearLayout/android.widget.FrameLayout[2]";
    public static final String favEduDesc = "com.mvp1ventures.masterdojo:id/tvDescription";
    public static final String favCount = "//android.widget.TextView[@resource-id='com.mvp1ventures.masterdojo:id/nav_favourite']";
    /* ----------------------- End: Favourite --------------------------------------------------- */

    /* ----------------------- Start: Profile --------------------------------------------------- */
    public static final String btnEditProfile = "com.mvp1ventures.masterdojo:id/imgEdit";
    public static final String btnCommSwitch = "com.mvp1ventures.masterdojo:id/switchProfileShow";
    public static final String btnSelectPic = "com.android.documentsui:id/thumbnail";
    public static final String btnPicDone = "com.mvp1ventures.masterdojo:id/btn_done";
    public static final String btnPersonalD = "com.mvp1ventures.masterdojo:id/btnTabPersonal";
    public static final String btnBusiness = "com.mvp1ventures.masterdojo:id/btnTabBusiness";
    public static final String btnSkills = "com.mvp1ventures.masterdojo:id/btnTabSkills";
    public static final String firstName = "com.mvp1ventures.masterdojo:id/etFirstName";
    public static final String lastName = "com.mvp1ventures.masterdojo:id/etLastName";
    public static final String mobileNumber = "com.mvp1ventures.masterdojo:id/etMobileNumber";
    public static final String btnPDSave = "com.mvp1ventures.masterdojo:id/btnSave";
    public static final String btnCPass = "com.mvp1ventures.masterdojo:id/tvChangePass";
    public static final String txtOldPass = "com.mvp1ventures.masterdojo:id/etOldPass";
    public static final String txtNewPass = "com.mvp1ventures.masterdojo:id/etNewPass";
    public static final String TxtConfPass = "com.mvp1ventures.masterdojo:id/etConfPass";
    public static final String btnCPassword = "com.mvp1ventures.masterdojo:id/btnChange";
    public static final String txtSkill = "com.mvp1ventures.masterdojo:id/etAddHere";
    public static final String btnSkill = "com.mvp1ventures.masterdojo:id/imgAdd";
    public static final String bName = "com.mvp1ventures.masterdojo:id/etBusinessName";
    public static final String bIndustry = "com.mvp1ventures.masterdojo:id/etIndustry";
    public static final String bPosition = "com.mvp1ventures.masterdojo:id/etPositionTitle";
    public static final String bDesc = "com.mvp1ventures.masterdojo:id/etDescription";
    public static final String bUploadLogo = "com.mvp1ventures.masterdojo:id/imgLogoUpload";
    public static final String bBusinessUrl = "com.mvp1ventures.masterdojo:id/etBusinessURL";
    public static final String bLinkedInUrl = "com.mvp1ventures.masterdojo:id/etLinkdinURL";
    public static final String bFacebookUrl = "com.mvp1ventures.masterdojo:id/etFacebookURL";
    public static final String bInstagramUrl = "com.mvp1ventures.masterdojo:id/etInstaURL";
    public static final String bTwitterUrl = "com.mvp1ventures.masterdojo:id/etTwitterURL";
    public static final String bAddressL1 = "com.mvp1ventures.masterdojo:id/etBusinessAddressLine1";
    public static final String bAddressL2 = "com.mvp1ventures.masterdojo:id/etBusinessAddressLine2";
    public static final String bCity = "com.mvp1ventures.masterdojo:id/etBusinessCity";
    public static final String bSate = "com.mvp1ventures.masterdojo:id/etBusinessStateCity";
    public static final String bPostalCode = "com.mvp1ventures.masterdojo:id/etBusinessPostalCode";
    public static final String bCountry = "com.mvp1ventures.masterdojo:id/etBusinessCountry";
    public static final String bAboutPS = "com.mvp1ventures.masterdojo:id/etAboutBusiness";
    public static final String bHelp = "com.mvp1ventures.masterdojo:id/etHelpOthers";
    public static final String addSkillList = "//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mvp1ventures.masterdojo:id/rvSkillsSuggested']/android.widget.LinearLayout"; //com.mvp1ventures.masterdojo:id/llSkill
    public static final String addSkillFromSS = "com.mvp1ventures.masterdojo:id/imgAdd";
    public static final String removeSkillList = "//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mvp1ventures.masterdojo:id/rvSkills']/android.widget.LinearLayout";
    public static final String txtSkillName = "com.mvp1ventures.masterdojo:id/tvSkill";
    public static final String removeSkill = "com.mvp1ventures.masterdojo:id/imgDelete";
    public static final String str = "";
    /* ----------------------- End: Profile --------------------------------------------------- */


    /* ----------------------- Start: Testimonial --------------------------------------------------- */
    public static final String btnAddVideo = "com.mvp1ventures.masterdojo:id/imgAdd";
    public static final String btnRecord = "com.mvp1ventures.masterdojo:id/record_button";
    public static final String btnSwitchFB = "com.mvp1ventures.masterdojo:id/front_back_camera_switcher";
    public static final String btnCancelVideo = "com.mvp1ventures.masterdojo:id/cancel_media_action";
    public static final String btnRepeat = "com.mvp1ventures.masterdojo:id/re_take_media";
    public static final String btnSaveVideo = "com.mvp1ventures.masterdojo:id/confirm_media_result";
    public static final String txtTitleName = "com.mvp1ventures.masterdojo:id/tvTestimonialTitle";
    public static final String txtDesc = "com.mvp1ventures.masterdojo:id/tvTestimonialMsg";
    public static final String btnMarketing = "com.mvp1ventures.masterdojo:id/switchMarketing";
    public static final String btnCommunity = "com.mvp1ventures.masterdojo:id/switchSharing";
    public static final String btnFeedback = "com.mvp1ventures.masterdojo:id/switchFeedback";
    public static final String btnUpload = "com.mvp1ventures.masterdojo:id/btnUpload";
    public static final String btnReviewPlus = "com.mvp1ventures.masterdojo:id/imgAddTestimonial";
    public static final String EduNameRH = "com.mvp1ventures.masterdojo:id/tvUserName";
    /* ----------------------- End: Testimonial --------------------------------------------------- */


    /* ----------------------- Start: Educator Page --------------------------------------------------- */
    public static final String btnBackEdu = "//android.widget.ImageView[@content-desc='TODO']";
    public static final String searchTitle = "com.mvp1ventures.masterdojo:id/txtTitle";
    public static final String btnReadMore = "com.mvp1ventures.masterdojo:id/txtReadMore";
    public static final String txtReadMore = "com.mvp1ventures.masterdojo:id/tvShortDescription";
    public static final String descReadMore = "com.mvp1ventures.masterdojo:id/tvDescription";
    public static final String closeReadMore = "com.mvp1ventures.masterdojo:id/imgClose";
    public static final String btnPlayOfferingVideo = "com.mvp1ventures.masterdojo:id/imgPlay";
    public static final String eduDetailsPage = "com.mvp1ventures.masterdojo:id/llMainView";
    public static final String popOfferings = "com.mvp1ventures.masterdojo:id/recyclerPopularOffering";
    public static final String popOfferingList = "//android.widget.FrameLayout";
    public static final String popFav = "com.mvp1ventures.masterdojo:id/imgFavourite";
    public static final String popReferral = "com.mvp1ventures.masterdojo:id/imgShareOffering";

    public static final String btnAccessNow = "com.mvp1ventures.masterdojo:id/btnAction";
    public static final String btnMore = "com.mvp1ventures.masterdojo:id/btnTabMore";

    public static final String lblLastViewedContent = "com.mvp1ventures.masterdojo:id/tvLabelLastViewedContent";
    public static final String lblUpcoming = "com.mvp1ventures.masterdojo:id/tvLabelUpComing";
    public static final String lblTestimonials = "com.mvp1ventures.masterdojo:id/tvLabelTestimonials";
    public static final String lblContinueWith = "com.mvp1ventures.masterdojo:id/progress_bar_offer";
    public static final String lblPopularOffering = "com.mvp1ventures.masterdojo:id/tvLabelPopularOffering";

    public static final String continueWithSec = "com.mvp1ventures.masterdojo:id/recyclerInProgressOffering";
    public static final String cwOfferingName = "//android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView";
    public static final String cwSecCompleted = "//android.widget.FrameLayout/android.widget.LinearLayout/android.widget.TextView";
    public static final String cwOffering = "//android.widget.FrameLayout/android.widget.LinearLayout";

    public static final String lastViewedContentSec = "com.mvp1ventures.masterdojo:id/recyclerLastViewContent";
    public static final String lastViewedContentList = "//android.widget.FrameLayout";

    public static final String upcomingSec = "com.mvp1ventures.masterdojo:id/recyclerUpComingOffering";
    public static final String upcomingOffList = "//androidx.recyclerview.widget.RecyclerView[@resource-id='" + upcomingSec + "']/android.widget.RelativeLayout"; //"com.mvp1ventures.masterdojo:id/llUpComingBackground";
    public static final String upcomingOffName = "com.mvp1ventures.masterdojo:id/txtUpComingOfferingTitle";
    public static final String upcomingOffFav = "com.mvp1ventures.masterdojo:id/imgFavouriteUpComing";
    public static final String upcomingOffRef = "com.mvp1ventures.masterdojo:id/imgShareOfferingUpComing";
    public static final String upcomingOffRegister = "com.mvp1ventures.masterdojo:id/btnBuy";

    /* ----------------------- End: Educator Page --------------------------------------------------- */

    /* ----------------------- Start: Messages --------------------------------------------------- */
    public static final String userNameSec = "com.mvp1ventures.masterdojo:id/recyclerView";
    public static final String btnMessage = "com.mvp1ventures.masterdojo:id/btnTabMessages";
    public static final String btnRequest = "com.mvp1ventures.masterdojo:id/btnTabRequests";
    public static final String txtNoData = "com.mvp1ventures.masterdojo:id/tvNoData";
    public static final String EduMessageList = "com.mvp1ventures.masterdojo:id/llMainView";
    public static final String eduName = "com.mvp1ventures.masterdojo:id/txtTitle";
    public static final String txtMessage = "com.mvp1ventures.masterdojo:id/etMessage";
    public static final String btnSendMessage = "com.mvp1ventures.masterdojo:id/ivSendMessage";
    public static final String txtMessageInScreen = "com.mvp1ventures.masterdojo:id/message_text"; //com.mvp1ventures.masterdojo:id/tvTime
    public static final String chatScreen = "com.mvp1ventures.masterdojo:id/recyclerChat";
    public static final String chatScreenLastMessage = "//android.widget.LinearLayout";
    public static final String eduMsgCount = "com.mvp1ventures.masterdojo:id/txtCount";
    public static final String isEduBlockedM = "com.mvp1ventures.masterdojo:id/imgBlock";
    public static final String unblockEduM = "android:id/button1";
    public static final String unblockEduMCancel = "android:id/button2";
    public static final String EduUBMsg = "android:id/message";
    //This learner is blocked by you. Please unblock to send message.
    public static final String reqUserNameList = "//android.widget.FrameLayout";
    public static final String reqUserName = "com.mvp1ventures.masterdojo:id/txtTitle";
    public static final String requestAccept = "com.mvp1ventures.masterdojo:id/btnAccept";
    public static final String requestDelete = "com.mvp1ventures.masterdojo:id/btnCancel";
    /* ----------------------- End: Messages --------------------------------------------------- */

    /* ----------------------- Start: My Network --------------------------------------------------- */
    public static final String userListMN = "com.mvp1ventures.masterdojo:id/llMainView";
    public static final String userName = "com.mvp1ventures.masterdojo:id/txtTitle";
    public static final String userOption = "com.mvp1ventures.masterdojo:id/textViewOptions";
    public static final String removeConn = "//android.widget.TextView[@text='Remove connection']";
    public static final String sendMsg = "//android.widget.TextView[@text='Send message']";
    public static final String blockUser = "//android.widget.TextView[@text='Block']";
    public static final String unblockUser = "//android.widget.TextView[@text='Unblock']";
    public static final String addRemoveConn_profile = "com.mvp1ventures.masterdojo:id/tvAddRemoveMyNetwork";
    public static final String optionProfile = "com.mvp1ventures.masterdojo:id/btnMore";
    public static final String sendMsg_profile = "com.mvp1ventures.masterdojo:id/tvSendMessage";
    public static final String isBlockUser = "com.mvp1ventures.masterdojo:id/imgBlock";
    //public static final String unblockUser_profile = "//android.widget.TextView[@text='Unblock']";
    /* ----------------------- End: My Network --------------------------------------------------- */

    /* ----------------------- Start: Community --------------------------------------------------- */
    public static final String communityList = "com.mvp1ventures.masterdojo:id/llMainView";
    public static final String addToNetwork = "//android.widget.TextView[@text='Connect']";
    public static final String communityUserList = "com.mvp1ventures.masterdojo:id/rvCommunityMember";
    public static final String btnFilter = "com.mvp1ventures.masterdojo:id/imgFilter";
    public static final String btnApply = "com.mvp1ventures.masterdojo:id/btnApply";
    public static final String btnOnlineProgram = "com.mvp1ventures.masterdojo:id/btnTabOnlinePrograms";
    public static final String btnEvent = "com.mvp1ventures.masterdojo:id/btnTabEvent";
    public static final String filterOfferingList = "com.mvp1ventures.masterdojo:id/llMainView";
    public static final String txtOfferingName = "com.mvp1ventures.masterdojo:id/tvOfferingTitle";
    public static final String txtOfferingDate = "com.mvp1ventures.masterdojo:id/tvDate";
    public static final String txtBusinessName = "com.mvp1ventures.masterdojo:id/etBusinessName";
    public static final String txtIndustry = "com.mvp1ventures.masterdojo:id/etIndustry";
    public static final String txtCity = "com.mvp1ventures.masterdojo:id/etCity";
    public static final String txtCountry = "com.mvp1ventures.masterdojo:id/etCountry";
    public static final String txtSkills = "com.mvp1ventures.masterdojo:id/etSkills";
    public static final String txtParticipationYear = "com.mvp1ventures.masterdojo:id/etParticipationYear";
    public static final String txtKeywords = "com.mvp1ventures.masterdojo:id/etKeywords";
    public static final String txtProductAndService = "com.mvp1ventures.masterdojo:id/etProductAndService";
    public static final String btnCancel = "com.mvp1ventures.masterdojo:id/btnCancel";
    /* ----------------------- End: Community --------------------------------------------------- */

    /* ----------------------- Start: Success Message --------------------------------------------------- */
    public static final String reviewSuccessMsg = "Testimonial submission successful \\u2013 thank you!";
    public static final String passChangeSuccessMsg = "Action applied successfully";
    public static final String addProfileSuccessMsg = "Saved successfully";
    public static final String personalDetailsSaved = "Hooray! new update on your profile successful!";
    public static final String blockLearnerMsg = "Learner blocked";
    public static final String unblockLearnerMsg = "Learner unblocked";
    public static final String removeFromNetworkMsg = "This learner has been removed";
    public static final String addToNetworkMsg = "Request sent";
    public static final String addToNetworkMsg2 = "Request sent ! Learner will be added to your network once your request is accepted by the learner.";
    public static final String addToNetworkMsg3 = "This learner has already sent you a request. Please accept the request.";
    public static final String addToNetworkMsg4 = "You have been blocked by this user.";
    public static final String addToNetworkMsg5 = "This learner is already added in your network.";
    public static final String searchResult0Msg = "Search returned no matches.";
    public static final String paymentSuccessMsg = "payment successfully processed";
    public static final String addCardSuccessMsg = "Successfully saved";
    public static final String requestDeleted = "Request deleted";
    public static final String requestAccepted = "Request accepted";
    public static final String downloadContentMsg = "successfully downloaded";
    public static final String sentSupportRequestMsg = "Support.request.has.been.sent";
    public static final String forgotPassMsg = "Forgot password request has been received successfully. Kindly check your registered email inbox and follow the instruction.";
    public static final String mailExitMsg = "This email address has already been registered";
    /* ----------------------- End: Success Message --------------------------------------------------- */

    public static final String expEdus = "com.mvp1ventures.masterdojo:id/linearLayout";
    //com.mvp1ventures.masterdojo:id/imgShare
    //com.mvp1ventures.masterdojo:id/imgSupport

    public static final String btnBack2 = "com.mvp1ventures.masterdojo:id/imgBack";
    public static final String btnAddToCalender = "com.mvp1ventures.masterdojo:id/tvAddToCalendar";
    public static final String btnExpand = "com.mvp1ventures.masterdojo:id/imgExpand";
    public static final String btnCalendar = "android:id/icon";
    public static final String btnCalendarAdded = "com.android.calendar:id/action_done";
    public static final String priceOffering = "com.mvp1ventures.masterdojo:id/tvPayment";
    public static final String scheduleListEle = "com.mvp1ventures.masterdojo:id/recycler_custom_dialog_multi_tier";
    public static final String scheduleList = "//android.widget.RelativeLayout";
    public static final String sDate = "com.mvp1ventures.masterdojo:id/txtStartDate";
    public static final String eDate = "com.mvp1ventures.masterdojo:id/txtEndDate";
    //com.mvp1ventures.masterdojo:id/imgSelect
    public static final String chkTerms = "com.mvp1ventures.masterdojo:id/cbTerms";
    public static final String btnOK2 = "com.mvp1ventures.masterdojo:id/btnOK";

    public static final String txtPromoCode = "com.mvp1ventures.masterdojo:id/etPromoCode";
    public static final String txtDiscount = "com.mvp1ventures.masterdojo:id/tvDiscount";
    public static final String btnApplyPromoCode = "com.mvp1ventures.masterdojo:id/btnApplyPromoCode";
    public static final String btnPay = "com.mvp1ventures.masterdojo:id/btnProcess";
    public static final String btnAddCard_Payment = "com.mvp1ventures.masterdojo:id/btnAddPaymentMethod";

    public static final String btnNotification2 = "com.mvp1ventures.masterdojo:id/btnTabInbox";
    public static final String btnSupportReq = "com.mvp1ventures.masterdojo:id/btnTabSent";
    public static final String notificationSec = "com.mvp1ventures.masterdojo:id/recycler_notifications";
    public static final String notificationList = "com.mvp1ventures.masterdojo:id/swipe_layout";
    public static final String deleteNotification = "com.mvp1ventures.masterdojo:id/right_view";

    public static final String supportReqSec = "com.mvp1ventures.masterdojo:id/recyclerView";
    public static final String supportReqList = "//android.widget.FrameLayout";
    public static final String supportReqDate = "com.mvp1ventures.masterdojo:id/tvDate";
    public static final String supportReqEduName = "com.mvp1ventures.masterdojo:id/tvName";
    //public static final String supportReqAction = "com.mvp1ventures.masterdojo:id/imgAction";
    public static final String supportReqTitle = "com.mvp1ventures.masterdojo:id/etTitle";
    public static final String supportReqDesc = "com.mvp1ventures.masterdojo:id/etDescription";
    public static final String btnSentSupportRequest = "com.mvp1ventures.masterdojo:id/imgSupport";
    public static final String btnSRSubmit = "com.mvp1ventures.masterdojo:id/btnSubmit";

    public static final String btnSection = "com.mvp1ventures.masterdojo:id/btnTabSection";
    public static final String allSection = "com.mvp1ventures.masterdojo:id/recyclerAllSection";
    public static final String sectionList = "//android.widget.LinearLayout";
    public static final String sectionName = "";
    public static final String fileName = "com.mvp1ventures.masterdojo:id/tvFileName";
    public static final String fileSeen = "com.mvp1ventures.masterdojo:id/imgDone";
    public static final String btnDownload = "com.mvp1ventures.masterdojo:id/imgDownload";
    public static final String btnArrowNext = "com.mvp1ventures.masterdojo:id/imgNextAll";
    public static final String btnArrowPrevious = "com.mvp1ventures.masterdojo:id/imgPreviousAll";
    public static final String btnNextSection = "com.mvp1ventures.masterdojo:id/btnNextSection";
    public static final String btnPermissionAllow = "com.android.packageinstaller:id/permission_allow_button";

    public static final String btnForgotPass = "com.mvp1ventures.masterdojo:id/btnForgotPassword";
    public static final String txtEmail = "com.mvp1ventures.masterdojo:id/etEmail";
    public static final String btnSentMal = "com.mvp1ventures.masterdojo:id/btnSend";

    public static final String btnSignUp = "com.mvp1ventures.masterdojo:id/btnDontHaveAccount";
    public static final String btnNextScreen = "com.mvp1ventures.masterdojo:id/btnNext";
    public static final String btnBackScreen = "com.mvp1ventures.masterdojo:id/ic_back";
    public static final String btnSignUp2 = "com.mvp1ventures.masterdojo:id/btnSignUpNow";


    public static final String btnOpenEduRH = "com.mvp1ventures.masterdojo:id/imgAerrow";
    public static final String eduListRefHistory = "//androidx.recyclerview.widget.RecyclerView[@resource-id='com.mvp1ventures.masterdojo:id/rvSubscriber']/android.widget.FrameLayout";

    /* ----------------------- Start:  --------------------------------------------------- */
    /* ----------------------- End:  --------------------------------------------------- */
}
