package pages;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import org.testng.Assert;
import others.Constants;
import testBase.TestBase;

import java.io.UnsupportedEncodingException;
import java.util.List;

public class ProfilePage extends TestBase {

    public void addUpdateProfilePic(AndroidDriver driver, ExtentTest logger) throws InterruptedException, UnsupportedEncodingException {
        waitUntilAndroidElementVisibleByID(driver, Constants.btnEditProfile);
        driver.findElementById(Constants.btnEditProfile).click();
        waitUntilAndroidElementVisibleByID(driver, Constants.btnSelectPic);
        driver.findElementById(Constants.btnSelectPic).click();
        logger.log(Status.INFO, "Profile picture selected");
        waitUntilAndroidElementVisibleByID(driver, Constants.btnPicDone);
        driver.findElementById(Constants.btnPicDone).click();
        logger.log(Status.INFO, "Profile picture uploaded");
        validatePopScreen(driver, Constants.addProfileSuccessMsg);

    }

    public void changePassword(AndroidDriver driver, ExtentTest logger) throws InterruptedException, UnsupportedEncodingException {
        waitUntilAndroidElementVisibleByID(driver, Constants.btnCPass);
        driver.findElementById(Constants.btnCPass).click();
        waitUntilAndroidElementVisibleByID(driver, Constants.txtOldPass);
        logger.log(Status.INFO, "Change password screen opened");
        driver.findElementById(Constants.txtOldPass).sendKeys("1234567890");
        driver.findElementById(Constants.txtNewPass).sendKeys("1234567890");
        driver.findElementById(Constants.TxtConfPass).sendKeys("1234567890");
        waitInSeconds(1);
        driver.findElementById(Constants.btnCPassword).click();
        logger.log(Status.INFO, "Change password button clicked.");
        validatePopScreen(driver, Constants.passChangeSuccessMsg);
    }

    public void changeProfileVisibility(AndroidDriver driver, ExtentTest logger, String status) throws InterruptedException {
        waitUntilAndroidElementVisibleByID(driver, Constants.btnCommSwitch);
        MobileElement commSwitch = (MobileElement) driver.findElementById(Constants.btnCommSwitch);
        //System.out.println(driver.findElementById(Constants.btnCommSwitch).getAttribute("checked"));
        waitUntilAndroidElementVisibleByID(driver, Constants.btnCommSwitch);
        if (status.toLowerCase().equals("on") && commSwitch.getAttribute("checked").equals("false")) {
            driver.findElementById(Constants.btnCommSwitch).click();
            logger.log(Status.INFO, "Profile visibility set to ON.");
        } else if (status.toLowerCase().equals("off") && commSwitch.getAttribute("checked").equals("true")) {
            driver.findElementById(Constants.btnCommSwitch).click();
            logger.log(Status.INFO, "Profile visibility set to OFF.");
        }
    }

    public void addUpdatePersonalDetails(AndroidDriver driver, ExtentTest logger) throws InterruptedException, UnsupportedEncodingException {
        waitUntilAndroidElementVisibleByID(driver, Constants.btnPersonalD);
        String str = "";
        int cCount = 0;
        boolean isFound = false;
        driver.findElementById(Constants.btnPersonalD).click();
        waitUntilAndroidElementVisibleByID(driver, Constants.firstName);
        driver.findElementById(Constants.firstName).clear();
        driver.findElementById(Constants.lastName).clear();
        driver.findElementById(Constants.mobileNumber).clear();
        driver.findElementById(Constants.firstName).sendKeys("Kanak");
        driver.findElementById(Constants.lastName).sendKeys("Bhuva");
        waitUntilAndroidElementVisibleByID(driver, Constants.countryCodeDD);
        driver.findElementById(Constants.countryCodeDD).click();
        Thread.sleep(3000);
        driver.findElementById(Constants.searchCountryCC).sendKeys("Australia");
        cCount = driver.findElementsByXPath(Constants.searchCountryList).size();
        for (int ci = 1; ci <= cCount; ci++) {
            str = "//androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[" + ci + "]/android.widget.TextView";
            if (driver.findElementByXPath(str).getText().trim().equals("Australia (AU)")) {
                driver.findElementByXPath(str).click();
                isFound = true;
            }
        }
        if (!isFound) {
            driver.pressKey(new KeyEvent().withKey(AndroidKey.ESCAPE));
        }
        driver.findElementById(Constants.mobileNumber).sendKeys("8866014335");
        driver.findElementById(Constants.btnPDSave).click();
        validatePopScreen(driver, Constants.personalDetailsSaved);
        logger.log(Status.INFO, "Personal details added.");
    }

    public void addUpdateBusinessDetails(AndroidDriver driver, ExtentTest logger) throws InterruptedException, UnsupportedEncodingException {
        waitUntilAndroidElementVisibleByID(driver, Constants.btnPersonalD);
        driver.findElementById(Constants.btnBusiness).click();
        waitUntilAndroidElementVisibleByID(driver, Constants.bName);
        driver.findElementById(Constants.bName).sendKeys("Mvp");
        driver.findElementById(Constants.bIndustry).sendKeys("Airlines");
        driver.findElementById(Constants.bPosition).sendKeys("Automation Engineer");
        driver.findElementById(Constants.bDesc).sendKeys("Web and mobile automation. ");
        scrollById(driver, Constants.bInstagramUrl);
        driver.findElementById(Constants.bBusinessUrl).sendKeys("https://test.learner.masterdojo.io/lerner/profile");
        driver.findElementById(Constants.bLinkedInUrl).sendKeys("https://www.linkedin.com/in/kanak-bhuva-5b465a102");
        //scrollById(driver, Constants.bInstagramUrl);
        driver.findElementById(Constants.bFacebookUrl).sendKeys("https://www.facebook.com/kanak79");
        driver.findElementById(Constants.bInstagramUrl).sendKeys("https://instagram.com/kanakbhuva");
        scrollById(driver, Constants.bCity);
        driver.findElementById(Constants.bTwitterUrl).sendKeys("https://twitter.com/kanak797?s=08");
        driver.findElementById(Constants.bAddressL1).sendKeys("Parth Indraprastha Tower");
        driver.findElementById(Constants.bAddressL2).sendKeys("Vastrapur");
        driver.findElementById(Constants.bCity).sendKeys("Ahmedabad");
        scrollById(driver, Constants.bAboutPS);
        driver.findElementById(Constants.bSate).sendKeys("Gujarat");
        driver.findElementById(Constants.bPostalCode).sendKeys("380059");
        driver.findElementById(Constants.bCountry).sendKeys("India");
        driver.findElementById(Constants.bAboutPS).sendKeys("Automation");
        scrollById(driver, Constants.bHelp);
        driver.findElementById(Constants.bHelp).sendKeys("Automation");
        scrollById(driver, Constants.bUploadLogo);
        driver.findElementById(Constants.bUploadLogo).click();
        waitUntilAndroidElementVisibleByID(driver, Constants.btnSelectPic);
        driver.findElementById(Constants.btnSelectPic).click();
        waitUntilAndroidElementVisibleByID(driver, Constants.btnPicDone);
        driver.findElementById(Constants.btnPicDone).click();
        waitUntilAndroidElementVisibleByID(driver, Constants.btnPDSave);
        driver.findElementById(Constants.btnPDSave).click();
        validatePopScreen(driver, Constants.personalDetailsSaved);
        logger.log(Status.INFO, "Business details added.");
    }

    public void addSkill(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        String skillName = "Automation";
        waitUntilAndroidElementVisibleByID(driver, Constants.btnSkills);
        driver.findElementById(Constants.btnSkills).click();
        waitUntilAndroidElementVisibleByID(driver, Constants.txtSkill);
        driver.findElementById(Constants.txtSkill).sendKeys(skillName);
        driver.findElementById(Constants.btnSkill).click();
        logger.log(Status.INFO, "Skill added successfully.");
        Assert.assertEquals(true, isSkillAdded(driver, logger, skillName));
        logger.log(Status.INFO, "Skill added and verified successfully in my skill.");
    }

    public void addSuggestedSkill(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        String skillName = "Time management";
        waitUntilAndroidElementVisibleByID(driver, Constants.btnSkills);
        driver.findElementById(Constants.btnSkills).click();
        waitUntilAndroidElementVisibleByID(driver, Constants.txtSkill);
        java.util.List<MobileElement> sSkills = (List<MobileElement>) driver.findElementsByXPath(Constants.addSkillList);
        System.out.println(sSkills.size());
        for (MobileElement ss : sSkills) {
            //System.out.println(ss.findElementById(Constants.txtSkillName).getText().trim());
            if (ss.findElementById(Constants.txtSkillName).getText().trim().equals(skillName)) {
                ss.findElementById(Constants.addSkillFromSS).click();
                logger.log(Status.INFO, "Skill added successfully.");
                Assert.assertEquals(true, isSkillAdded(driver, logger, skillName));
                logger.log(Status.INFO, "Skill added and verified successfully in my skill.");
                break;
            }
        }
    }

    public void removeSkill(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        String skillName = "Time management";
        waitUntilAndroidElementVisibleByID(driver, Constants.btnSkills);
        driver.findElementById(Constants.btnSkills).click();
        waitUntilAndroidElementVisibleByID(driver, Constants.txtSkill);
        java.util.List<MobileElement> sSkills = (List<MobileElement>) driver.findElementsByXPath(Constants.removeSkillList);
        System.out.println(sSkills.size());
        for (MobileElement ss : sSkills) {
            //System.out.println(ss.findElementById(Constants.txtSkillName).getText().trim());
            if (ss.findElementById(Constants.txtSkillName).getText().trim().equals(skillName)) {
                ss.findElementById(Constants.removeSkill).click();
                logger.log(Status.INFO, "Skill removed successfully.");
                Assert.assertEquals(false, isSkillAdded(driver, logger, skillName));
                logger.log(Status.INFO, "Skill removed and verified successfully in my skill.");
                break;
            }
        }
    }

    public boolean isSkillAdded(AndroidDriver driver, ExtentTest logger, String skill) throws InterruptedException {
        waitUntilAndroidElementVisibleByID(driver, Constants.btnSkills);
        driver.findElementById(Constants.btnSkills).click();
        waitUntilAndroidElementVisibleByID(driver, Constants.txtSkill);
        java.util.List<MobileElement> sSkills = (List<MobileElement>) driver.findElementsByXPath(Constants.removeSkillList);
        System.out.println(sSkills.size());
        for (MobileElement ss : sSkills) {
            //System.out.println(ss.findElementById(Constants.txtSkillName).getText().trim());
            if (ss.findElementById(Constants.txtSkillName).getText().trim().equals(skill)) {
                logger.log(Status.INFO, "Skill found.");
                return true;
            }
        }
        return false;
    }

}
