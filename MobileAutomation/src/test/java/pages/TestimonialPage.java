package pages;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import others.Constants;
import testBase.TestBase;

import java.io.UnsupportedEncodingException;

public class TestimonialPage extends TestBase {

    EducatorPage educator = new EducatorPage();

    public void addTestimonialFromReviewPage(AndroidDriver driver, ExtentTest logger) throws InterruptedException, UnsupportedEncodingException {
        searchAndClick(driver, Constants.eleScrollAndFind, Constants.EduNameRH, "Mvp1ventures");
        waitUntilAndroidElementVisibleByID(driver, Constants.btnAddVideo);
        driver.findElementById(Constants.btnAddVideo).click();
        addTestimonial(driver, logger);
        waitUntilAndroidElementVisibleByxPath(driver, Constants.btnBack);
        driver.findElementByXPath(Constants.btnBack).click();
        waitUntilAndroidElementVisibleByID(driver, Constants.btnHomeTB);
        driver.findElementById(Constants.btnHomeTB).click();
        waitInSeconds(1);
    }

    public void addTestimonialFromEducatorPage(AndroidDriver driver, ExtentTest logger) throws InterruptedException, UnsupportedEncodingException {
        String eduCName = "Mvp1ventures";
        String txtSearch = "Mvp1";
        educator.openEducatorPage(driver, logger, txtSearch, eduCName);
        scrollToElement(driver, "Testimonials");
        waitUntilAndroidElementVisibleByID(driver, Constants.btnReviewPlus);
        driver.findElementById(Constants.btnReviewPlus).click();
        addTestimonial(driver, logger);
        waitUntilAndroidElementVisibleByxPath(driver, Constants.btnBackEdu);
        driver.findElementByXPath(Constants.btnBackEdu).click();
    }

    public void addTestimonial(AndroidDriver driver, ExtentTest logger) throws InterruptedException, UnsupportedEncodingException {
        waitInSeconds(5);
        String statusMarketing = "On";
        String statusCommunity = "On";
        String statusFeedback = "On";

        waitUntilAndroidElementVisibleByID(driver, Constants.btnRecord);
        logger.log(Status.INFO,"Record screen displayed");
        driver.findElementById(Constants.btnRecord).click();
        logger.log(Status.INFO,"Recording started");
        waitInSeconds(5);
        driver.findElementById(Constants.btnRecord).click();
        logger.log(Status.INFO,"Recording stopped");
        waitUntilAndroidElementVisibleByID(driver, Constants.btnSaveVideo);
        driver.findElementById(Constants.btnSaveVideo).click();
        waitUntilAndroidElementVisibleByID(driver, Constants.btnUpload);
        logger.log(Status.INFO,"Video uploaded successfully");
        logger.log(Status.INFO,"Add testimonial details screen displayed");
        driver.findElementById(Constants.txtTitleName).sendKeys("Test");
        driver.findElementById(Constants.txtDesc).sendKeys("Test desc");

        MobileElement marketingSwitch = (MobileElement) driver.findElementById(Constants.btnMarketing);
        if (statusMarketing.toLowerCase().equals("on") && marketingSwitch.getAttribute("checked").equals("false")) {
            driver.findElementById(Constants.btnMarketing).click();
        } else if (statusMarketing.toLowerCase().equals("off") && marketingSwitch.getAttribute("checked").equals("true")) {
            driver.findElementById(Constants.btnMarketing).click();
        }
        MobileElement commSwitch = (MobileElement) driver.findElementById(Constants.btnCommunity);
        if (statusCommunity.toLowerCase().equals("on") && commSwitch.getAttribute("checked").equals("false")) {
            driver.findElementById(Constants.btnCommunity).click();
        } else if (statusCommunity.toLowerCase().equals("off") && commSwitch.getAttribute("checked").equals("true")) {
            driver.findElementById(Constants.btnCommunity).click();
        }
        MobileElement feedbackSwitch = (MobileElement) driver.findElementById(Constants.btnFeedback);
        if (statusFeedback.toLowerCase().equals("on") && feedbackSwitch.getAttribute("checked").equals("false")) {
            driver.findElementById(Constants.btnFeedback).click();
        } else if (statusFeedback.toLowerCase().equals("off") && feedbackSwitch.getAttribute("checked").equals("true")) {
            driver.findElementById(Constants.btnFeedback).click();
        }

        driver.findElementById(Constants.btnUpload).click();
        validatePopScreen(driver, Constants.reviewSuccessMsg);
        logger.log(Status.INFO,"Testimonial submitted");
    }
}
