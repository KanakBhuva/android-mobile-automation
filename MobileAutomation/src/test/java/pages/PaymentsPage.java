package pages;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import org.testng.Assert;
import others.Constants;
import testBase.TestBase;

import java.io.UnsupportedEncodingException;
import java.util.List;

public class PaymentsPage extends TestBase {

    public void addCard(AndroidDriver driver, ExtentTest logger) throws InterruptedException, UnsupportedEncodingException {
        waitUntilAndroidElementVisibleByID(driver, Constants.btnPaymentM);
        driver.findElementById(Constants.btnPaymentM).click();
        waitUntilAndroidElementVisibleByID(driver, Constants.btnAddCard);
        driver.findElementById(Constants.btnAddCard).click();
        waitUntilAndroidElementVisibleByID(driver, Constants.txtCardNumber);
        logger.log(Status.INFO,"Add card screen displayed");
        driver.findElementById(Constants.txtCardNumber).sendKeys("5555555555554444");
        driver.findElementById(Constants.txtMonthYear).sendKeys("1222");
        driver.findElementById(Constants.txtCVV).sendKeys("111");
        driver.hideKeyboard();
        driver.findElementById(Constants.btnSaveCard).click();
        validatePopScreen(driver, Constants.addCardSuccessMsg);
        logger.log(Status.INFO,"Card details submitted");
    }

    public void changeCard(AndroidDriver driver, ExtentTest logger) throws InterruptedException, UnsupportedEncodingException {
        waitUntilAndroidElementVisibleByID(driver, Constants.btnPaymentM);
        driver.findElementById(Constants.btnPaymentM).click();
        Thread.sleep(3000);
        List<MobileElement> cardList = (List<MobileElement>) driver.findElementsByXPath(Constants.cardList);
        System.out.println(cardList.size());
        for(MobileElement me: cardList){
            //System.out.println(me.findElementById(Constants.cardNumberList).getText());
            //System.out.println(me.findElementById(Constants.cardCVVList).getText());
            if(me.findElementsById(Constants.isCardSelected).size() == 0){
                me.findElementById(Constants.cardNumberList).click();
                validatePopScreen(driver, Constants.addCardSuccessMsg);
                logger.log(Status.INFO,"Card changed");
                waitUntilAndroidElementVisibleByxPath(driver, Constants.btnBack);
                driver.findElementByXPath(Constants.btnBack).click();
                waitUntilAndroidElementVisibleByID(driver, Constants.btnHomeTB);
                driver.findElementById(Constants.btnHomeTB).click();
                break;
            } else {
                logger.log(Status.INFO,"Card already selected.");
            }
        }
        /*System.out.println(driver.findElementsById(Constants.cardNumberList).size());
        System.out.println(driver.findElementById(Constants.cardNumberList).getText()); // word.substring(word.length() - 3) //StringUtils.right("abcdef", 3)
        System.out.println(driver.findElementById(Constants.cardCVVList).getText());
        if(driver.findElementsByXPath(Constants.isCardSelected).size() == 0){
            driver.findElementById(Constants.cardNumberList).click();
        } else {
            System.out.println("Card already selected.");
        }*/
    }

    public void readPaymentHistory(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        waitUntilAndroidElementVisibleByID(driver, Constants.btnPaymentH);
        driver.findElementById(Constants.btnPaymentH).click();
        waitUntilAndroidElementVisibleByID(driver, Constants.pHistoryOffList);
        System.out.println(driver.findElementsById(Constants.pHistoryOffList).size());
        System.out.println(driver.findElementById(Constants.pHistoryOffList).getText());
    }


}
