package pages;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import org.testng.Assert;
import others.Constants;
import testBase.TestBase;

import java.io.UnsupportedEncodingException;
import java.util.List;

public class MyNetworkPage extends TestBase {

    public MobileElement readUserFromMyNetwork(AndroidDriver driver, ExtentTest logger, String userName) throws InterruptedException {
        if(driver.findElementsById(Constants.txtNoData).size() != 1) {
            waitUntilAndroidElementVisibleByID(driver, Constants.userListMN);
            java.util.List<MobileElement> userList = (List<MobileElement>) driver.findElementsById(Constants.userListMN);
            //System.out.println(userList.size());
            for (MobileElement ul : userList) {
                if (ul.findElementById(Constants.userName).getText().trim().equals(userName)) {
                    return (MobileElement) ul;
                }
            }
        } else {
            logger.log(Status.INFO, "No learner added to network.");
        }
        return null;
    }

    public void removeConnection_MyNetwork(AndroidDriver driver, ExtentTest logger) throws InterruptedException, UnsupportedEncodingException {
        String userName = "Bhavin Bhuva";
        MobileElement ele = readUserFromMyNetwork(driver, logger, userName);
        if (ele != null) {
            waitUntilAndroidElementVisibleByID(driver, Constants.userOption);
            ele.findElementById(Constants.userOption).click();
            logger.log(Status.INFO, "Option button clicked");
            waitUntilAndroidElementVisibleByxPath(driver, Constants.removeConn);
            if (driver.findElementByXPath(Constants.removeConn).getText().trim().equals("Remove connection")) {
                driver.findElementByXPath(Constants.removeConn).click();
                logger.log(Status.INFO, "Remove connection button clicked");
                validatePopScreen(driver, Constants.removeFromNetworkMsg);
            }
            goToHomeInAndroid(driver, logger);
        }
    }

    public void sendMessage_MyNetwork(AndroidDriver driver, ExtentTest logger) throws InterruptedException, UnsupportedEncodingException {
        String userName = "Bhavin Bhuva";
        String message = "Hi! How are you?";
        String sentMessage = "";
        MobileElement ele = readUserFromMyNetwork(driver, logger, userName);
        if (ele != null) {
            waitUntilAndroidElementVisibleByID(driver, Constants.userOption);
            ele.findElementById(Constants.userOption).click();
            waitUntilAndroidElementVisibleByxPath(driver, Constants.sendMsg);
            driver.findElementByXPath(Constants.sendMsg).click();
            logger.log(Status.INFO, "Message screen opened");
            if (driver.findElementsById(Constants.txtNoData).size() != 1) {
                waitUntilAndroidElementVisibleByID(driver, Constants.txtMessage);
                driver.findElementById(Constants.txtMessage).sendKeys(message);
                driver.findElementById(Constants.btnSendMessage).click();
                logger.log(Status.INFO, "Sending ...");
                waitUntilAndroidElementVisibleByID(driver, Constants.btnBack2);
                MobileElement ml = (MobileElement) driver.findElementById(Constants.chatScreen);
                List<MobileElement> msgList = ml.findElementsByXPath(Constants.chatScreenLastMessage);
                sentMessage = msgList.get(msgList.size() - 1).findElementById(Constants.txtMessageInScreen).getText();
                //System.out.println(sentMessage);
                Assert.assertEquals(sentMessage, message);
                logger.log(Status.INFO, "Verified: Message sent");
                driver.findElementById(Constants.btnBack2).click();
                waitUntilAndroidElementVisibleByID(driver, Constants.userOption);
            }
        }
    }

    public void blockUser_MyNetwork(AndroidDriver driver, ExtentTest logger) throws InterruptedException, UnsupportedEncodingException {
        String userName = "Bhavin Bhuva";
        MobileElement ele = readUserFromMyNetwork(driver, logger, userName);
        if (ele != null) {
            waitUntilAndroidElementVisibleByID(driver, Constants.userOption);
            ele.findElementById(Constants.userOption).click();
            logger.log(Status.INFO, "Option button clicked");
            waitUntilAndroidElementVisibleByxPath(driver, Constants.blockUser);
            driver.findElementByXPath(Constants.blockUser).click();
            logger.log(Status.INFO, "Block button clicked");
            validatePopScreen(driver, Constants.blockLearnerMsg);
        }
    }

    public void unblockUser_MyNetwork(AndroidDriver driver, ExtentTest logger) throws InterruptedException, UnsupportedEncodingException {
        String userName = "Bhavin Bhuva";
        MobileElement ele = readUserFromMyNetwork(driver, logger, userName);
        if (ele != null) {
            waitUntilAndroidElementVisibleByID(driver, Constants.userOption);
            ele.findElementById(Constants.userOption).click();
            logger.log(Status.INFO, "Option button clicked");
            waitUntilAndroidElementVisibleByxPath(driver, Constants.unblockUser);
            driver.findElementByXPath(Constants.unblockUser).click();
            logger.log(Status.INFO, "Unblock button clicked");
            validatePopScreen(driver, Constants.unblockLearnerMsg);
        }
    }

    public void removeConnection_MyNetwork_Profile(AndroidDriver driver, ExtentTest logger) throws InterruptedException, UnsupportedEncodingException {
        String userName = "Bhavin Bhuva";
        MobileElement ele = readUserFromMyNetwork(driver, logger, userName);
        if (ele != null) {
            ele.click();
            waitUntilAndroidElementVisibleByID(driver, Constants.addRemoveConn_profile);
            logger.log(Status.INFO, "User profile opened");
            if (driver.findElementById(Constants.addRemoveConn_profile).getText().trim().equals("Remove connection")) {
                driver.findElementById(Constants.addRemoveConn_profile).click();
                logger.log(Status.INFO, "Remove connection button clicked");
                validatePopScreen(driver, Constants.removeFromNetworkMsg);
            }
            waitUntilAndroidElementVisibleByxPath(driver, Constants.btnBack);
            driver.findElementByXPath(Constants.btnBack).click();
        }
    }

    public void sendMessage_MyNetwork_Profile(AndroidDriver driver, ExtentTest logger) throws InterruptedException, UnsupportedEncodingException {
        String userName = "Bhavin Bhuva";
        String message = "Hi! How are you?";
        String sentMessage = "";
        MobileElement ele = readUserFromMyNetwork(driver, logger, userName);
        if (ele != null) {
            ele.click();
            waitUntilAndroidElementVisibleByID(driver, Constants.sendMsg_profile);
            logger.log(Status.INFO, "User profile opened");
            driver.findElementById(Constants.sendMsg_profile).click();
            logger.log(Status.INFO, "Message screen opened");
            if (driver.findElementsById(Constants.txtNoData).size() != 1) {
                waitUntilAndroidElementVisibleByID(driver, Constants.txtMessage);
                driver.findElementById(Constants.txtMessage).sendKeys(message);
                driver.findElementById(Constants.btnSendMessage).click();
                logger.log(Status.INFO, "Sending ...");
                waitUntilAndroidElementVisibleByID(driver, Constants.btnBack2);
                MobileElement ml = (MobileElement) driver.findElementById(Constants.chatScreen);
                List<MobileElement> msgList = ml.findElementsByXPath(Constants.chatScreenLastMessage);
                sentMessage = msgList.get(msgList.size() - 1).findElementById(Constants.txtMessageInScreen).getText();
                //System.out.println(sentMessage);
                Assert.assertEquals(sentMessage, message);
                logger.log(Status.INFO, "Verified: Message sent");
                driver.findElementById(Constants.btnBack2).click();
                waitUntilAndroidElementVisibleByxPath(driver, Constants.btnBack);
                driver.findElementByXPath(Constants.btnBack).click();
            }
        }
    }

    public void blockUser_MyNetwork_Profile(AndroidDriver driver, ExtentTest logger) throws InterruptedException, UnsupportedEncodingException {
        String userName = "Bhavin Bhuva";
        MobileElement ele = readUserFromMyNetwork(driver, logger, userName);
        if (ele != null) {
            ele.click();
            waitUntilAndroidElementVisibleByID(driver, Constants.optionProfile);
            logger.log(Status.INFO, "User profile opened");
            driver.findElementById(Constants.optionProfile).click();
            waitUntilAndroidElementVisibleByxPath(driver, Constants.blockUser);
            driver.findElementByXPath(Constants.blockUser).click();
            logger.log(Status.INFO, "Block button clicked");
            validatePopScreen(driver, Constants.blockLearnerMsg);
            waitUntilAndroidElementVisibleByxPath(driver, Constants.btnBack);
            driver.findElementByXPath(Constants.btnBack).click();
        }
    }

    public void unblockUser_MyNetwork_Profile(AndroidDriver driver, ExtentTest logger) throws InterruptedException, UnsupportedEncodingException {
        String userName = "Bhavin Bhuva";
        MobileElement ele = readUserFromMyNetwork(driver, logger, userName);
        if (ele != null) {
            ele.click();
            waitUntilAndroidElementVisibleByID(driver, Constants.optionProfile);
            logger.log(Status.INFO, "User profile opened");
            driver.findElementById(Constants.optionProfile).click();
            logger.log(Status.INFO, "Option button clicked");
            waitUntilAndroidElementVisibleByxPath(driver, Constants.unblockUser);
            driver.findElementByXPath(Constants.unblockUser).click();
            logger.log(Status.INFO, "Unblock button clicked");
            validatePopScreen(driver, Constants.unblockLearnerMsg);
            waitUntilAndroidElementVisibleByxPath(driver, Constants.btnBack);
            driver.findElementByXPath(Constants.btnBack).click();
        }
    }

}
