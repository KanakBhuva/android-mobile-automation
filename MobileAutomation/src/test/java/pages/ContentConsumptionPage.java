package pages;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import others.Constants;
import testBase.TestBase;

import java.io.UnsupportedEncodingException;
import java.util.List;

public class ContentConsumptionPage extends TestBase {

    EducatorPage educator = new EducatorPage();

    public void verifyNextSectionButton(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        String txtSearch = "Mvp1";
        String eduCName = "Mvp1ventures";
        String offeringName = "Test";
        MobileElement oe = (MobileElement) educator.getOffering(driver, logger, txtSearch, eduCName, offeringName);
        if (oe != null) {
            oe.findElementById(Constants.titleName).click();
            logger.log(Status.INFO,"Offering opened");
            waitUntilAndroidElementVisibleByID(driver, Constants.btnAccessNow);
            driver.findElementById(Constants.btnAccessNow).click();
            waitUntilAndroidElementVisibleByID(driver, Constants.btnSection);
            logger.log(Status.INFO,"Content consumption page opened");
            MobileElement allSection = (MobileElement) driver.findElementById(Constants.allSection);
            List<MobileElement> lsSection = (List<MobileElement>) allSection.findElementsByXPath(Constants.sectionList);
            lsSection.get(0).findElementById(Constants.fileName).click();
            logger.log(Status.INFO,"File opened");
            waitUntilAndroidElementVisibleByxPath(driver, Constants.btnBack);
            if (driver.findElementsById(Constants.btnArrowNext).size() == 1) {
                while (driver.findElementsById(Constants.btnArrowNext).size() == 1) {
                    driver.findElementById(Constants.btnArrowNext).click();
                    waitInSeconds(3);
                }
            }
            if (driver.findElementsById(Constants.btnNextSection).size() == 1) {
                driver.findElementById(Constants.btnNextSection).click();
                logger.log(Status.INFO,"Next section button clicked");
            }
            waitUntilAndroidElementVisibleByxPath(driver, Constants.btnBack);
            driver.findElementByXPath(Constants.btnBack).click();
        }
    }

    public void downloadContent(AndroidDriver driver, ExtentTest logger) throws InterruptedException, UnsupportedEncodingException {
        String sectionName = "Section 1";
        String contentName = "ActionEdit.png";
        waitUntilAndroidElementVisibleByID(driver, Constants.btnSection);
        logger.log(Status.INFO,"Content consumption page opened");
        MobileElement allSection = (MobileElement) driver.findElementById(Constants.allSection);
        List<MobileElement> lsSection = (List<MobileElement>) allSection.findElementsByXPath(Constants.sectionList);
        System.out.println(lsSection.size());
        if (lsSection.get(0).findElementsById(Constants.btnDownload).size() > 0) {
            lsSection.get(0).findElementById(Constants.btnDownload).click();
            logger.log(Status.INFO,"Download buttton clicked");
            waitInSeconds(3);
            if (driver.findElementsById(Constants.btnPermissionAllow).size() == 1) {
                driver.findElementById(Constants.btnPermissionAllow).click();
            }
            validatePopScreen(driver, Constants.downloadContentMsg);
            waitUntilAndroidElementVisibleByxPath(driver, Constants.btnBack);
            driver.findElementByXPath(Constants.btnBack).click();
            waitUntilAndroidElementVisibleByxPath(driver, Constants.btnBack);
            driver.findElementByXPath(Constants.btnBack).click();
        }
    }

}
