package pages;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import org.testng.Assert;
import others.Constants;
import others.leftMenu;
import testBase.TestBase;

import java.util.List;

public class FavouritePage extends TestBase {

    EducatorPage educator = new EducatorPage();
    leftMenu lMenu = new leftMenu();
    ExplorePage ep = new ExplorePage();

    public void favouriteEducator_Home(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        String eduName = "Kanak Bhuva";
        String eduCName = "Mvp1ventures";
        scrollToElement(driver, eduCName);
        java.util.List<MobileElement> educatorsList = (List<MobileElement>) driver.findElementsByXPath(Constants.eleScrollAndFind);
        System.out.println(educatorsList.size());
        for (MobileElement el : educatorsList) {
            if (el.findElementsById(Constants.txtTitle).size() > 0) {
                if (el.findElementById(Constants.txtTitle).getText().trim().equals(eduCName)) {
                    waitUntilAndroidElementVisibleByID(driver, Constants.btnFavourite2);
                    el.findElementById(Constants.btnFavourite2).click();
                    logger.log(Status.INFO, "Educator favourited");
                    lMenu.displayLeftMenu(driver, logger);
                    lMenu.displayFavourites(driver, logger);
                    Assert.assertEquals(isEducatorFavourited(driver, logger, eduName, eduCName), true);
                    waitUntilAndroidElementVisibleByID(driver, Constants.btnHomeTB);
                    driver.findElementById(Constants.btnHomeTB).click();
                    break;
                }
            }
        }
    }

    public void unfavouriteEducator_Home(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        //String eduName = "varun Pandya";
        //String eduCName = "Varun1Pandya1";
        String eduName = "Kanak Bhuva";
        String eduCName = "Mvp1ventures";
        scrollToElement(driver, eduCName);
        java.util.List<MobileElement> educatorsList = (List<MobileElement>) driver.findElementsByXPath(Constants.eleScrollAndFind);
        System.out.println(educatorsList.size());
        for (MobileElement el : educatorsList) {
            if (el.findElementsById(Constants.txtTitle).size() > 0) {
                if (el.findElementById(Constants.txtTitle).getText().trim().equals(eduCName)) {
                    waitUntilAndroidElementVisibleByID(driver, Constants.btnFavourite2);
                    el.findElementById(Constants.btnFavourite2).click();
                    logger.log(Status.INFO, "Educator unfavourited");
                    lMenu.displayLeftMenu(driver, logger);
                    lMenu.displayFavourites(driver, logger);
                    Assert.assertEquals(isEducatorFavourited(driver, logger, eduName, eduCName), false);
                    waitUntilAndroidElementVisibleByID(driver, Constants.btnHomeTB);
                    driver.findElementById(Constants.btnHomeTB).click();
                    break;
                }
            }
        }
    }

    public void favouriteEduFromEducatorPage(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        //String eduName = "varun Pandya";
        //String eduCName = "Varun1Pandya1";
        String eduName = "Kanak Bhuva";
        String eduCName = "Mvp1ventures";
        String txtSearch = "Mvp1";
        educator.openEducatorPage(driver, logger, txtSearch, eduCName);
        driver.findElementById(Constants.btnFavourite3).click();
        waitUntilAndroidElementVisibleByID(driver, Constants.btnBack2);
        logger.log(Status.INFO, "Educator favourited");
        driver.findElementById(Constants.btnBack2).click();
        lMenu.displayLeftMenu(driver, logger);
        lMenu.displayFavourites(driver, logger);
        Assert.assertEquals(isEducatorFavourited(driver, logger, eduName, eduCName), true);
        waitUntilAndroidElementVisibleByID(driver, Constants.btnHomeTB);
        driver.findElementById(Constants.btnHomeTB).click();
    }

    public void unfavouriteEduFromEducatorPage(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        //String eduName = "varun Pandya";
        //String eduCName = "Varun1Pandya1";
        String eduName = "Kanak Bhuva";
        String eduCName = "Mvp1ventures";
        String txtSearch = "Mvp1";
        educator.openEducatorPage(driver, logger, txtSearch, eduCName);
        driver.findElementById(Constants.btnFavourite3).click();
        waitUntilAndroidElementVisibleByID(driver, Constants.btnBack2);
        logger.log(Status.INFO, "Educator unfavourited");
        driver.findElementById(Constants.btnBack2).click();
        lMenu.displayLeftMenu(driver, logger);
        lMenu.displayFavourites(driver, logger);
        Assert.assertEquals(isEducatorFavourited(driver, logger, eduName, eduCName), false);
        waitUntilAndroidElementVisibleByID(driver, Constants.btnHomeTB);
        driver.findElementById(Constants.btnHomeTB).click();
    }

    public void favouriteEduFromExplorePage(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        String eduName = "Angie Saunders";
        String eduCName = "Breathe Me";
        MobileElement edu = (MobileElement) ep.getEducatorFromExplorePage(driver, logger, eduCName);
        if (edu != null) {
            edu.findElementById(Constants.btnFavourite).click();
            logger.log(Status.INFO, "Educator favourited");
            lMenu.displayLeftMenu(driver, logger);
            lMenu.displayFavourites(driver, logger);
            Assert.assertEquals(isEducatorFavourited(driver, logger, eduName, eduCName), true);
            waitUntilAndroidElementVisibleByID(driver, Constants.btnHomeTB);
            driver.findElementById(Constants.btnHomeTB).click();
        }
    }

    public void unfavouriteEduFromExplorePage(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        String eduName = "Angie Saunders";
        String eduCName = "Breathe Me";
        MobileElement edu = (MobileElement) ep.getEducatorFromExplorePage(driver, logger, eduCName);
        if (edu != null) {
            edu.findElementById(Constants.btnFavourite).click();
            logger.log(Status.INFO, "Educator unfavourited");
            lMenu.displayLeftMenu(driver, logger);
            lMenu.displayFavourites(driver, logger);
            Assert.assertEquals(isEducatorFavourited(driver, logger, eduName, eduCName), false);
            waitUntilAndroidElementVisibleByID(driver, Constants.btnHomeTB);
            driver.findElementById(Constants.btnHomeTB).click();
        }
    }

    public void favouriteOfferingFromEducatorPage(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        //String txtSearch = "Varun1";
        //String eduName = "varun Pandya";
        //String eduCName = "Varun1Pandya1";
        String offeringName = "Test";
        String eduName = "Kanak Bhuva";
        String eduCName = "Mvp1ventures";
        String txtSearch = "Mvp1";
        MobileElement oe = (MobileElement) educator.getOffering(driver, logger, txtSearch, eduCName, offeringName);
        if (oe != null) {
            waitUntilAndroidElementVisibleByID(driver, Constants.btnFavourite3);
            oe.findElementById(Constants.btnFavourite3).click();
            waitUntilAndroidElementVisibleByID(driver, Constants.btnBack2);
            logger.log(Status.INFO, "Offering favourited");
            driver.findElementById(Constants.btnBack2).click();
            lMenu.displayLeftMenu(driver, logger);
            lMenu.displayFavourites(driver, logger);
            Assert.assertEquals(isOfferingFavourited(driver, logger, offeringName), true);
            waitUntilAndroidElementVisibleByID(driver, Constants.btnHomeTB);
            driver.findElementById(Constants.btnHomeTB).click();
        }
    }

    public void unfavouriteOfferingFromEducatorPage(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        //String txtSearch = "Varun1";
        //String eduName = "varun Pandya";
        //String eduCName = "Varun1Pandya1";
        String offeringName = "Test";
        String eduName = "Kanak Bhuva";
        String eduCName = "Mvp1ventures";
        String txtSearch = "Mvp1";
        MobileElement oe = (MobileElement) educator.getOffering(driver, logger, txtSearch, eduCName, offeringName);
        if (oe != null) {
            waitUntilAndroidElementVisibleByID(driver, Constants.btnFavourite3);
            oe.findElementById(Constants.btnFavourite3).click();
            waitUntilAndroidElementVisibleByID(driver, Constants.btnBack2);
            logger.log(Status.INFO, "Offering unfavourited");
            driver.findElementById(Constants.btnBack2).click();
            lMenu.displayLeftMenu(driver, logger);
            lMenu.displayFavourites(driver, logger);
            Assert.assertEquals(isOfferingFavourited(driver, logger, offeringName), false);
            waitUntilAndroidElementVisibleByID(driver, Constants.btnHomeTB);
            driver.findElementById(Constants.btnHomeTB).click();
        }
    }

    public void favouriteOfferingFromOfferingPage(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        //String txtSearch = "Varun1";
        //String eduName = "varun Pandya";
        //String eduCName = "Varun1Pandya1";
        String offeringName = "Test";
        String eduName = "Kanak Bhuva";
        String eduCName = "Mvp1ventures";
        String txtSearch = "Mvp1";
        MobileElement oe = (MobileElement) educator.getOffering(driver, logger, txtSearch, eduCName, offeringName);
        if (oe != null) {
            oe.findElementById(Constants.titleName).click();
            waitUntilAndroidElementVisibleByID(driver, Constants.btnFavourite3);
            driver.findElementById(Constants.btnFavourite3).click();
            waitUntilAndroidElementVisibleByxPath(driver, Constants.btnBack);
            logger.log(Status.INFO, "Offering favourited");
            driver.findElementByXPath(Constants.btnBack).click();
            waitUntilAndroidElementVisibleByID(driver, Constants.btnBack2);
            driver.findElementById(Constants.btnBack2).click();
            lMenu.displayLeftMenu(driver, logger);
            lMenu.displayFavourites(driver, logger);
            Assert.assertEquals(isOfferingFavourited(driver, logger, offeringName), true);
            waitUntilAndroidElementVisibleByID(driver, Constants.btnHomeTB);
            driver.findElementById(Constants.btnHomeTB).click();
        }
    }

    public void unfavouriteOfferingFromOfferingPage(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        //String txtSearch = "Varun1";
        //String eduName = "varun Pandya";
        //String eduCName = "Varun1Pandya1";
        String offeringName = "Test";
        String eduName = "Kanak Bhuva";
        String eduCName = "Mvp1ventures";
        String txtSearch = "Mvp1";
        MobileElement oe = (MobileElement) educator.getOffering(driver, logger, txtSearch, eduCName, offeringName);
        if (oe != null) {
            oe.findElementById(Constants.titleName).click();
            waitUntilAndroidElementVisibleByID(driver, Constants.btnFavourite3);
            driver.findElementById(Constants.btnFavourite3).click();
            waitUntilAndroidElementVisibleByxPath(driver, Constants.btnBack);
            logger.log(Status.INFO, "Offering unfavourited");
            driver.findElementByXPath(Constants.btnBack).click();
            waitUntilAndroidElementVisibleByID(driver, Constants.btnBack2);
            driver.findElementById(Constants.btnBack2).click();
            lMenu.displayLeftMenu(driver, logger);
            lMenu.displayFavourites(driver, logger);
            Assert.assertEquals(isOfferingFavourited(driver, logger, offeringName), false);
            waitUntilAndroidElementVisibleByID(driver, Constants.btnHomeTB);
            driver.findElementById(Constants.btnHomeTB).click();
        }
    }

    public void favouriteOfferingFromOfferingMorePage(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        //String txtSearch = "Varun1";
        //String eduName = "varun Pandya";
        //String eduCName = "Varun1Pandya1";
        String offeringName = "Test";
        String eduName = "Kanak Bhuva";
        String eduCName = "Mvp1ventures";
        String txtSearch = "Mvp1";
        MobileElement oe = (MobileElement) educator.getOffering(driver, logger, txtSearch, eduCName, offeringName);
        if (oe != null) {
            oe.findElementById(Constants.titleName).click();
            waitUntilAndroidElementVisibleByID(driver, Constants.btnAccessNow);
            driver.findElementById(Constants.btnAccessNow).click();
            waitUntilAndroidElementVisibleByID(driver, Constants.btnMore);
            driver.findElementById(Constants.btnMore).click();
            waitUntilAndroidElementVisibleByID(driver, Constants.btnFavourite3);
            driver.findElementById(Constants.btnFavourite3).click();
            waitUntilAndroidElementVisibleByxPath(driver, Constants.btnBack);
            logger.log(Status.INFO, "Offering favourited");
            driver.findElementByXPath(Constants.btnBack).click();
            waitUntilAndroidElementVisibleByxPath(driver, Constants.btnBack);
            driver.findElementByXPath(Constants.btnBack).click();
            waitUntilAndroidElementVisibleByID(driver, Constants.btnBack2);
            driver.findElementById(Constants.btnBack2).click();
            lMenu.displayLeftMenu(driver, logger);
            lMenu.displayFavourites(driver, logger);
            Assert.assertEquals(isOfferingFavourited(driver, logger, offeringName), true);
            waitUntilAndroidElementVisibleByID(driver, Constants.btnHomeTB);
            driver.findElementById(Constants.btnHomeTB).click();
        }
    }

    public void unfavouriteOfferingFromOfferingMorePage(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        //String txtSearch = "Varun1";
        //String eduName = "varun Pandya";
        //String eduCName = "Varun1Pandya1";
        String offeringName = "Test";
        String eduName = "Kanak Bhuva";
        String eduCName = "Mvp1ventures";
        String txtSearch = "Mvp1";
        MobileElement oe = (MobileElement) educator.getOffering(driver, logger, txtSearch, eduCName, offeringName);
        if (oe != null) {
            oe.findElementById(Constants.titleName).click();
            waitUntilAndroidElementVisibleByID(driver, Constants.btnAccessNow);
            driver.findElementById(Constants.btnAccessNow).click();
            waitUntilAndroidElementVisibleByID(driver, Constants.btnMore);
            driver.findElementById(Constants.btnMore).click();
            waitUntilAndroidElementVisibleByID(driver, Constants.btnFavourite3);
            driver.findElementById(Constants.btnFavourite3).click();
            waitUntilAndroidElementVisibleByxPath(driver, Constants.btnBack);
            logger.log(Status.INFO, "Offering unfavourited");
            driver.findElementByXPath(Constants.btnBack).click();
            waitUntilAndroidElementVisibleByxPath(driver, Constants.btnBack);
            driver.findElementByXPath(Constants.btnBack).click();
            waitUntilAndroidElementVisibleByID(driver, Constants.btnBack2);
            driver.findElementById(Constants.btnBack2).click();
            lMenu.displayLeftMenu(driver, logger);
            lMenu.displayFavourites(driver, logger);
            Assert.assertEquals(isOfferingFavourited(driver, logger, offeringName), false);
            waitUntilAndroidElementVisibleByID(driver, Constants.btnHomeTB);
            driver.findElementById(Constants.btnHomeTB).click();
        }
    }

    public void favouriteOnlineOffering_Home(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        String offeringName = "Test";
        waitUntilAndroidElementVisibleByID(driver, Constants.btnOnlinePrograms);
        driver.findElementById(Constants.searchBox).sendKeys(offeringName);
        driver.findElementById(Constants.btnOnlinePrograms).click();
        waitUntilAndroidElementVisibleByxPath(driver, Constants.eleScrollAndFind);
        scrollToElement(driver, offeringName);
        java.util.List<MobileElement> educatorsList = (List<MobileElement>) driver.findElementsByXPath(Constants.eleScrollAndFind);
        System.out.println(educatorsList.size());
        for (MobileElement el : educatorsList) {
            if (el.findElementsById(Constants.txtTitle).size() > 0) {
                if (el.findElementById(Constants.txtTitle).getText().trim().equals(offeringName)) {
                    el.findElementById(Constants.btnFavourite2).click();
                    logger.log(Status.INFO, "Educator favourited");
                    lMenu.displayLeftMenu(driver, logger);
                    lMenu.displayFavourites(driver, logger);
                    Assert.assertEquals(isOfferingFavourited(driver, logger, offeringName), true);
                    waitUntilAndroidElementVisibleByID(driver, Constants.btnHomeTB);
                    driver.findElementById(Constants.btnHomeTB).click();
                    break;
                }
            }
        }
    }

    public void unfavouriteOnlineOffering_Home(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        String offeringName = "Test";
        waitUntilAndroidElementVisibleByID(driver, Constants.btnOnlinePrograms);
        driver.findElementById(Constants.searchBox).sendKeys(offeringName);
        driver.findElementById(Constants.btnOnlinePrograms).click();
        waitUntilAndroidElementVisibleByxPath(driver, Constants.eleScrollAndFind);
        scrollToElement(driver, offeringName);
        java.util.List<MobileElement> educatorsList = (List<MobileElement>) driver.findElementsByXPath(Constants.eleScrollAndFind);
        System.out.println(educatorsList.size());
        for (MobileElement el : educatorsList) {
            if (el.findElementsById(Constants.txtTitle).size() > 0) {
                if (el.findElementById(Constants.txtTitle).getText().trim().equals(offeringName)) {
                    el.findElementById(Constants.btnFavourite2).click();
                    logger.log(Status.INFO, "Educator unfavourited");
                    lMenu.displayLeftMenu(driver, logger);
                    lMenu.displayFavourites(driver, logger);
                    Assert.assertEquals(isOfferingFavourited(driver, logger, offeringName), false);
                    waitUntilAndroidElementVisibleByID(driver, Constants.btnHomeTB);
                    driver.findElementById(Constants.btnHomeTB).click();
                    break;
                }
            }
        }
    }

    public void favouriteEvent_Home(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        String offeringName = "Test Drip Feature";
        waitUntilAndroidElementVisibleByID(driver, Constants.btnOfflineEvent);
        driver.findElementById(Constants.searchBox).sendKeys(offeringName);
        driver.findElementById(Constants.btnOfflineEvent).click();
        waitUntilAndroidElementVisibleByxPath(driver, Constants.eleScrollAndFind);
        scrollToElement(driver, offeringName);
        java.util.List<MobileElement> educatorsList = (List<MobileElement>) driver.findElementsByXPath(Constants.eleScrollAndFind);
        System.out.println(educatorsList.size());
        for (MobileElement el : educatorsList) {
            if (el.findElementsById(Constants.txtTitle).size() > 0) {
                if (el.findElementById(Constants.txtTitle).getText().trim().equals(offeringName)) {
                    el.findElementById(Constants.btnFavourite2).click();
                    logger.log(Status.INFO, "Educator favourited");
                    lMenu.displayLeftMenu(driver, logger);
                    lMenu.displayFavourites(driver, logger);
                    Assert.assertEquals(isOfferingFavourited(driver, logger, offeringName), true);
                    waitUntilAndroidElementVisibleByID(driver, Constants.btnHomeTB);
                    driver.findElementById(Constants.btnHomeTB).click();
                    break;
                }
            }
        }
    }

    public void unfavouriteEvent_Home(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        String offeringName = "Test Drip Feature";
        waitUntilAndroidElementVisibleByID(driver, Constants.btnOfflineEvent);
        driver.findElementById(Constants.searchBox).sendKeys(offeringName);
        driver.findElementById(Constants.btnOfflineEvent).click();
        waitUntilAndroidElementVisibleByxPath(driver, Constants.eleScrollAndFind);
        scrollToElement(driver, offeringName);
        java.util.List<MobileElement> educatorsList = (List<MobileElement>) driver.findElementsByXPath(Constants.eleScrollAndFind);
        System.out.println(educatorsList.size());
        for (MobileElement el : educatorsList) {
            if (el.findElementsById(Constants.txtTitle).size() > 0) {
                if (el.findElementById(Constants.txtTitle).getText().trim().equals(offeringName)) {
                    el.findElementById(Constants.btnFavourite2).click();
                    logger.log(Status.INFO, "Educator unfavourited");
                    lMenu.displayLeftMenu(driver, logger);
                    lMenu.displayFavourites(driver, logger);
                    Assert.assertEquals(isOfferingFavourited(driver, logger, offeringName), false);
                    waitUntilAndroidElementVisibleByID(driver, Constants.btnHomeTB);
                    driver.findElementById(Constants.btnHomeTB).click();
                    break;
                }
            }
        }
    }

    public void favouriteContent(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        //String txtSearch = "Varun1";
        //String eduName = "varun Pandya";
        //String eduCName = "Varun1Pandya1";
        String offeringName = "Test";
        String eduName = "Kanak Bhuva";
        String eduCName = "Mvp1ventures";
        String txtSearch = "Mvp1";
        String contentName = "EditAction.png";
        MobileElement oe = (MobileElement) educator.getOffering(driver, logger, txtSearch, eduCName, offeringName);
        if (oe != null) {
            oe.findElementById(Constants.titleName).click();
            waitUntilAndroidElementVisibleByID(driver, Constants.btnAccessNow);
            driver.findElementById(Constants.btnAccessNow).click();
            waitUntilAndroidElementVisibleByID(driver, Constants.btnFavourite3);
            driver.findElementById(Constants.btnFavourite3).click();
            waitUntilAndroidElementVisibleByxPath(driver, Constants.btnBack);
            logger.log(Status.INFO, "Offering favourited");
            driver.findElementByXPath(Constants.btnBack).click();
            waitUntilAndroidElementVisibleByxPath(driver, Constants.btnBack);
            driver.findElementByXPath(Constants.btnBack).click();
            waitUntilAndroidElementVisibleByID(driver, Constants.btnBack2);
            driver.findElementById(Constants.btnBack2).click();
            lMenu.displayLeftMenu(driver, logger);
            lMenu.displayFavourites(driver, logger);
            Assert.assertEquals(isContentFavourited(driver, logger, contentName), true);
            waitUntilAndroidElementVisibleByID(driver, Constants.btnHomeTB);
            driver.findElementById(Constants.btnHomeTB).click();
        }
    }

    public void unfavouriteContent(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        //String txtSearch = "Varun1";
        //String eduName = "varun Pandya";
        //String eduCName = "Varun1Pandya1";
        String offeringName = "Test";
        String eduName = "Kanak Bhuva";
        String eduCName = "Mvp1ventures";
        String txtSearch = "Mvp1";
        String contentName = "EditAction.png";
        MobileElement oe = (MobileElement) educator.getOffering(driver, logger, txtSearch, eduCName, offeringName);
        if (oe != null) {
            oe.findElementById(Constants.titleName).click();
            waitUntilAndroidElementVisibleByID(driver, Constants.btnAccessNow);
            driver.findElementById(Constants.btnAccessNow).click();
            waitUntilAndroidElementVisibleByID(driver, Constants.btnFavourite3);
            driver.findElementById(Constants.btnFavourite3).click();
            waitUntilAndroidElementVisibleByxPath(driver, Constants.btnBack);
            logger.log(Status.INFO, "Offering unfavourited");
            driver.findElementByXPath(Constants.btnBack).click();
            waitUntilAndroidElementVisibleByxPath(driver, Constants.btnBack);
            driver.findElementByXPath(Constants.btnBack).click();
            waitUntilAndroidElementVisibleByID(driver, Constants.btnBack2);
            driver.findElementById(Constants.btnBack2).click();
            lMenu.displayLeftMenu(driver, logger);
            lMenu.displayFavourites(driver, logger);
            Assert.assertEquals(isContentFavourited(driver, logger, contentName), false);
            waitUntilAndroidElementVisibleByID(driver, Constants.btnHomeTB);
            driver.findElementById(Constants.btnHomeTB).click();
        }
    }

    public void favouriteLastViewedContent(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        String eduCName = "Mvp1ventures";
        String txtSearch = "Mvp1";
        String contentName = "ActionEdit.png";
        MobileElement oe = (MobileElement) educator.getContentFromLVC(driver, logger, txtSearch, eduCName, contentName);
        if (oe != null) {
            oe.findElementById(Constants.btnFavourite3).click();
            waitUntilAndroidElementVisibleByID(driver, Constants.btnBack2);
            logger.log(Status.INFO, "Content favourited");
            driver.findElementById(Constants.btnBack2).click();
            lMenu.displayLeftMenu(driver, logger);
            lMenu.displayFavourites(driver, logger);
            Assert.assertEquals(isContentFavourited(driver, logger, contentName), true);
            waitUntilAndroidElementVisibleByID(driver, Constants.btnHomeTB);
            driver.findElementById(Constants.btnHomeTB).click();
        }
    }

    public void unfavouriteLastViewedContent(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        String eduCName = "Mvp1ventures";
        String txtSearch = "Mvp1";
        String contentName = "ActionEdit.png";
        MobileElement oe = (MobileElement) educator.getContentFromLVC(driver, logger, txtSearch, eduCName, contentName);
        if (oe != null) {
            oe.findElementById(Constants.btnFavourite3).click();
            waitUntilAndroidElementVisibleByID(driver, Constants.btnBack2);
            logger.log(Status.INFO, "Offering unfavourited");
            driver.findElementById(Constants.btnBack2).click();
            lMenu.displayLeftMenu(driver, logger);
            lMenu.displayFavourites(driver, logger);
            Assert.assertEquals(isContentFavourited(driver, logger, contentName), false);
            waitUntilAndroidElementVisibleByID(driver, Constants.btnHomeTB);
            driver.findElementById(Constants.btnHomeTB).click();
        }
    }

    public void favouriteOffering_UpcomingSection(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        String eduCName = "Mvp1ventures";
        String txtSearch = "Mvp1";
        String offeringName = "Test";
        MobileElement oe = (MobileElement) educator.getOfferingFromUpcomingSection(driver, logger, txtSearch, eduCName, offeringName);
        if (oe != null) {
            System.out.println(oe.findElementById(Constants.upcomingOffRegister).getText());
            oe.findElementById(Constants.upcomingOffFav).click();
            waitUntilAndroidElementVisibleByID(driver, Constants.btnBack2);
            logger.log(Status.INFO,"Offering favoutired.");
            driver.findElementById(Constants.btnBack2).click();
            lMenu.displayLeftMenu(driver, logger);
            lMenu.displayFavourites(driver, logger);
            Assert.assertEquals(isOfferingFavourited(driver, logger, offeringName), true);
            waitUntilAndroidElementVisibleByID(driver, Constants.btnHomeTB);
            driver.findElementById(Constants.btnHomeTB).click();
        }
    }

    public void unfavouriteOffering_UpcomingSection(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        String eduCName = "Mvp1ventures";
        String txtSearch = "Mvp1";
        String offeringName = "Test";
        MobileElement oe = (MobileElement) educator.getOfferingFromUpcomingSection(driver, logger, txtSearch, eduCName, offeringName);
        if (oe != null) {
            oe.findElementById(Constants.upcomingOffFav).click();
            waitUntilAndroidElementVisibleByID(driver, Constants.btnBack2);
            logger.log(Status.INFO,"Offering unfavourited.");
            driver.findElementById(Constants.btnBack2).click();
            lMenu.displayLeftMenu(driver, logger);
            lMenu.displayFavourites(driver, logger);
            Assert.assertEquals(isOfferingFavourited(driver, logger, offeringName), false);
            waitUntilAndroidElementVisibleByID(driver, Constants.btnHomeTB);
            driver.findElementById(Constants.btnHomeTB).click();
        }
    }

    public void UnFavouriteEducator(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        waitUntilAndroidElementVisibleByID(driver, Constants.btnMaster);
        String eduName = "Kanak Bhuva";
        String eduCName = "Mvp1ventures";
        driver.findElementById(Constants.btnMaster).click();
        waitUntilAndroidElementVisibleByxPath(driver, Constants.favList);
        List<MobileElement> favList = (List<MobileElement>) driver.findElementsByXPath(Constants.favList);
        System.out.println(favList.size());
        for (MobileElement fl : favList) {
            if (fl.findElementById(Constants.titleName).getText().trim().equals(eduName) && fl.findElementById(Constants.favEduDesc).getText().trim().equals(eduCName)) {
                fl.findElementById(Constants.btnFavourite).click();
                Assert.assertEquals(isEducatorFavourited(driver, logger, eduName, eduCName), false);
                waitUntilAndroidElementVisibleByID(driver, Constants.btnHomeTB);
                driver.findElementById(Constants.btnHomeTB).click();
            }
        }
    }

    public void UnFavouriteOffering(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        waitUntilAndroidElementVisibleByID(driver, Constants.btnOffering);
        String favOffName = "Test";
        driver.findElementById(Constants.btnOffering).click();
        waitUntilAndroidElementVisibleByxPath(driver, Constants.favList);
        List<MobileElement> favList = (List<MobileElement>) driver.findElementsByXPath(Constants.favList);
        System.out.println(favList.size());
        for (MobileElement fl : favList) {
            System.out.println(fl.findElementById(Constants.title).getText());
            if (fl.findElementById(Constants.title).getText().trim().equals(favOffName)) {
                fl.findElementById(Constants.btnFavourite).click();
                Assert.assertEquals(isOfferingFavourited(driver, logger, favOffName), false);
                waitUntilAndroidElementVisibleByID(driver, Constants.btnHomeTB);
                driver.findElementById(Constants.btnHomeTB).click();
            }
        }
    }

    public void UnFavouriteContent(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        waitUntilAndroidElementVisibleByID(driver, Constants.btnContent);
        String favContentName = "EditAction.png";
        driver.findElementById(Constants.btnContent).click();
        waitInSeconds(3);
        waitUntilAndroidElementVisibleByxPath(driver, Constants.contentFavList);
        List<MobileElement> favList = (List<MobileElement>) driver.findElementsByXPath(Constants.contentFavList);
        System.out.println(favList.size());
        for (MobileElement fl : favList) {
            if (fl.findElementById(Constants.titleName).getText().trim().equals(favContentName)) {
                fl.findElementById(Constants.btnFavourite).click();
                Assert.assertEquals(isContentFavourited(driver, logger, favContentName), false);
                waitUntilAndroidElementVisibleByID(driver, Constants.btnHomeTB);
                driver.findElementById(Constants.btnHomeTB).click();
            }
        }
    }

    public boolean isEducatorFavourited(AndroidDriver driver, ExtentTest logger, String favEduName, String favEduComName) throws InterruptedException {
        waitUntilAndroidElementVisibleByID(driver, Constants.btnMaster);
        driver.findElementById(Constants.btnMaster).click();
        logger.log(Status.INFO, "Verification started for weather educator favourited or not");
        waitInSeconds(3);
        if (driver.findElementsByXPath(Constants.favList).size() > 0) {
            waitUntilAndroidElementVisibleByxPath(driver, Constants.favList);
            List<MobileElement> favList = (List<MobileElement>) driver.findElementsByXPath(Constants.favList);
            System.out.println(favList.size());
            for (MobileElement fl : favList) {
                if (fl.findElementById(Constants.titleName).getText().trim().equals(favEduName) && fl.findElementById(Constants.favEduDesc).getText().trim().equals(favEduComName)) {
                    logger.log(Status.INFO, "Verified: Educator exist in favourite page");
                    return true;
                }
            }
        }
        logger.log(Status.INFO, "Verified: Educator not exist in favourite page");
        return false;
    }

    public boolean isOfferingFavourited(AndroidDriver driver, ExtentTest logger, String favOffName) throws InterruptedException {
        waitUntilAndroidElementVisibleByID(driver, Constants.btnOffering);
        driver.findElementById(Constants.btnOffering).click();
        logger.log(Status.INFO, "Verification started for weather offering favourited or not");
        waitInSeconds(3);
        if (driver.findElementsByXPath(Constants.favList).size() > 0) {
            waitUntilAndroidElementVisibleByxPath(driver, Constants.favList);
            List<MobileElement> favList = (List<MobileElement>) driver.findElementsByXPath(Constants.favList);
            System.out.println(favList.size());
            for (MobileElement fl : favList) {
                if (fl.findElementById(Constants.title).getText().trim().equals(favOffName)) {
                    logger.log(Status.INFO, "Verified: Offering exist in favourite page");
                    return true;
                }
            }
        }
        logger.log(Status.INFO, "Verified: Offering not exist in favourite page");
        return false;
    }

    public boolean isContentFavourited(AndroidDriver driver, ExtentTest logger, String favContentName) throws InterruptedException {
        waitUntilAndroidElementVisibleByID(driver, Constants.btnContent);
        driver.findElementById(Constants.btnContent).click();
        waitInSeconds(3);
        logger.log(Status.INFO, "Verification started for weather content favourited or not");
        if (driver.findElementsByXPath(Constants.contentFavList).size() > 0) {
            waitUntilAndroidElementVisibleByxPath(driver, Constants.contentFavList);
            List<MobileElement> favList = (List<MobileElement>) driver.findElementsByXPath(Constants.contentFavList);
            System.out.println(favList.size());
            for (MobileElement fl : favList) {
                if (fl.findElementById(Constants.titleName).getText().trim().equals(favContentName)) {
                    logger.log(Status.INFO, "Verified: Content exist in favourite page");
                    return true;
                }
            }
        }
        logger.log(Status.INFO, "Verified: Content not exist in favourite page");
        return false;
    }

}