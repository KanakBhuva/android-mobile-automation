package pages;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import org.testng.Assert;
import others.Constants;
import testBase.TestBase;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.UUID;

public class CommunityPage extends TestBase {
    EducatorPage educator = new EducatorPage();
    String screenShotAt = System.getProperty("user.dir") + "\\ScreenShots\\";

    public MobileElement readUserFromCommunity(AndroidDriver driver, ExtentTest logger, String eduCName, String txtSearch, String userName) throws InterruptedException {
        educator.openEducatorPage(driver, logger, txtSearch, eduCName);
        waitUntilAndroidElementVisibleByID(driver, Constants.btnCommunityEdu);
        driver.findElementById(Constants.btnCommunityEdu).click();
        waitUntilAndroidElementVisibleByID(driver, Constants.communityList);
        logger.log(Status.INFO, "Community opened");
        MobileElement cul = (MobileElement) getUsersFromCommunity(driver, logger, userName);
        if (cul != null) {
            return cul;
        }
        return null;
    }

    public MobileElement getUsersFromCommunity(AndroidDriver driver, ExtentTest logger, String userName) throws InterruptedException {
        java.util.List<MobileElement> userList = (List<MobileElement>) driver.findElementsById(Constants.communityList);
        System.out.println(userList.size());
        for (MobileElement ul : userList) {
            if (ul.findElementsById(Constants.txtTitle).size() > 0) {
                System.out.println(ul.findElementById(Constants.txtTitle).getText());
                if (ul.findElementById(Constants.txtTitle).getText().trim().equals(userName)) {
                    logger.log(Status.INFO, "User found in community");
                    return (MobileElement) ul;
                }
            }
        }
        return null;
    }

    public void sendConnectionRequest(AndroidDriver driver, ExtentTest logger) throws InterruptedException, UnsupportedEncodingException {
        String eduCName = "Varun1Pandya1";
        String txtSearch = "Varun1";
        String userName = "Ajay Bhuva";
        MobileElement ele = readUserFromCommunity(driver, logger, eduCName, txtSearch, userName);
        if (ele != null) {
            waitUntilAndroidElementVisibleByID(driver, Constants.userOption);
            ele.findElementById(Constants.userOption).click();
            logger.log(Status.INFO, "Option button clicked");
            waitUntilAndroidElementVisibleByxPath(driver, Constants.addToNetwork);
            driver.findElementByXPath(Constants.addToNetwork).click();
            logger.log(Status.INFO, "Connect Button clicked");
            validatePopScreen(driver, Constants.addToNetworkMsg);
        }
    }

    public void sendConnectionRequestAgain(AndroidDriver driver, ExtentTest logger) throws InterruptedException, UnsupportedEncodingException {
        String userName = "Ajay Bhuva";
        MobileElement ele = getUsersFromCommunity(driver, logger, userName);
        if (ele != null) {
            waitUntilAndroidElementVisibleByID(driver, Constants.userOption);
            ele.findElementById(Constants.userOption).click();
            logger.log(Status.INFO, "Option button clicked");
            waitUntilAndroidElementVisibleByxPath(driver, Constants.addToNetwork);
            driver.findElementByXPath(Constants.addToNetwork).click();
            logger.log(Status.INFO, "Connect Button clicked");
            validatePopScreen(driver, Constants.addToNetworkMsg2);
        }
    }

    public void sendConnectionRequest_ReceivedRequest(AndroidDriver driver, ExtentTest logger) throws InterruptedException, UnsupportedEncodingException {
        String eduCName = "Varun1Pandya1";
        String txtSearch = "Varun1";
        String userName = "Bhavin Bhuva";
        MobileElement ele = readUserFromCommunity(driver, logger, eduCName, txtSearch, userName);
        if (ele != null) {
            waitUntilAndroidElementVisibleByID(driver, Constants.userOption);
            ele.findElementById(Constants.userOption).click();
            logger.log(Status.INFO, "Option button clicked");
            waitUntilAndroidElementVisibleByxPath(driver, Constants.addToNetwork);
            driver.findElementByXPath(Constants.addToNetwork).click();
            logger.log(Status.INFO, "Connect Button clicked");
            validatePopScreen(driver, Constants.addToNetworkMsg3);
        }
    }

    public void sendConnectionRequest_MyNetwork(AndroidDriver driver, ExtentTest logger) throws InterruptedException, UnsupportedEncodingException {
        String eduCName = "Varun1Pandya1";
        String txtSearch = "Varun1";
        String userName = "Bhavin Bhuva";
        MobileElement ele = readUserFromCommunity(driver, logger, eduCName, txtSearch, userName);
        if (ele != null) {
            waitUntilAndroidElementVisibleByID(driver, Constants.userOption);
            ele.findElementById(Constants.userOption).click();
            logger.log(Status.INFO, "Option button clicked");
            waitUntilAndroidElementVisibleByxPath(driver, Constants.addToNetwork);
            driver.findElementByXPath(Constants.addToNetwork).click();
            logger.log(Status.INFO, "Connect Button clicked");
            validatePopScreen(driver, Constants.addToNetworkMsg5);
        }
    }

    public void sendConnectionRequest_BlockedUser(AndroidDriver driver, ExtentTest logger) throws InterruptedException, UnsupportedEncodingException {
        String eduCName = "Varun1Pandya1";
        String txtSearch = "Varun1";
        String userName = "Ajay Bhuva";
        MobileElement ele = readUserFromCommunity(driver, logger, eduCName, txtSearch, userName);
        if (ele != null) {
            waitUntilAndroidElementVisibleByID(driver, Constants.userOption);
            ele.findElementById(Constants.userOption).click();
            logger.log(Status.INFO, "Option button clicked");
            waitUntilAndroidElementVisibleByxPath(driver, Constants.addToNetwork);
            driver.findElementByXPath(Constants.addToNetwork).click();
            logger.log(Status.INFO, "Connect Button clicked");
            validatePopScreen(driver, Constants.addToNetworkMsg4);
        }
    }

    public void sendMessage(AndroidDriver driver, ExtentTest logger) throws InterruptedException, UnsupportedEncodingException {
        String eduCName = "Varun1Pandya1";
        String txtSearch = "Varun1";
        String userName = "Bhavin Bhuva";
        String message = "Hi! How are you?";
        String sentMessage = "";
        MobileElement ele = readUserFromCommunity(driver, logger, eduCName, txtSearch, userName);
        if (ele != null) {
            waitUntilAndroidElementVisibleByID(driver, Constants.userOption);
            ele.findElementById(Constants.userOption).click();
            waitUntilAndroidElementVisibleByxPath(driver, Constants.sendMsg);
            driver.findElementByXPath(Constants.sendMsg).click();
            logger.log(Status.INFO, "Message screen opened");
            if (driver.findElementsById(Constants.txtNoData).size() != 1) {
                waitUntilAndroidElementVisibleByID(driver, Constants.txtMessage);
                driver.findElementById(Constants.txtMessage).sendKeys(message);
                driver.findElementById(Constants.btnSendMessage).click();
                logger.log(Status.INFO, "Sending ...");
                waitUntilAndroidElementVisibleByID(driver, Constants.btnBack2);
                MobileElement ml = (MobileElement) driver.findElementById(Constants.chatScreen);
                List<MobileElement> msgList = ml.findElementsByXPath(Constants.chatScreenLastMessage);
                sentMessage = msgList.get(msgList.size() - 1).findElementById(Constants.txtMessageInScreen).getText();
                //System.out.println(sentMessage);
                Assert.assertEquals(sentMessage, message);
                logger.log(Status.INFO, "Verified: Message sent");
                driver.findElementById(Constants.btnBack2).click();
                waitUntilAndroidElementVisibleByID(driver, Constants.userOption);
            }
        }
    }

    public void blockUser(AndroidDriver driver, ExtentTest logger) throws InterruptedException, UnsupportedEncodingException {
        String eduCName = "Varun1Pandya1";
        String txtSearch = "Varun1";
        String userName = "Bhavin Bhuva";
        MobileElement ele = readUserFromCommunity(driver, logger, eduCName, txtSearch, userName);
        if (ele != null) {
            waitUntilAndroidElementVisibleByID(driver, Constants.userOption);
            ele.findElementById(Constants.userOption).click();
            logger.log(Status.INFO, "Option button clicked");
            waitUntilAndroidElementVisibleByxPath(driver, Constants.blockUser);
            driver.findElementByXPath(Constants.blockUser).click();
            logger.log(Status.INFO, "Block button clicked");
            validatePopScreen(driver, Constants.blockLearnerMsg);
        }
    }

    public void unblockUser(AndroidDriver driver, ExtentTest logger) throws InterruptedException, UnsupportedEncodingException {
        String eduCName = "Varun1Pandya1";
        String txtSearch = "Varun1";
        String userName = "Bhavin Bhuva";
        MobileElement ele = readUserFromCommunity(driver, logger, eduCName, txtSearch, userName);
        if (ele != null) {
            waitUntilAndroidElementVisibleByID(driver, Constants.userOption);
            ele.findElementById(Constants.userOption).click();
            logger.log(Status.INFO, "Option button clicked");
            waitUntilAndroidElementVisibleByxPath(driver, Constants.unblockUser);
            driver.findElementByXPath(Constants.unblockUser).click();
            logger.log(Status.INFO, "Unblock button clicked");
            validatePopScreen(driver, Constants.unblockLearnerMsg);
        }
    }

    public void sendConnectionRequest_profile(AndroidDriver driver, ExtentTest logger) throws InterruptedException, UnsupportedEncodingException {
        String eduCName = "Varun1Pandya1";
        String txtSearch = "Varun1";
        String userName = "Ajay Bhuva";
        MobileElement ele = readUserFromCommunity(driver, logger, eduCName, txtSearch, userName);
        if (ele != null) {
            ele.click();
            waitUntilAndroidElementVisibleByID(driver, Constants.addRemoveConn_profile);
            logger.log(Status.INFO, "User profile opened");
            if (driver.findElementById(Constants.addRemoveConn_profile).getText().trim().equals("Connect")) {
                driver.findElementById(Constants.addRemoveConn_profile).click();
                logger.log(Status.INFO, "Connect button clicked");
                validatePopScreen(driver, Constants.addToNetworkMsg);
                waitUntilAndroidElementVisibleByxPath(driver, Constants.btnBack);
                driver.findElementByXPath(Constants.btnBack).click();
            }
        }
    }

    public void sendConnectionRequestAgain_profile(AndroidDriver driver, ExtentTest logger) throws InterruptedException, UnsupportedEncodingException {
        String eduCName = "Varun1Pandya1";
        String txtSearch = "Varun1";
        String userName = "Ajay Bhuva";
        MobileElement ele = getUsersFromCommunity(driver, logger, userName);
        if (ele != null) {
            ele.click();
            waitUntilAndroidElementVisibleByID(driver, Constants.addRemoveConn_profile);
            logger.log(Status.INFO, "User profile opened");
            if (driver.findElementById(Constants.addRemoveConn_profile).getText().trim().equals("Connect")) {
                driver.findElementById(Constants.addRemoveConn_profile).click();
                logger.log(Status.INFO, "Connect button clicked");
                validatePopScreen(driver, Constants.addToNetworkMsg2);
                waitUntilAndroidElementVisibleByxPath(driver, Constants.btnBack);
                driver.findElementByXPath(Constants.btnBack).click();
            }
        }
    }

    public void sendConnectionRequestAgain_profile_ReceivedRequest(AndroidDriver driver, ExtentTest logger) throws InterruptedException, UnsupportedEncodingException {
        String eduCName = "Varun1Pandya1";
        String txtSearch = "Varun1";
        String userName = "Bhavin Bhuva";
        MobileElement ele = readUserFromCommunity(driver, logger, eduCName, txtSearch, userName);
        if (ele != null) {
            ele.click();
            waitUntilAndroidElementVisibleByID(driver, Constants.addRemoveConn_profile);
            logger.log(Status.INFO, "User profile opened");
            if (driver.findElementById(Constants.addRemoveConn_profile).getText().trim().equals("Connect")) {
                driver.findElementById(Constants.addRemoveConn_profile).click();
                logger.log(Status.INFO, "Connect button clicked");
                validatePopScreen(driver, Constants.addToNetworkMsg3);
                waitUntilAndroidElementVisibleByxPath(driver, Constants.btnBack);
                driver.findElementByXPath(Constants.btnBack).click();
            }
        }
    }

    public void sendConnectionRequestAgain_profile_BlockedUser(AndroidDriver driver, ExtentTest logger) throws InterruptedException, UnsupportedEncodingException {
        String eduCName = "Varun1Pandya1";
        String txtSearch = "Varun1";
        String userName = "Ajay Bhuva";
        MobileElement ele = readUserFromCommunity(driver, logger, eduCName, txtSearch, userName);
        if (ele != null) {
            ele.click();
            waitUntilAndroidElementVisibleByID(driver, Constants.addRemoveConn_profile);
            logger.log(Status.INFO, "User profile opened");
            if (driver.findElementById(Constants.addRemoveConn_profile).getText().trim().equals("Connect")) {
                driver.findElementById(Constants.addRemoveConn_profile).click();
                logger.log(Status.INFO, "Connect button clicked");
                validatePopScreen(driver, Constants.addToNetworkMsg4);
                waitUntilAndroidElementVisibleByxPath(driver, Constants.btnBack);
                driver.findElementByXPath(Constants.btnBack).click();
            }
        }
    }

    public void RemoveFromCommunity_profile(AndroidDriver driver, ExtentTest logger) throws InterruptedException, UnsupportedEncodingException {
        String eduCName = "Varun1Pandya1";
        String txtSearch = "Varun1";
        String userName = "Bhavin Bhuva";
        MobileElement ele = readUserFromCommunity(driver, logger, eduCName, txtSearch, userName);
        if (ele != null) {
            ele.click();
            waitUntilAndroidElementVisibleByID(driver, Constants.addRemoveConn_profile);
            logger.log(Status.INFO, "User profile opened");
            if (driver.findElementById(Constants.addRemoveConn_profile).getText().trim().equals("Remove connection")) {
                driver.findElementById(Constants.addRemoveConn_profile).click();
                logger.log(Status.INFO, "Remove connection button clicked");
                validatePopScreen(driver, Constants.removeFromNetworkMsg);
            }
            waitUntilAndroidElementVisibleByxPath(driver, Constants.btnBack);
            driver.findElementByXPath(Constants.btnBack).click();
        }
    }

    public void sendMessage_profile(AndroidDriver driver, ExtentTest logger) throws InterruptedException, UnsupportedEncodingException {
        String eduCName = "Varun1Pandya1";
        String txtSearch = "Varun1";
        String userName = "Bhavin Bhuva";
        String message = "Hi! How are you?";
        String sentMessage = "";
        MobileElement ele = readUserFromCommunity(driver, logger, eduCName, txtSearch, userName);
        if (ele != null) {
            ele.click();
            waitUntilAndroidElementVisibleByID(driver, Constants.sendMsg_profile);
            logger.log(Status.INFO, "User profile opened");
            driver.findElementById(Constants.sendMsg_profile).click();
            logger.log(Status.INFO, "Message screen opened");
            if (driver.findElementsById(Constants.txtNoData).size() != 1) {
                waitUntilAndroidElementVisibleByID(driver, Constants.txtMessage);
                driver.findElementById(Constants.txtMessage).sendKeys(message);
                driver.findElementById(Constants.btnSendMessage).click();
                logger.log(Status.INFO, "Sending ...");
                waitUntilAndroidElementVisibleByID(driver, Constants.btnBack2);
                MobileElement ml = (MobileElement) driver.findElementById(Constants.chatScreen);
                List<MobileElement> msgList = ml.findElementsByXPath(Constants.chatScreenLastMessage);
                sentMessage = msgList.get(msgList.size() - 1).findElementById(Constants.txtMessageInScreen).getText();
                //System.out.println(sentMessage);
                Assert.assertEquals(sentMessage, message);
                logger.log(Status.INFO, "Verified: Message sent");
                driver.findElementById(Constants.btnBack2).click();
                waitUntilAndroidElementVisibleByxPath(driver, Constants.btnBack);
                driver.findElementByXPath(Constants.btnBack).click();
            }
        }
    }

    public void blockUser_profile(AndroidDriver driver, ExtentTest logger) throws InterruptedException, UnsupportedEncodingException {
        String eduCName = "Varun1Pandya1";
        String txtSearch = "Varun1";
        String userName = "Bhavin Bhuva";
        MobileElement ele = readUserFromCommunity(driver, logger, eduCName, txtSearch, userName);
        if (ele != null) {
            ele.click();
            waitUntilAndroidElementVisibleByID(driver, Constants.optionProfile);
            logger.log(Status.INFO, "User profile opened");
            driver.findElementById(Constants.optionProfile).click();
            logger.log(Status.INFO, "Option button clicked");
            waitUntilAndroidElementVisibleByxPath(driver, Constants.blockUser);
            driver.findElementByXPath(Constants.blockUser).click();
            logger.log(Status.INFO, "Block button clicked");
            validatePopScreen(driver, Constants.blockLearnerMsg);
            waitUntilAndroidElementVisibleByxPath(driver, Constants.btnBack);
            driver.findElementByXPath(Constants.btnBack).click();
        }
    }

    public void unblockUser_profile(AndroidDriver driver, ExtentTest logger) throws InterruptedException, UnsupportedEncodingException {
        String eduCName = "Varun1Pandya1";
        String txtSearch = "Varun1";
        String userName = "Bhavin Bhuva";
        MobileElement ele = readUserFromCommunity(driver, logger, eduCName, txtSearch, userName);
        if (ele != null) {
            ele.click();
            waitUntilAndroidElementVisibleByID(driver, Constants.optionProfile);
            logger.log(Status.INFO, "User profile opened");
            driver.findElementById(Constants.optionProfile).click();
            logger.log(Status.INFO, "Option button clicked");
            waitUntilAndroidElementVisibleByxPath(driver, Constants.unblockUser);
            driver.findElementByXPath(Constants.unblockUser).click();
            logger.log(Status.INFO, "Unblock button clicked");
            validatePopScreen(driver, Constants.unblockLearnerMsg);
            waitUntilAndroidElementVisibleByxPath(driver, Constants.btnBack);
            driver.findElementByXPath(Constants.btnBack).click();
        }
    }

    public boolean isUserExistInCommunity(AndroidDriver driver, ExtentTest logger, String eduCName, String txtSearch, String userName) throws InterruptedException, IOException {
        String uuid = UUID.randomUUID().toString();
        educator.openEducatorPage(driver, logger, txtSearch, eduCName);
        waitUntilAndroidElementVisibleByID(driver, Constants.btnCommunityEdu);
        driver.findElementById(Constants.btnCommunityEdu).click();
        waitUntilAndroidElementVisibleByID(driver, Constants.communityList);
        logger.log(Status.INFO, "Community opened");
        waitUntilAndroidElementVisibleByID(driver, Constants.searchBox);
        driver.findElementById(Constants.searchBox).sendKeys(userName);
        driver.findElementById(Constants.btnSearch).click();
        waitInSeconds(1);
        waitUntilAndroidElementVisibleByID(driver, Constants.searchBox);
        java.util.List<MobileElement> userList = (List<MobileElement>) driver.findElementsById(Constants.communityList);
        System.out.println(userList.size());
        for (MobileElement ul : userList) {
            if (ul.findElementsById(Constants.txtTitle).size() > 0) {
                System.out.println(ul.findElementById(Constants.txtTitle).getText());
                if (ul.findElementById(Constants.txtTitle).getText().trim().equals(userName)) {
                    logger.log(Status.INFO, "User exist in community");
                    takeScreenshot_Android(driver, screenShotAt + userName + "_" + uuid);
                    logger.addScreenCaptureFromPath(screenShotAt + userName + "_" + uuid + ".jpg");
                    waitUntilAndroidElementVisibleByID(driver, Constants.btnSearch);
                    return true;
                }
            }
        }
        logger.log(Status.INFO, "User does not exist in community");
        takeScreenshot_Android(driver, screenShotAt + userName);
        logger.addScreenCaptureFromPath(screenShotAt + userName + ".jpg");
        waitUntilAndroidElementVisibleByID(driver, Constants.btnSearch);
        return false;
    }

    public void verify_UserExistInCommunity(AndroidDriver driver, ExtentTest logger) throws IOException, InterruptedException {
        String eduCName = "Mvp1ventures";
        String txtSearch = "Mvp1";
        String userName = "Ketan Bhuva";
        Assert.assertEquals(true, isUserExistInCommunity(driver, logger, eduCName, txtSearch, userName));
    }

    public void verify_UserNotExistInCommunity(AndroidDriver driver, ExtentTest logger) throws IOException, InterruptedException {
        String eduCName = "Mvp1ventures";
        String txtSearch = "Mvp1";
        String userName = "Ketan Bhuva";
        Assert.assertEquals(false, isUserExistInCommunity(driver, logger, eduCName, txtSearch, userName));
    }

}
