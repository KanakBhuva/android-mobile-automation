package pages;

import com.aventstack.extentreports.ExtentTest;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import others.Constants;
import others.ReferralsMethods;
import testBase.TestBase;

import java.util.List;

public class ReferralsPage extends TestBase {
    ReferralsMethods refMethod = new ReferralsMethods();
    EducatorPage educator = new EducatorPage();

    public void sendBusinessReferralFromEducatorDetailsPage_SMS(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        String refType = "SMS";
        String msg = "";
        String SelectNames = "Kanak. R";
        String eduCName = "Mvp1ventures";
        String txtSearch = "Mvp1";
        educator.openEducatorPage(driver, logger, txtSearch, eduCName);
        driver.findElementById(Constants.businessRef).click();
        refMethod.postReferralProcess(driver, logger, refType, msg, SelectNames);
    }

    public void sendBusinessReferralFromEducatorDetailsPage_Mail(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        String refType = "Mail";
        String msg = "Test";
        String SelectNames = "Kanak. R";
        String eduCName = "Mvp1ventures";
        String txtSearch = "Mvp1";
        educator.openEducatorPage(driver, logger, txtSearch, eduCName);
        driver.findElementById(Constants.businessRef).click();
        refMethod.postReferralProcess(driver, logger, refType, msg, SelectNames);
    }

    public void sendBusinessReferralFromExploreDetailsPage_SMS(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        String refType = "SMS";
        String msg = "Test";
        String SelectNames = "Kanak. R";
        waitUntilAndroidElementVisibleByID(driver, Constants.businessRefExp);
        driver.findElementById(Constants.businessRefExp).click();
        waitInSeconds(3);
        scrollToElement(driver, "Breathe Me");
        java.util.List<MobileElement> educatorsList = (List<MobileElement>) driver.findElementsById(Constants.expEdus);
        System.out.println(educatorsList.size());
        for (MobileElement sl : educatorsList) {
            if (sl.findElementsById("com.mvp1ventures.masterdojo:id/tvCoachCompany").size() > 0) {
                if (sl.findElementById("com.mvp1ventures.masterdojo:id/tvCoachCompany").getText().trim().equals("Breathe Me")) {
                    sl.findElementById("com.mvp1ventures.masterdojo:id/imgShare").click();
                    break;
                }
            }
        }
        refMethod.postReferralProcess(driver, logger, refType, msg, SelectNames);
    }

    public void sendBusinessReferralFromExploreDetailsPage_Mail(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        String refType = "Mail";
        String msg = "";
        String SelectNames = "Kanak. R";
        waitUntilAndroidElementVisibleByID(driver, Constants.businessRefExp);
        driver.findElementById(Constants.businessRefExp).click();
        waitInSeconds(3);
        scrollToElement(driver, "Breathe Me");
        java.util.List<MobileElement> educatorsList = (List<MobileElement>) driver.findElementsById(Constants.expEdus);
        System.out.println(educatorsList.size());
        for (MobileElement sl : educatorsList) {
            if (sl.findElementsById("com.mvp1ventures.masterdojo:id/tvCoachCompany").size() > 0) {
                if (sl.findElementById("com.mvp1ventures.masterdojo:id/tvCoachCompany").getText().trim().equals("Breathe Me")) {
                    sl.findElementById("com.mvp1ventures.masterdojo:id/imgShare").click();
                    break;
                }
            }
        }
        refMethod.postReferralProcess(driver, logger, refType, msg, SelectNames);
    }

    public void sendOfferingReferralFromEducatorDetailsPage_SMS(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        String refType = "SMS";
        String msg = "";
        String SelectNames = "Kanak. R";
        String eduCName = "Mvp1ventures";
        String txtSearch = "Mvp1";
        String offeringName = "Test";
        MobileElement oe = (MobileElement) educator.getOffering(driver, logger, txtSearch, eduCName, offeringName);
        if (oe != null) {
            //oe.findElementById(Constants.titleName).click();
            waitUntilAndroidElementVisibleByID(driver, Constants.popReferral);
            driver.findElementById(Constants.popReferral).click();
            refMethod.postReferralProcess(driver, logger, refType, msg, SelectNames);
        }
    }

    public void sendOfferingReferralFromEducatorDetailsPage_Mail(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        String refType = "Mail";
        String msg = "Test";
        String SelectNames = "Kanak. R";
        String eduCName = "Mvp1ventures";
        String txtSearch = "Mvp1";
        String offeringName = "Test";
        MobileElement oe = (MobileElement) educator.getOffering(driver, logger, txtSearch, eduCName, offeringName);
        if (oe != null) {
            //oe.findElementById(Constants.titleName).click();
            waitUntilAndroidElementVisibleByID(driver, Constants.popReferral);
            driver.findElementById(Constants.popReferral).click();
            refMethod.postReferralProcess(driver, logger, refType, msg, SelectNames);
        }
    }

    public void sendOfferingReferralFromOfferingDetailsPage_SMS(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        String refType = "SMS";
        String msg = "Test";
        String SelectNames = "Kanak. R";
        String eduCName = "Mvp1ventures";
        String txtSearch = "Mvp1";
        String offeringName = "Test";
        MobileElement oe = (MobileElement) educator.getOffering(driver, logger, txtSearch, eduCName, offeringName);
        if (oe != null) {
            oe.findElementById(Constants.titleName).click();
            //waitUntilAndroidElementVisibleByID(driver, Constants.btnAccessNow);
            //driver.findElementById(Constants.btnAccessNow).click();
            waitUntilAndroidElementVisibleByID(driver, Constants.popReferral);
            driver.findElementById(Constants.popReferral).click();
            refMethod.postReferralProcess(driver, logger, refType, msg, SelectNames);
        }
    }

    public void sendOfferingReferralFromOfferingDetailsPage_Mail(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        String refType = "Mail";
        String msg = "";
        String SelectNames = "Kanak. R";
        String eduCName = "Mvp1ventures";
        String txtSearch = "Mvp1";
        String offeringName = "Test";
        MobileElement oe = (MobileElement) educator.getOffering(driver, logger, txtSearch, eduCName, offeringName);
        if (oe != null) {
            oe.findElementById(Constants.titleName).click();
            //waitUntilAndroidElementVisibleByID(driver, Constants.btnAccessNow);
            //driver.findElementById(Constants.btnAccessNow).click();
            waitUntilAndroidElementVisibleByID(driver, Constants.popReferral);
            driver.findElementById(Constants.popReferral).click();
            refMethod.postReferralProcess(driver, logger, refType, msg, SelectNames);
        }
    }

    public void sendOfferingReferralFromOfferingDetailsMorePage_SMS(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        String refType = "SMS";
        String msg = "";
        String SelectNames = "Kanak. R";
        String eduCName = "Mvp1ventures";
        String txtSearch = "Mvp1";
        String offeringName = "Test";
        MobileElement oe = (MobileElement) educator.getOffering(driver, logger, txtSearch, eduCName, offeringName);
        if (oe != null) {
            oe.findElementById(Constants.titleName).click();
            waitUntilAndroidElementVisibleByID(driver, Constants.btnAccessNow);
            driver.findElementById(Constants.btnAccessNow).click();
            waitUntilAndroidElementVisibleByID(driver, Constants.btnMore);
            driver.findElementById(Constants.btnMore).click();
            waitUntilAndroidElementVisibleByID(driver, Constants.popReferral);
            driver.findElementById(Constants.popReferral).click();
            refMethod.postReferralProcess(driver, logger, refType, msg, SelectNames);
        }
    }

    public void sendOfferingReferralFromOfferingDetailsMorePage_Mail(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        String refType = "Mail";
        String msg = "";
        String SelectNames = "Kanak. R";
        String eduCName = "Mvp1ventures";
        String txtSearch = "Mvp1";
        String offeringName = "Test";
        MobileElement oe = (MobileElement) educator.getOffering(driver, logger, txtSearch, eduCName, offeringName);
        if (oe != null) {
            oe.findElementById(Constants.titleName).click();
            waitUntilAndroidElementVisibleByID(driver, Constants.btnAccessNow);
            driver.findElementById(Constants.btnAccessNow).click();
            waitUntilAndroidElementVisibleByID(driver, Constants.btnMore);
            driver.findElementById(Constants.btnMore).click();
            waitUntilAndroidElementVisibleByID(driver, Constants.popReferral);
            driver.findElementById(Constants.popReferral).click();
            refMethod.postReferralProcess(driver, logger, refType, msg, SelectNames);
        }
    }

    public void sendOfferingReferralFromUpcomingSection_SMS(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        String refType = "SMS";
        String msg = "";
        String SelectNames = "Kanak. R";
        String eduCName = "Mvp1ventures";
        String txtSearch = "Mvp1";
        String offeringName = "Test";
        MobileElement oe = (MobileElement) educator.getOfferingFromUpcomingSection(driver, logger, txtSearch, eduCName, offeringName);
        if (oe != null) {
            //oe.findElementById(Constants.titleName).click();
            waitUntilAndroidElementVisibleByID(driver, Constants.upcomingOffRef);
            driver.findElementById(Constants.upcomingOffRef).click();
            refMethod.postReferralProcess(driver, logger, refType, msg, SelectNames);
        }
    }

    public void sendOfferingReferralFromUpcomingSectionPage_Mail(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        String refType = "Mail";
        String msg = "Test";
        String SelectNames = "Kanak. R";
        String eduCName = "Mvp1ventures";
        String txtSearch = "Mvp1";
        String offeringName = "Test";
        MobileElement oe = (MobileElement) educator.getOfferingFromUpcomingSection(driver, logger, txtSearch, eduCName, offeringName);
        if (oe != null) {
            //oe.findElementById(Constants.titleName).click();
            waitUntilAndroidElementVisibleByID(driver, Constants.upcomingOffRef);
            driver.findElementById(Constants.upcomingOffRef).click();
            refMethod.postReferralProcess(driver, logger, refType, msg, SelectNames);
        }
    }

    public void reSendBusinessReferral(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        waitUntilAndroidElementVisibleByID(driver, Constants.btnBusiness);
        String refType = "Mail";
        String msg = "";
        String selectNames = "Kanak. R";
        String businessName = "Breathe Me";
        List<MobileElement> cnList = driver.findElementsByXPath(Constants.eduListRefHistory);
        for (MobileElement cn : cnList) {
            if (cn.findElementById(Constants.EduNameRH).getText().trim().equals(businessName)) {
                cn.findElementById(Constants.btnOpenEduRH).click();
                refMethod.postResendReferralProcess(driver, logger, refType, msg, selectNames);
                break;
            }
        }
    }

    public void reSendOfferingReferral(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        waitUntilAndroidElementVisibleByID(driver, Constants.btnBusiness);
        String refType = "SMS";
        String msg = "";
        String selectNames = "Kanak. R";
        String offeringName = "Test";
        driver.findElementById(Constants.btnOffering).click();
        waitUntilAndroidElementVisibleByxPath(driver, Constants.eduListRefHistory);
        List<MobileElement> cnList = driver.findElementsByXPath(Constants.eduListRefHistory);
        for (MobileElement cn : cnList) {
            if (cn.findElementById(Constants.txtOfferingName).getText().trim().equals(offeringName)) {
                cn.findElementById(Constants.txtOfferingName).click();
                refMethod.postResendReferralProcess(driver, logger, refType, msg, selectNames);
                break;
            }
        }
    }

}
