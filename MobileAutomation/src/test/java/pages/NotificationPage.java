package pages;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import org.testng.Assert;
import others.Constants;
import testBase.TestBase;

import java.time.Duration;
import java.util.List;

public class NotificationPage extends TestBase {

    public void deleteNotification(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        waitUntilAndroidElementVisibleByID(driver, Constants.btnNotification2);
        logger.log(Status.INFO,"Notification screen opened");
        //driver.findElementById(Constants.btnNotification2).click();
        waitUntilAndroidElementVisibleByID(driver, Constants.notificationSec);
        int X1 = driver.findElementById(Constants.notificationList).getLocation().getX();
        int X2 = ((driver.findElementById(Constants.notificationList).getSize().getWidth()) / 2);
        int Y = ((driver.findElementById(Constants.notificationList).getLocation().getY()) + (driver.findElementById(Constants.notificationList).getSize().getHeight() / 2));
        System.out.println("X1:" + X1 + "|X2: " + X2 + "|Y: " + Y);
        System.out.println(driver.findElementById(Constants.notificationList).getSize());
        new TouchAction<>(driver)
                .press(PointOption.point(X2, Y)).waitAction(WaitOptions.waitOptions(Duration.ofMillis(1000)))
                .moveTo(PointOption.point(X1, Y)).release().perform();
        waitUntilAndroidElementVisibleByID(driver, Constants.deleteNotification);
        driver.findElementById(Constants.deleteNotification).click();
        logger.log(Status.INFO,"Delete button clicked");
    }

    public boolean isSupportRequestSent(AndroidDriver driver, ExtentTest logger, String name, String date, String title, String desc) throws InterruptedException {
        waitUntilAndroidElementVisibleByID(driver, Constants.btnSupportReq);
        String expectedTitle = "";
        String expectedDesc = "";
        driver.findElementById(Constants.btnSupportReq).click();
        waitUntilAndroidElementVisibleByID(driver, Constants.supportReqSec);
        logger.log(Status.INFO,"Support request list displayed");
        MobileElement sr = (MobileElement) driver.findElementById(Constants.supportReqSec);
        List<MobileElement> eduNames = sr.findElementsByXPath(Constants.supportReqList);
        for (MobileElement en : eduNames) {
            if (en.findElementById(Constants.supportReqEduName).getText().equals(name) && en.findElementById(Constants.supportReqDate).getText().equals(date)) {
                en.findElementById(Constants.supportReqEduName).click();
                logger.log(Status.INFO,"Support request opened");
                waitUntilAndroidElementVisibleByID(driver, Constants.supportReqTitle);
                expectedTitle = driver.findElementById(Constants.supportReqTitle).getText();
                expectedDesc = driver.findElementById(Constants.supportReqDesc).getText();
                Assert.assertEquals(title, expectedTitle);
                Assert.assertEquals(desc, expectedDesc);
                logger.log(Status.INFO,"Verified: Support request found");
                driver.findElementByXPath(Constants.btnBack).click();
                waitUntilAndroidElementVisibleByID(driver, Constants.supportReqSec);
                return true;
            }
        }
        return false;
    }

}
