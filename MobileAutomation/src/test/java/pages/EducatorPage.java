package pages;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import org.testng.Assert;
import others.Constants;
import testBase.TestBase;

import java.time.Duration;
import java.util.List;

public class EducatorPage extends TestBase {

    public void openEducatorPage(AndroidDriver driver, ExtentTest logger, String searchTxt, String companyName) throws InterruptedException {
        waitUntilAndroidElementVisibleByxPath(driver, Constants.btnLeftMenu);
        scrollById(driver, Constants.searchBox);
        driver.findElementById(Constants.searchBox).sendKeys(searchTxt);
        driver.findElementById(Constants.btnSearch).click();
        waitInSeconds(3);
        searchAndClick(driver, Constants.eleScrollAndFind, Constants.txtTitle, companyName);
        waitUntilAndroidElementVisibleByID(driver, Constants.eduDetailsPage);
        waitInSeconds(1);
        logger.log(Status.INFO, "Educator screen displayed");
    }

    public MobileElement getOffering(AndroidDriver driver, ExtentTest logger, String txtSearch, String companyName, String offeringName) throws InterruptedException {
        openEducatorPage(driver, logger, txtSearch, companyName);
        Boolean isScrollNeeded = true;
        //scrollById(driver, Constants.popOfferings);
        MobileElement po = (MobileElement) driver.findElementById(Constants.popOfferings);
        List<MobileElement> offList = po.findElementsByXPath(Constants.popOfferingList);
        int YCoordinates = offList.get(0).getCenter().getY();
        int X1Coordinates = offList.get(offList.size() - 1).getLocation().getX();
        int X2Coordinates = offList.get(0).getLocation().getX() - 6;

        while (isScrollNeeded) {
            for (MobileElement on : offList) {
                if (on.findElementsById(Constants.titleName).size() > 0) {
                    //System.out.println(on.findElementById(Constants.titleName).getText());
                    if (on.findElementById(Constants.titleName).getText().trim().equals(offeringName)) {
                        logger.log(Status.INFO, on.findElementById(Constants.titleName).getText().trim() + " Offering found");
                        isScrollNeeded = false;
                        return (MobileElement) on;
                    }
                }
            }
            if (isScrollNeeded) {
                new TouchAction<>(driver)
                        .press(PointOption.point(X1Coordinates, YCoordinates)).waitAction(WaitOptions.waitOptions(Duration.ofMillis(3500)))
                        .moveTo(PointOption.point(X2Coordinates, YCoordinates)).release().perform();
                waitInSeconds(3);
                offList = po.findElementsByXPath(Constants.popOfferingList);
            }
        }
        return null;
    }

    public void openOffering(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        String txtSearch = "Varun1";
        String companyName = "Varun1Pandya1";
        String offeringName = "Test paid recurring offering";
        MobileElement oe = (MobileElement) getOffering(driver, logger, txtSearch, companyName, offeringName);
        if (oe != null) {
            oe.findElementById(Constants.titleName).click();
        }
    }

    public void openOfferingFromContinueWithSection(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        String txtSearch = "Varun1";
        String companyName = "Varun1Pandya1";
        openEducatorPage(driver, logger, txtSearch, companyName);
        Boolean isScrollNeeded = true;
        String offeringName = "Test_All_Types";
        scrollById(driver, Constants.lblContinueWith);
        MobileElement cw = (MobileElement) driver.findElementById(Constants.continueWithSec);
        List<MobileElement> offList = cw.findElementsByXPath(Constants.cwOffering);
        int YCoordinates = offList.get(0).getCenter().getY();
        int X1Coordinates = offList.get(offList.size() - 1).getLocation().getX();
        int X2Coordinates = offList.get(0).getLocation().getX();

        while (isScrollNeeded) {
            if (cw.findElementByXPath(Constants.cwOfferingName).getText().equals(offeringName)) {
                //System.out.println(cw.findElementByXPath(Constants.cwSecCompleted).getText());
                cw.findElementByXPath(Constants.cwOfferingName).click();
                logger.log(Status.INFO, cw.findElementById(Constants.cwOfferingName).getText().trim() + " Offering found");
                isScrollNeeded = false;
                break;
            }
            if (isScrollNeeded) {
                new TouchAction<>(driver)
                        .press(PointOption.point(X1Coordinates, YCoordinates)).waitAction(WaitOptions.waitOptions(Duration.ofMillis(3000)))
                        .moveTo(PointOption.point(X2Coordinates, YCoordinates)).release().perform();
                waitInSeconds(1);
            }
        }
        logger.log(Status.INFO, "Offering screen displayed");
    }

    public MobileElement getContentFromLVC(AndroidDriver driver, ExtentTest logger, String txtSearch, String companyName, String contentName) throws InterruptedException {
        openEducatorPage(driver, logger, txtSearch, companyName);
        Boolean isScrollNeeded = true;
        scrollById(driver, Constants.lblLastViewedContent);
        waitInSeconds(3);
        MobileElement lwc = (MobileElement) driver.findElementById(Constants.lastViewedContentSec);
        List<MobileElement> offList = lwc.findElementsByXPath(Constants.lastViewedContentList);
        int YCoordinates = offList.get(0).getCenter().getY();
        int X1Coordinates = offList.get(offList.size() - 1).getLocation().getX();
        int X2Coordinates = offList.get(0).getLocation().getX();
        //System.out.println("|X1: " + X1Coordinates + "|X2: " + X2Coordinates + "|Y: " + YCoordinates);
        while (isScrollNeeded) {
            //System.out.println(offList.size());
            for (MobileElement on : offList) {
                if (on.findElementsById(Constants.titleName).size() > 0) {
                    System.out.println(on.findElementById(Constants.titleName).getText());
                    if (on.findElementById(Constants.titleName).getText().equals(contentName)) {
                        logger.log(Status.INFO, on.findElementById(Constants.titleName).getText().trim() + " Offering found");
                        isScrollNeeded = false;
                        return (MobileElement) on;
                    }
                }
            }
            if (isScrollNeeded) {
                new TouchAction<>(driver)
                        .press(PointOption.point(X1Coordinates, YCoordinates)).waitAction(WaitOptions.waitOptions(Duration.ofMillis(3300)))
                        .moveTo(PointOption.point(X2Coordinates, YCoordinates)).release().perform();
                waitInSeconds(3);
                offList = lwc.findElementsByXPath(Constants.lastViewedContentList);
            }
            /*else {
                waitInSeconds(3);
                driver.findElementByXPath(Constants.btnLeftMenu).click();
            }*/
        }
        return null;
    }

    public void openContentFromLVC(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        String txtSearch = "Varun1";
        String companyName = "Varun1Pandya1";
        String offeringName = "Test paid recurring offering";
        MobileElement oe = (MobileElement) getContentFromLVC(driver, logger, txtSearch, companyName, offeringName);
        if (oe != null) {
            oe.findElementById(Constants.titleName).click();
        }
    }

    public MobileElement getOfferingFromUpcomingSection(AndroidDriver driver, ExtentTest logger, String txtSearch, String companyName, String offeringName) throws InterruptedException {
        openEducatorPage(driver, logger, txtSearch, companyName);
        Boolean isScrollNeeded = true;
        scrollById(driver, Constants.lblUpcoming);
        //scrollToElement(driver, offeringName);
        waitInSeconds(3);
        MobileElement uoSec = (MobileElement) driver.findElementById(Constants.upcomingSec);
        List<MobileElement> offList = uoSec.findElementsByXPath(Constants.upcomingOffList);
        int XCoordinates = offList.get(0).getCenter().getX();
        int Y1Coordinates = offList.get(offList.size() - 1).getLocation().getY();
        int Y2Coordinates = offList.get(0).getLocation().getY();
        //System.out.println("|Y1: " + Y1Coordinates + "|Y2: " + Y2Coordinates + "|X: " + XCoordinates);
        while (isScrollNeeded) {
            System.out.println(offList.size());
            for (MobileElement uo : offList) {
                if (uo.findElementsById(Constants.upcomingOffName).size() > 0 && uo.findElementsById(Constants.upcomingOffRegister).size() > 0) {
                    System.out.println(uo.findElementById(Constants.upcomingOffName).getText());
                    //if (uo.findElementById(Constants.upcomingOffName).getText().trim().equals(offeringName) && !(uo.findElementById(Constants.upcomingOffRegister).getText().equals("Access Now"))) { ///removed !
                    if (uo.findElementById(Constants.upcomingOffName).getText().trim().equals(offeringName)) {
                        logger.log(Status.INFO, uo.findElementById(Constants.upcomingOffName).getText().trim() + " Offering found");
                        isScrollNeeded = false;
                        return (MobileElement) uo;
                    }
                }
            }
            if (isScrollNeeded) {
                if (driver.findElementsById(Constants.lblTestimonials).size() > 0) {
                    logger.log(Status.INFO, "Offering Not found in upcoming section");
                    isScrollNeeded = false;
                    return null;
                }
                if (offList.size() == 1) { /////////////////////////////////////////////////////////////////////////////////////////////////////
                    Y1Coordinates = driver.findElementById(Constants.searchBox).getLocation().getY();
                    new TouchAction<>(driver)
                            .press(PointOption.point(XCoordinates, Y2Coordinates)).waitAction(WaitOptions.waitOptions(Duration.ofMillis(3000)))
                            .moveTo(PointOption.point(XCoordinates, Y1Coordinates)).release().perform();
                    waitInSeconds(3);
                    offList = uoSec.findElementsByXPath(Constants.upcomingOffList);
                    Y1Coordinates = offList.get(offList.size() - 1).getLocation().getY();
                    Y2Coordinates = offList.get(0).getLocation().getY();
                } else {
                    new TouchAction<>(driver)
                            .press(PointOption.point(XCoordinates, Y1Coordinates)).waitAction(WaitOptions.waitOptions(Duration.ofMillis(5000)))
                            .moveTo(PointOption.point(XCoordinates, Y2Coordinates)).release().perform();
                    waitInSeconds(3);
                    offList = uoSec.findElementsByXPath(Constants.upcomingOffList);
                }
            }
        }
        return null;
    }

    public void openOfferingFromUpcomingSection(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        String txtSearch = "Varun1";
        String companyName = "Varun1Pandya1";
        String offeringName = "Offring 8 - (Thumbnail)";
        MobileElement oe = (MobileElement) getOfferingFromUpcomingSection(driver, logger, txtSearch, companyName, offeringName);
        if (oe != null) {
            oe.findElementById(Constants.upcomingOffRegister).click();
            System.out.println("Got it");
        }
    }

    public void checkReadMoreButton(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        String txtSearch = "Mvp1";
        String companyName = "Mvp1ventures";
        String readMoreText = "Nothing";
        String readMoreDesc = "Nothing";
        openEducatorPage(driver, logger, txtSearch, companyName);
        logger.log(Status.INFO,"Educator details page opened");
        driver.findElementById(Constants.btnReadMore).click();
        logger.log(Status.INFO,"Read more button clicked");
        waitUntilAndroidElementVisibleByID(driver, Constants.txtReadMore);
        Assert.assertEquals(readMoreText, driver.findElementById(Constants.txtReadMore).getText());
        Assert.assertEquals(readMoreDesc, driver.findElementById(Constants.descReadMore).getText());
        driver.findElementById(Constants.closeReadMore).click();
        logger.log(Status.INFO,"Read more screen closed");
    }

}
