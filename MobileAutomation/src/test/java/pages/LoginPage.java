package pages;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import others.Constants;
import testBase.TestBase;

import java.io.UnsupportedEncodingException;

public class LoginPage extends TestBase {

    public void SignUp(AndroidDriver driver, ExtentTest logger) throws InterruptedException, UnsupportedEncodingException {

        String str = "";
        int cCount = 0;
        boolean isFound = false;
        driver.findElementById(Constants.btnSignUp).click();
        waitUntilAndroidElementVisibleByID(driver, Constants.btnNextScreen);
        logger.log(Status.INFO, "SignUp email displayed");
        driver.findElementById(Constants.txtEmail).sendKeys("tamligarde@enayu.com");
        driver.findElementById(Constants.chkTerms).click();
        driver.findElementById(Constants.btnNextScreen).click();
        Thread.sleep(3000);

        if (driver.findElementsById(Constants.messagePopUp).size() == 1) {
            validatePopScreen(driver, Constants.mailExitMsg);
            waitUntilAndroidElementVisibleByID(driver, Constants.btnBackScreen);
            driver.findElementById(Constants.btnBackScreen).click();
        } else {
            logger.log(Status.INFO, "SignUp password displayed");
            waitUntilAndroidElementVisibleByID(driver, Constants.password);
            driver.findElementById(Constants.password).sendKeys("1234567890");
            driver.findElementById(Constants.btnNextScreen).click();
            logger.log(Status.INFO, "SignUp name displayed");
            waitUntilAndroidElementVisibleByID(driver, Constants.firstName);
            driver.findElementById(Constants.firstName).sendKeys("Kanak");
            driver.findElementById(Constants.lastName).sendKeys("Bhuva");
            driver.findElementById(Constants.btnNextScreen).click();
            logger.log(Status.INFO, "SignUp country displayed");
            waitUntilAndroidElementVisibleByID(driver, Constants.countryCodeDD);
            driver.findElementById(Constants.countryCodeDD).click();
            waitUntilAndroidElementVisibleByID(driver, Constants.searchCountryCC);
            driver.findElementById(Constants.searchCountryCC).sendKeys("Australia");
            cCount = driver.findElementsByXPath(Constants.searchCountryList).size();
            for (int ci = 1; ci <= cCount; ci++) {
                str = "//androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[" + ci + "]/android.widget.TextView";
                if (driver.findElementByXPath(str).getText().trim().equals("Australia (AU)")) {
                    driver.findElementByXPath(str).click();
                    isFound = true;
                }
            }
            if (!isFound) {
                driver.pressKey(new KeyEvent().withKey(AndroidKey.ESCAPE));
            }
            waitUntilAndroidElementVisibleByID(driver, Constants.mobileNumber);
            driver.findElementById(Constants.mobileNumber).sendKeys("887766995544");
            driver.findElementById(Constants.btnSignUp2).click();
        }
    }

    public void ForgotPassword(AndroidDriver driver, ExtentTest logger) throws InterruptedException, UnsupportedEncodingException {
        waitUntilAndroidElementVisibleByID(driver, Constants.btnForgotPass);
        driver.findElementById(Constants.btnForgotPass).click();
        logger.log(Status.INFO, "Forgot password screen displayed");
        waitUntilAndroidElementVisibleByID(driver, Constants.txtEmail);
        driver.findElementById(Constants.txtEmail).sendKeys("bhuvakanak777@gmail.com");
        driver.findElementById(Constants.btnSentMal).click();
        logger.log(Status.INFO,"Sent mail button clicked");
        validatePopScreen(driver, Constants.forgotPassMsg);
    }

    public void login1(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        String id = "bhuvakanak777@gmail.com";
        String pass = "1234567890";
        login(driver, logger, id, pass);
        logger.log(Status.INFO, "User id: " + id);
    }

    public void login2(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        String id = "zdtm@wootap.me";
        String pass = "1234567890";
        login(driver, logger, id, pass);
        logger.log(Status.INFO, "User id: " + id);
    }

    public void login(AndroidDriver driver, ExtentTest logger, String id, String Pass) throws InterruptedException {
        waitUntilAndroidElementVisibleByID(driver, Constants.userId);
        logger.log(Status.INFO, "Login screen displayed");
        driver.findElementById(Constants.userId).sendKeys(id);
        driver.findElementById(Constants.password).sendKeys(Pass);
        driver.findElementById(Constants.btnLogin).click();
        waitUntilAndroidElementVisibleByID(driver, Constants.searchBox);
    }

}