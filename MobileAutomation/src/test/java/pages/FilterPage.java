package pages;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import others.Constants;
import testBase.TestBase;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.time.Duration;
import java.util.List;

public class FilterPage extends TestBase {

    EducatorPage educator = new EducatorPage();
    String screenShotAt = System.getProperty("user.dir") + "\\ScreenShots\\";

    public void openFilterScreen(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        String eduCName = "Varun1Pandya1";
        String txtSearch = "Varun1";
        educator.openEducatorPage(driver, logger, txtSearch, eduCName);
        logger.log(Status.INFO, "Educator details screen opened");
        waitUntilAndroidElementVisibleByID(driver, Constants.btnCommunityEdu);
        driver.findElementById(Constants.btnCommunityEdu).click();
        logger.log(Status.INFO, "Community Screen opened");
        clickFilterButton(driver, logger);
    }

    public void clickFilterButton(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        waitUntilAndroidElementVisibleByID(driver, Constants.btnFilter);
        driver.findElementById(Constants.btnFilter).click();
        logger.log(Status.INFO, "Filter button clicked");
    }

    public void filterByBusinessName(AndroidDriver driver, ExtentTest logger) throws InterruptedException, IOException {
        String businessName = "qwertyuij";
        openFilterScreen(driver, logger);
        waitUntilAndroidElementVisibleByID(driver, Constants.btnOnlineProgram);
        logger.log(Status.INFO, "Filter screen opened");
        driver.findElementById(Constants.txtBusinessName).sendKeys(businessName);
        scrollById(driver, Constants.btnApply);
        driver.findElementById(Constants.btnApply).click();
        logger.log(Status.INFO, "Filter applied for " + businessName);
        waitInSeconds(3);
        if (driver.findElementsById(Constants.messagePopUp).size() == 1) {
            validatePopScreen(driver, Constants.searchResult0Msg);
            logger.log(Status.INFO, "Search returned no matches");
            waitUntilAndroidElementVisibleByxPath(driver, Constants.btnBack);
            driver.findElementByXPath(Constants.btnBack).click();
        } else {
            logger.log(Status.INFO, "Result found. Adding screenshot ...");
            takeScreenshot_Android(driver, screenShotAt + businessName);
            logger.addScreenCaptureFromPath(screenShotAt + businessName + ".jpg");
            waitUntilAndroidElementVisibleByID(driver, Constants.btnSearch);
            driver.findElementById(Constants.btnSearch).click();
        }
        waitUntilAndroidElementVisibleByID(driver, Constants.btnFilter);
    }

    public void filterByIndustry(AndroidDriver driver, ExtentTest logger) throws InterruptedException, IOException {
        String industryName = "IT";
        clickFilterButton(driver, logger);
        waitUntilAndroidElementVisibleByID(driver, Constants.btnOnlineProgram);
        logger.log(Status.INFO, "Filter screen opened");
        driver.findElementById(Constants.txtIndustry).sendKeys(industryName);
        scrollById(driver, Constants.btnApply);
        driver.findElementById(Constants.btnApply).click();
        logger.log(Status.INFO, "Filter applied for " + industryName);
        waitInSeconds(3);
        if (driver.findElementsById(Constants.messagePopUp).size() == 1) {
            validatePopScreen(driver, Constants.searchResult0Msg);
            logger.log(Status.INFO, "Search returned no matches");
            waitUntilAndroidElementVisibleByxPath(driver, Constants.btnBack);
            driver.findElementByXPath(Constants.btnBack).click();
        } else {
            logger.log(Status.INFO, "Result found. Adding screenshot ...");
            takeScreenshot_Android(driver, screenShotAt + industryName);
            logger.addScreenCaptureFromPath(screenShotAt + industryName + ".jpg");
            waitUntilAndroidElementVisibleByID(driver, Constants.btnSearch);
            driver.findElementById(Constants.btnSearch).click();
        }
        waitUntilAndroidElementVisibleByID(driver, Constants.btnFilter);
    }

    public void filterByCity(AndroidDriver driver, ExtentTest logger) throws InterruptedException, IOException {
        String city = "Ahmedabad";
        clickFilterButton(driver, logger);
        waitUntilAndroidElementVisibleByID(driver, Constants.btnOnlineProgram);
        logger.log(Status.INFO, "Filter screen opened");
        driver.findElementById(Constants.txtCity).sendKeys(city);
        scrollById(driver, Constants.btnApply);
        driver.findElementById(Constants.btnApply).click();
        logger.log(Status.INFO, "Filter applied for " + city);
        waitInSeconds(3);
        if (driver.findElementsById(Constants.messagePopUp).size() == 1) {
            validatePopScreen(driver, Constants.searchResult0Msg);
            logger.log(Status.INFO, "Search returned no matches");
            waitUntilAndroidElementVisibleByxPath(driver, Constants.btnBack);
            driver.findElementByXPath(Constants.btnBack).click();
        } else {
            logger.log(Status.INFO, "Result found. Adding screenshot ...");
            takeScreenshot_Android(driver, screenShotAt + city);
            logger.addScreenCaptureFromPath(screenShotAt + city + ".jpg");
            waitUntilAndroidElementVisibleByID(driver, Constants.btnSearch);
            driver.findElementById(Constants.btnSearch).click();
        }
        waitUntilAndroidElementVisibleByID(driver, Constants.btnFilter);
    }

    public void filterByCountry(AndroidDriver driver, ExtentTest logger) throws InterruptedException, IOException {
        String country = "India";
        clickFilterButton(driver, logger);
        waitUntilAndroidElementVisibleByID(driver, Constants.btnOnlineProgram);
        logger.log(Status.INFO, "Filter screen opened");
        driver.findElementById(Constants.txtCountry).sendKeys(country);
        scrollById(driver, Constants.btnApply);
        driver.findElementById(Constants.btnApply).click();
        logger.log(Status.INFO, "Filter applied for " + country);
        waitInSeconds(3);
        if (driver.findElementsById(Constants.messagePopUp).size() == 1) {
            validatePopScreen(driver, Constants.searchResult0Msg);
            logger.log(Status.INFO, "Search returned no matches");
            waitUntilAndroidElementVisibleByxPath(driver, Constants.btnBack);
            driver.findElementByXPath(Constants.btnBack).click();
        } else {
            logger.log(Status.INFO, "Result found. Adding screenshot ...");
            takeScreenshot_Android(driver, screenShotAt + country);
            logger.addScreenCaptureFromPath(screenShotAt + country + ".jpg");
            waitUntilAndroidElementVisibleByID(driver, Constants.btnSearch);
            driver.findElementById(Constants.btnSearch).click();
        }
        waitUntilAndroidElementVisibleByID(driver, Constants.btnFilter);
    }

    public void filterBySkill(AndroidDriver driver, ExtentTest logger) throws InterruptedException, IOException {
        String skill = "Automation";
        clickFilterButton(driver, logger);
        waitUntilAndroidElementVisibleByID(driver, Constants.btnOnlineProgram);
        logger.log(Status.INFO, "Filter screen opened");
        driver.findElementById(Constants.txtSkills).sendKeys(skill);
        scrollById(driver, Constants.btnApply);
        driver.findElementById(Constants.btnApply).click();
        logger.log(Status.INFO, "Filter applied for " + skill);
        waitInSeconds(3);
        if (driver.findElementsById(Constants.messagePopUp).size() == 1) {
            validatePopScreen(driver, Constants.searchResult0Msg);
            logger.log(Status.INFO, "Search returned no matches");
            waitUntilAndroidElementVisibleByxPath(driver, Constants.btnBack);
            driver.findElementByXPath(Constants.btnBack).click();
        } else {
            logger.log(Status.INFO, "Result found. Adding screenshot ...");
            takeScreenshot_Android(driver, screenShotAt + skill);
            logger.addScreenCaptureFromPath(screenShotAt + skill + ".jpg");
            waitUntilAndroidElementVisibleByID(driver, Constants.btnSearch);
            driver.findElementById(Constants.btnSearch).click();
        }
        waitUntilAndroidElementVisibleByID(driver, Constants.btnFilter);
    }

    public void filterByParticipationYear(AndroidDriver driver, ExtentTest logger) throws InterruptedException, IOException {
        String participationYear = "2020";
        clickFilterButton(driver, logger);
        waitUntilAndroidElementVisibleByID(driver, Constants.btnOnlineProgram);
        logger.log(Status.INFO, "Filter screen opened");
        driver.findElementById(Constants.txtParticipationYear).sendKeys(participationYear);
        scrollById(driver, Constants.btnApply);
        driver.findElementById(Constants.btnApply).click();
        logger.log(Status.INFO, "Filter applied for " + participationYear);
        waitInSeconds(3);
        if (driver.findElementsById(Constants.messagePopUp).size() == 1) {
            validatePopScreen(driver, Constants.searchResult0Msg);
            logger.log(Status.INFO, "Search returned no matches");
            waitUntilAndroidElementVisibleByxPath(driver, Constants.btnBack);
            driver.findElementByXPath(Constants.btnBack).click();
        } else {
            logger.log(Status.INFO, "Result found. Adding screenshot ...");
            takeScreenshot_Android(driver, screenShotAt + "Participation_" + participationYear);
            logger.addScreenCaptureFromPath(screenShotAt + "Participation_" + participationYear + ".jpg");
            waitUntilAndroidElementVisibleByID(driver, Constants.btnSearch);
            driver.findElementById(Constants.btnSearch).click();
        }
        waitUntilAndroidElementVisibleByID(driver, Constants.btnFilter);
    }

    public void filterByKeyword(AndroidDriver driver, ExtentTest logger) throws InterruptedException, IOException {
        String keyword = "Test";
        clickFilterButton(driver, logger);
        waitUntilAndroidElementVisibleByID(driver, Constants.btnOnlineProgram);
        logger.log(Status.INFO, "Filter screen opened");
        scrollById(driver, Constants.btnApply);
        driver.findElementById(Constants.txtKeywords).sendKeys(keyword);
        driver.findElementById(Constants.btnApply).click();
        logger.log(Status.INFO, "Filter applied for " + keyword);
        waitInSeconds(3);
        if (driver.findElementsById(Constants.messagePopUp).size() == 1) {
            validatePopScreen(driver, Constants.searchResult0Msg);
            logger.log(Status.INFO, "Search returned no matches");
            waitUntilAndroidElementVisibleByxPath(driver, Constants.btnBack);
            driver.findElementByXPath(Constants.btnBack).click();
        } else {
            logger.log(Status.INFO, "Result found. Adding screenshot ...");
            takeScreenshot_Android(driver, screenShotAt + keyword);
            logger.addScreenCaptureFromPath(screenShotAt + keyword + ".jpg");
            waitUntilAndroidElementVisibleByID(driver, Constants.btnSearch);
            driver.findElementById(Constants.btnSearch).click();
        }
        waitUntilAndroidElementVisibleByID(driver, Constants.btnFilter);
    }

    public void filterByProductAndServices(AndroidDriver driver, ExtentTest logger) throws InterruptedException, IOException {
        String pAnds = "Test";
        clickFilterButton(driver, logger);
        waitUntilAndroidElementVisibleByID(driver, Constants.btnOnlineProgram);
        logger.log(Status.INFO, "Filter screen opened");
        scrollById(driver, Constants.btnApply);
        driver.findElementById(Constants.txtProductAndService).sendKeys(pAnds);
        driver.findElementById(Constants.btnApply).click();
        logger.log(Status.INFO, "Filter applied for " + pAnds);
        waitInSeconds(3);
        if (driver.findElementsById(Constants.messagePopUp).size() == 1) {
            validatePopScreen(driver, Constants.searchResult0Msg);
            logger.log(Status.INFO, "Search returned no matches");
            waitUntilAndroidElementVisibleByxPath(driver, Constants.btnBack);
            driver.findElementByXPath(Constants.btnBack).click();
        } else {
            logger.log(Status.INFO, "Result found. Adding screenshot ...");
            takeScreenshot_Android(driver, screenShotAt + pAnds);
            logger.addScreenCaptureFromPath(screenShotAt + pAnds + ".jpg");
            waitUntilAndroidElementVisibleByID(driver, Constants.btnSearch);
            driver.findElementById(Constants.btnSearch).click();
        }
        waitUntilAndroidElementVisibleByID(driver, Constants.btnFilter);
    }

    public void closeFilter(AndroidDriver driver, ExtentTest logger) throws InterruptedException, UnsupportedEncodingException {
        clickFilterButton(driver, logger);
        waitUntilAndroidElementVisibleByID(driver, Constants.btnOnlineProgram);
        logger.log(Status.INFO, "Filter screen opened");
        scrollById(driver, Constants.btnApply);
        driver.findElementById(Constants.btnCancel).click();
        logger.log(Status.INFO, "Cancel button clicked");
        waitUntilAndroidElementVisibleByID(driver, Constants.btnFilter);
        logger.log(Status.INFO, "Redirected filter screen");
    }

    public void filterByOnlineOffering(AndroidDriver driver, ExtentTest logger) throws InterruptedException, IOException {
        String offeringName = "online 7- GBP - PAid";
        clickFilterButton(driver, logger);
        waitUntilAndroidElementVisibleByID(driver, Constants.btnOnlineProgram);
        logger.log(Status.INFO, "Filter screen opened");
        System.out.println(driver.findElementById(Constants.txtOfferingName).getText());
        System.out.println(driver.findElementById(Constants.txtOfferingDate).getText());
        driver.findElementById(Constants.btnOnlineProgram).click();
        logger.log(Status.INFO, "Online offering selected");
        waitUntilAndroidElementVisibleByID(driver, Constants.txtOfferingName);
        ScrollTabs(driver, offeringName);
        scrollById(driver, Constants.btnApply);
        logger.log(Status.INFO, "Filter applied");
        driver.findElementById(Constants.btnApply).click();
        waitInSeconds(3);
        if (driver.findElementsById(Constants.messagePopUp).size() == 1) {
            validatePopScreen(driver, Constants.searchResult0Msg);
            logger.log(Status.INFO, "Search returned no matches");
        } else {
            logger.log(Status.INFO, "Result found. Adding screenshot ...");
            takeScreenshot_Android(driver, screenShotAt + offeringName);
            logger.addScreenCaptureFromPath(screenShotAt + offeringName + ".jpg");
            waitUntilAndroidElementVisibleByID(driver, Constants.btnSearch);
            driver.findElementById(Constants.btnSearch).click();
        }
        waitUntilAndroidElementVisibleByID(driver, Constants.btnFilter);
    }

    public void filterByOfflineOffering(AndroidDriver driver, ExtentTest logger) throws InterruptedException, IOException {
        String offeringName = "Offering 7 - All Files";
        clickFilterButton(driver, logger);
        waitUntilAndroidElementVisibleByID(driver, Constants.btnOnlineProgram);
        logger.log(Status.INFO, "Filter screen opened");
        System.out.println(driver.findElementById(Constants.txtOfferingName).getText());
        System.out.println(driver.findElementById(Constants.txtOfferingDate).getText());
        driver.findElementById(Constants.btnEvent).click();
        logger.log(Status.INFO, "Offline offering selected");
        waitUntilAndroidElementVisibleByID(driver, Constants.txtOfferingName);
        ScrollTabs(driver, offeringName);
        scrollById(driver, Constants.btnApply);
        driver.findElementById(Constants.btnApply).click();
        waitInSeconds(3);
        if (driver.findElementsById(Constants.messagePopUp).size() == 1) {
            validatePopScreen(driver, Constants.searchResult0Msg);
            logger.log(Status.INFO, "Search returned no matches");
        } else {
            logger.log(Status.INFO, "Result found. Adding screenshot ...");
            takeScreenshot_Android(driver, screenShotAt + offeringName);
            logger.addScreenCaptureFromPath(screenShotAt + offeringName + ".jpg");
            waitUntilAndroidElementVisibleByID(driver, Constants.btnSearch);
            driver.findElementById(Constants.btnSearch).click();
        }
        waitUntilAndroidElementVisibleByID(driver, Constants.btnFilter);
    }

    public void ScrollTabs(AndroidDriver driver, String offeringName) throws InterruptedException {

        int YCoordinates = 590;
        List<MobileElement> offList = driver.findElementsById(Constants.txtOfferingName);
        while (!(offList.get(0).getText().trim().equals(offeringName) || offList.get(1).getText().trim().equals(offeringName))) {
            new TouchAction<>(driver)
                    .press(PointOption.point(408, YCoordinates)).waitAction(WaitOptions.waitOptions(Duration.ofMillis(2000)))
                    .moveTo(PointOption.point(65, YCoordinates)).release().perform();
            waitInSeconds(1);
            offList = driver.findElementsById(Constants.txtOfferingName);
        }

        if (offList.get(0).getText().trim().equals(offeringName)) {
            offList.get(0).click();
        } else if (offList.get(1).getText().trim().equals(offeringName)) {
            offList.get(1).click();
        }

    }

}
