package pages;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import org.testng.Assert;
import others.Constants;
import testBase.TestBase;

import java.io.UnsupportedEncodingException;
import java.util.List;

public class OfferingDetailsPage extends TestBase {

    EducatorPage educator = new EducatorPage();

    public void registerInOnlineOngoingFreeOffering(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        String txtSearch = "Mvp1";
        String companyName = "Mvp1ventures";
        String offeringName = "Test_Automation (online, Ongoing, Free)";
        MobileElement oe = (MobileElement) educator.getOffering(driver, logger, txtSearch, companyName, offeringName);
        if (oe != null) {
            oe.findElementById(Constants.titleName).click();
            logger.log(Status.INFO,"Offering opened.");
            registerInFreeOffering(driver, logger, false, "", "");
        }
    }

    public void registerInOnlineSpecificPeriodFreeOffering(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        String txtSearch = "Mvp1";
        String companyName = "Mvp1ventures";
        String offeringName = "Test_Automation (online, specificperiod, Free)";
        MobileElement oe = (MobileElement) educator.getOffering(driver, logger, txtSearch, companyName, offeringName);
        if (oe != null) {
            oe.findElementById(Constants.titleName).click();
            logger.log(Status.INFO,"Offering opened.");
            registerInFreeOffering(driver, logger, false, "", "");
        }
    }

    public void registerInOfflineOneTimeFreeOffering(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        String txtSearch = "Mvp1";
        String companyName = "Mvp1ventures";
        String offeringName = "Test_Automation (online, Ongoing, Onetime)";
        MobileElement oe = (MobileElement) educator.getOffering(driver, logger, txtSearch, companyName, offeringName);
        if (oe != null) {
            oe.findElementById(Constants.titleName).click();
            logger.log(Status.INFO,"Offering opened.");
            registerInFreeOffering(driver, logger, false, "", "");
        }
    }

    public void registerInOfflineRecurringFreeOffering(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        String txtSearch = "Mvp1";
        String companyName = "Mvp1ventures";
        String offeringName = "Test_Automation (offline, recurringschedule, Free)";
        String sDate = "06 Nov 2020 04:37 PM";
        String eDate = "30 Nov 2020 07:37 PM";
        MobileElement oe = (MobileElement) educator.getOffering(driver, logger, txtSearch, companyName, offeringName);
        if (oe != null) {
            oe.findElementById(Constants.titleName).click();
            logger.log(Status.INFO,"Offering opened.");
            registerInFreeOffering(driver, logger, true, sDate, eDate);
        }
    }

    public void registerInOnlineOngoingPaidOffering(AndroidDriver driver, ExtentTest logger) throws InterruptedException, UnsupportedEncodingException {
        String txtSearch = "Mvp1";
        String companyName = "Mvp1ventures";
        String offeringName = "Test_Automation (online, specificperiod, Free)";
        MobileElement oe = (MobileElement) educator.getOffering(driver, logger, txtSearch, companyName, offeringName);
        if (oe != null) {
            oe.findElementById(Constants.titleName).click();
            logger.log(Status.INFO,"Offering opened.");
            registerInOnlinePaidOffering(driver, logger);
        }
    }

    public void registerInOnlineSpecificPeriodPaidOffering(AndroidDriver driver, ExtentTest logger) throws InterruptedException, UnsupportedEncodingException {
        String txtSearch = "Mvp1";
        String companyName = "Mvp1ventures";
        String offeringName = "Test_Automation (online, specificperiod, Onetime)";
        MobileElement oe = (MobileElement) educator.getOffering(driver, logger, txtSearch, companyName, offeringName);
        if (oe != null) {
            oe.findElementById(Constants.titleName).click();
            logger.log(Status.INFO,"Offering opened.");
            registerInOnlinePaidOffering(driver, logger);
        }
    }

    public void registerInOnlineOngoingSubscriptionOffering(AndroidDriver driver, ExtentTest logger) throws InterruptedException, UnsupportedEncodingException {
        String txtSearch = "Mvp1";
        String companyName = "Mvp1ventures";
        String offeringName = "Test_Automation (online, Ongoing, Subscription)";
        MobileElement oe = (MobileElement) educator.getOffering(driver, logger, txtSearch, companyName, offeringName);
        if (oe != null) {
            oe.findElementById(Constants.titleName).click();
            logger.log(Status.INFO,"Offering opened.");
            registerInOnlinePaidOffering(driver, logger);
        }
    }

    public void registerInOnlineSpecificPeriodSubscriptionOffering(AndroidDriver driver, ExtentTest logger) throws InterruptedException, UnsupportedEncodingException {
        String txtSearch = "Mvp1";
        String companyName = "Mvp1ventures";
        String offeringName = "Test_Automation (online, specificperiod, Subscription)";
        MobileElement oe = (MobileElement) educator.getOffering(driver, logger, txtSearch, companyName, offeringName);
        if (oe != null) {
            oe.findElementById(Constants.titleName).click();
            logger.log(Status.INFO,"Offering opened.");
            registerInOnlinePaidOffering(driver, logger);
        }
    }

    public void registerInOfflineOneTimePaidOffering(AndroidDriver driver, ExtentTest logger) throws InterruptedException, UnsupportedEncodingException {
        String txtSearch = "Mvp1";
        String companyName = "Mvp1ventures";
        String offeringName = "Test_Automation (offline, onetimeschedule, Onetime)";
        MobileElement oe = (MobileElement) educator.getOffering(driver, logger, txtSearch, companyName, offeringName);
        if (oe != null) {
            oe.findElementById(Constants.titleName).click();
            logger.log(Status.INFO,"Offering opened.");
            registerInPaidOfflineOffering(driver, logger, "", "", false, false);
        }
    }

    public void registerInOfflineRecurringPaidOffering(AndroidDriver driver, ExtentTest logger) throws InterruptedException, UnsupportedEncodingException {
        String txtSearch = "Mvp1";
        String companyName = "Mvp1ventures";
        String offeringName = "Test_Automation (offline, recurringschedule, Onetime)";
        String sDate = "06 Nov 2020 18:13 PM";
        String eDate = "30 Nov 2020 11:13 AM";
        MobileElement oe = (MobileElement) educator.getOffering(driver, logger, txtSearch, companyName, offeringName);
        if (oe != null) {
            oe.findElementById(Constants.titleName).click();
            logger.log(Status.INFO,"Offering opened.");
            registerInPaidOfflineOffering(driver, logger, sDate, eDate, true, false);
        }
    }
    //////////////////////////////////////////////////////////////////////////
    public void registerInOnlineOngoingFreeOffering_UpcomingSection(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        String txtSearch = "Mvp1";
        String companyName = "Mvp1ventures";
        String offeringName = "Test_Automation (online, Ongoing, Free)";
        MobileElement oe = (MobileElement) educator.getOfferingFromUpcomingSection(driver, logger, txtSearch, companyName, offeringName);
        if (oe != null) {
            oe.findElementById(Constants.upcomingOffRegister).click();
            logger.log(Status.INFO,"Offering opened.");
            registerInFreeOffering(driver, logger, false, "", "");
        }
    }

    public void registerInOnlineSpecificPeriodFreeOffering_UpcomingSection(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        String txtSearch = "Mvp1";
        String companyName = "Mvp1ventures";
        String offeringName = "Test_Automation (online, specificperiod, Free)";
        MobileElement oe = (MobileElement) educator.getOfferingFromUpcomingSection(driver, logger, txtSearch, companyName, offeringName);
        if (oe != null) {
            oe.findElementById(Constants.upcomingOffRegister).click();
            logger.log(Status.INFO,"Offering opened.");
            registerInFreeOffering(driver, logger, false, "", "");
        }
    }

    public void registerInOfflineOneTimeFreeOffering_UpcomingSection(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        String txtSearch = "Mvp1";
        String companyName = "Mvp1ventures";
        String offeringName = "Test_Automation (online, Ongoing, Onetime)";
        MobileElement oe = (MobileElement) educator.getOfferingFromUpcomingSection(driver, logger, txtSearch, companyName, offeringName);
        if (oe != null) {
            oe.findElementById(Constants.upcomingOffRegister).click();
            logger.log(Status.INFO,"Offering opened.");
            registerInFreeOffering(driver, logger, false, "", "");
        }
    }

    public void registerInOfflineRecurringFreeOffering_UpcomingSection(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        String txtSearch = "Mvp1";
        String companyName = "Mvp1ventures";
        String offeringName = "Test_Automation (offline, recurringschedule, Free)";
        String sDate = "06 Nov 2020 04:37 PM";
        String eDate = "30 Nov 2020 07:37 PM";
        MobileElement oe = (MobileElement) educator.getOfferingFromUpcomingSection(driver, logger, txtSearch, companyName, offeringName);
        if (oe != null) {
            oe.findElementById(Constants.upcomingOffRegister).click();
            logger.log(Status.INFO,"Offering opened.");
            registerInFreeOffering(driver, logger, true, sDate, eDate);
        }
    }

    public void registerInOnlineOngoingPaidOffering_UpcomingSection(AndroidDriver driver, ExtentTest logger) throws InterruptedException, UnsupportedEncodingException {
        String txtSearch = "Mvp1";
        String companyName = "Mvp1ventures";
        String offeringName = "Test_Automation (online, specificperiod, Free)";
        MobileElement oe = (MobileElement) educator.getOfferingFromUpcomingSection(driver, logger, txtSearch, companyName, offeringName);
        if (oe != null) {
            oe.findElementById(Constants.upcomingOffRegister).click();
            logger.log(Status.INFO,"Offering opened.");
            registerInOnlinePaidOffering(driver, logger);
        }
    }

    public void registerInOnlineSpecificPeriodPaidOffering_UpcomingSection(AndroidDriver driver, ExtentTest logger) throws InterruptedException, UnsupportedEncodingException {
        String txtSearch = "Mvp1";
        String companyName = "Mvp1ventures";
        String offeringName = "Test_Automation (online, specificperiod, Onetime)";
        MobileElement oe = (MobileElement) educator.getOfferingFromUpcomingSection(driver, logger, txtSearch, companyName, offeringName);
        if (oe != null) {
            oe.findElementById(Constants.upcomingOffRegister).click();
            logger.log(Status.INFO,"Offering opened.");
            registerInOnlinePaidOffering(driver, logger);
        }
    }

    public void registerInOnlineOngoingSubscriptionOffering_UpcomingSection(AndroidDriver driver, ExtentTest logger) throws InterruptedException, UnsupportedEncodingException {
        String txtSearch = "Mvp1";
        String companyName = "Mvp1ventures";
        String offeringName = "Test_Automation (online, Ongoing, Subscription)";
        MobileElement oe = (MobileElement) educator.getOfferingFromUpcomingSection(driver, logger, txtSearch, companyName, offeringName);
        if (oe != null) {
            oe.findElementById(Constants.upcomingOffRegister).click();
            logger.log(Status.INFO,"Offering opened.");
            registerInOnlinePaidOffering(driver, logger);
        }
    }

    public void registerInOnlineSpecificPeriodSubscriptionOffering_UpcomingSection(AndroidDriver driver, ExtentTest logger) throws InterruptedException, UnsupportedEncodingException {
        String txtSearch = "Mvp1";
        String companyName = "Mvp1ventures";
        String offeringName = "Test_Automation (online, specificperiod, Subscription)";
        MobileElement oe = (MobileElement) educator.getOfferingFromUpcomingSection(driver, logger, txtSearch, companyName, offeringName);
        if (oe != null) {
            oe.findElementById(Constants.upcomingOffRegister).click();
            logger.log(Status.INFO,"Offering opened.");
            registerInOnlinePaidOffering(driver, logger);
        }
    }

    public void registerInOfflineOneTimePaidOffering_UpcomingSection(AndroidDriver driver, ExtentTest logger) throws InterruptedException, UnsupportedEncodingException {
        String txtSearch = "Mvp1";
        String companyName = "Mvp1ventures";
        String offeringName = "Test_Automation (offline, onetimeschedule, Onetime)";
        MobileElement oe = (MobileElement) educator.getOfferingFromUpcomingSection(driver, logger, txtSearch, companyName, offeringName);
        if (oe != null) {
            oe.findElementById(Constants.upcomingOffRegister).click();
            logger.log(Status.INFO,"Offering opened.");
            registerInPaidOfflineOffering(driver, logger, "", "", false, false);
        }
    }

    public void registerInOfflineRecurringPaidOffering_UpcomingSection(AndroidDriver driver, ExtentTest logger) throws InterruptedException, UnsupportedEncodingException {
        String txtSearch = "Mvp1";
        String companyName = "Mvp1ventures";
        String offeringName = "Test_Automation (offline, recurringschedule, Onetime)";
        String sDate = "06 Nov 2020 18:13 PM";
        String eDate = "30 Nov 2020 11:13 AM";
        MobileElement oe = (MobileElement) educator.getOfferingFromUpcomingSection(driver, logger, txtSearch, companyName, offeringName);
        if (oe != null) {
            oe.findElementById(Constants.upcomingOffRegister).click();
            logger.log(Status.INFO,"Offering opened.");
            registerInPaidOfflineOffering(driver, logger, sDate, eDate, true, false);
        }
    }
    //////////////////////////////////////////////////////////////////////////

    public void registerInOfflineOneTimePaidOffering_PromoCode(AndroidDriver driver, ExtentTest logger) throws InterruptedException, UnsupportedEncodingException {
        String txtSearch = "Mvp1";
        String companyName = "Mvp1ventures";
        String offeringName = "Promocode";
        MobileElement oe = (MobileElement) educator.getOffering(driver, logger, txtSearch, companyName, offeringName);
        if (oe != null) {
            oe.findElementById(Constants.titleName).click();
            logger.log(Status.INFO,"Offering opened.");
            registerInPaidOfflineOffering(driver, logger, "", "", false, true);
        }
    }
    /////////////////////////////////////////////////////////////////////////
    public void addToCalendar_Offering(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        String txtSearch = "Mvp1";
        String companyName = "Mvp1ventures";
        String offeringName = "Test";
        MobileElement oe = (MobileElement) educator.getOffering(driver, logger, txtSearch, companyName, offeringName);
        if (oe != null) {
            oe.findElementById(Constants.titleName).click();
            logger.log(Status.INFO,"Offering opened");
            waitUntilAndroidElementVisibleByID(driver, Constants.btnAddToCalender);
            driver.findElementById(Constants.btnAddToCalender).click();
            waitUntilAndroidElementVisibleByID(driver, Constants.btnCalendar);
            logger.log(Status.INFO,"Content consumption page opened");
            driver.findElementById(Constants.btnCalendar).click();
            waitUntilAndroidElementVisibleByID(driver, Constants.btnCalendarAdded);
            driver.findElementById(Constants.btnCalendarAdded).click();
            logger.log(Status.INFO,"Save in calendar");
            driver.hideKeyboard();
            waitUntilAndroidElementVisibleByxPath(driver, Constants.btnBack);
            driver.findElementByXPath(Constants.btnBack).click();
            waitUntilAndroidElementVisibleByID(driver, Constants.btnBack2);
            driver.findElementById(Constants.btnBack2).click();
            waitUntilAndroidElementVisibleByID(driver, Constants.btnHomeTB);
        }
    }

    public void addToCalendar_Offering_MoreDetailsPage(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        String txtSearch = "Mvp1";
        String companyName = "Mvp1ventures";
        String offeringName = "Test";
        MobileElement oe = (MobileElement) educator.getOffering(driver, logger, txtSearch, companyName, offeringName);
        if (oe != null) {
            oe.findElementById(Constants.titleName).click();
            logger.log(Status.INFO,"Offering opened");
            waitUntilAndroidElementVisibleByID(driver, Constants.btnAccessNow);
            driver.findElementById(Constants.btnAccessNow).click();
            waitUntilAndroidElementVisibleByID(driver, Constants.btnMore);
            logger.log(Status.INFO,"Content consumption page opened");
            driver.findElementById(Constants.btnMore).click();
            waitUntilAndroidElementVisibleByID(driver, Constants.btnAddToCalender);
            logger.log(Status.INFO,"More button clicked and offering details opened");
            driver.findElementById(Constants.btnAddToCalender).click();
            waitUntilAndroidElementVisibleByID(driver, Constants.btnCalendar);
            driver.findElementById(Constants.btnCalendar).click();
            waitUntilAndroidElementVisibleByID(driver, Constants.btnCalendarAdded);
            driver.findElementById(Constants.btnCalendarAdded).click();
            logger.log(Status.INFO,"Save in calendar");
            driver.hideKeyboard();
            waitUntilAndroidElementVisibleByxPath(driver, Constants.btnBack);
            driver.findElementByXPath(Constants.btnBack).click();
            waitUntilAndroidElementVisibleByID(driver, Constants.btnBack2);
            driver.findElementById(Constants.btnBack2).click();
            waitUntilAndroidElementVisibleByID(driver, Constants.btnHomeTB);
        }
    }

    public void registerInFreeOffering(AndroidDriver driver, ExtentTest logger, Boolean isRecurring, String sDate, String eDate) throws InterruptedException {
        Boolean isScrollNeeded = true;
        waitUntilAndroidElementVisibleByID(driver, Constants.btnAccessNow);
        if(driver.findElementById(Constants.btnAccessNow).getText().equals("Register") && driver.findElementById(Constants.priceOffering).getText().equals("FREE")) {
            driver.findElementById(Constants.btnAccessNow).click();
            logger.log(Status.INFO,"Register button clicked");
            if(isRecurring){
                waitUntilAndroidElementVisibleByID(driver, Constants.scheduleListEle);
                logger.log(Status.INFO,"Schedule list opened");
                MobileElement sl = (MobileElement) driver.findElementById(Constants.scheduleListEle);
                List<MobileElement> scheduleList = sl.findElementsByXPath(Constants.scheduleList);
                //System.out.println(scheduleList.size());
                for (MobileElement dl : scheduleList) {
                    if (dl.findElementsById(Constants.sDate).size() > 0 && dl.findElementsById(Constants.eDate).size() > 0) {
                        //System.out.println(dl.findElementById(Constants.sDate).getText());
                        //System.out.println(dl.findElementById(Constants.eDate).getText());
                        if (dl.findElementById(Constants.sDate).getText().trim().equals(sDate) && dl.findElementById(Constants.eDate).getText().trim().equals(eDate)) {
                            dl.findElementById(Constants.sDate).click();
                            logger.log(Status.INFO,"Schedule selected");
                            isScrollNeeded = false;
                            break;
                        }
                    }
                }
                driver.findElementById(Constants.btnOK).click();
            }
            waitUntilAndroidElementVisibleByID(driver, Constants.btnAccessNow);
            Assert.assertEquals(driver.findElementById(Constants.btnAccessNow).getText(),"Access Now");
            logger.log(Status.INFO,"Verified: Button changed to Access Now");
            waitUntilAndroidElementVisibleByxPath(driver, Constants.btnBack);
            driver.findElementByXPath(Constants.btnBack).click();
            waitUntilAndroidElementVisibleByID(driver, Constants.eduDetailsPage);
            waitUntilAndroidElementVisibleByID(driver, Constants.btnBack2);
            driver.findElementById(Constants.btnBack2).click();
            waitUntilAndroidElementVisibleByID(driver, Constants.btnHomeTB);
        }
    }

    public void registerInPaidOfflineOffering(AndroidDriver driver, ExtentTest logger, String sDate, String eDate, Boolean isRecurring, Boolean promoCode) throws InterruptedException, UnsupportedEncodingException {
        Boolean isScrollNeeded = true;
        waitUntilAndroidElementVisibleByID(driver, Constants.btnAccessNow);
        if(driver.findElementById(Constants.btnAccessNow).getText().equals("Register") && !(driver.findElementById(Constants.priceOffering).getText().equals("FREE"))) {
            driver.findElementById(Constants.btnAccessNow).click();
            logger.log(Status.INFO,"Buy Now Button clicked");
            if(isRecurring){
                waitUntilAndroidElementVisibleByID(driver, Constants.scheduleListEle);
                MobileElement sl = (MobileElement) driver.findElementById(Constants.scheduleListEle);
                List<MobileElement> scheduleList = sl.findElementsByXPath(Constants.scheduleList);
                //System.out.println(scheduleList.size());
                for (MobileElement dl : scheduleList) {
                    if (dl.findElementsById(Constants.sDate).size() > 0 && dl.findElementsById(Constants.eDate).size() > 0) {
                        if (dl.findElementById(Constants.sDate).getText().equals(sDate) && dl.findElementById(Constants.eDate).getText().equals(eDate)) {
                            dl.findElementById(Constants.sDate).click();
                            logger.log(Status.INFO,"Schedule selected");
                            isScrollNeeded = false;
                            break;
                        }
                    }
                }
                driver.findElementById(Constants.btnOK).click();
            }
            waitInSeconds(3);
            if (driver.findElementsById(Constants.chkTerms).size() == 1){
                driver.findElementById(Constants.chkTerms).click();
                driver.findElementById(Constants.btnOK2).click();
                logger.log(Status.INFO,"Terms & conditions accepted");
            }
            waitUntilAndroidElementVisibleByID(driver, Constants.btnPay);
            if(driver.findElementsById(Constants.isCardSelected).size() == 0){
                driver.findElementById(Constants.btnAddCard_Payment).click();
                waitUntilAndroidElementVisibleByID(driver, Constants.txtCardNumber);
                logger.log(Status.INFO,"Add card screen opened");
                driver.findElementById(Constants.txtCardNumber).sendKeys("4242424242424242");
                driver.findElementById(Constants.txtMonthYear).sendKeys("1222");
                driver.findElementById(Constants.txtCVV).sendKeys("111");
                driver.hideKeyboard();
                driver.findElementById(Constants.btnSaveCard).click();
                validatePopScreen(driver, Constants.addCardSuccessMsg);
                logger.log(Status.INFO,"Card added successfully.");
                waitInSeconds(1);
                waitUntilAndroidElementVisibleByID(driver, Constants.isCardSelected);
            }
            if(promoCode){
                driver.findElementById(Constants.txtPromoCode).sendKeys("test2345");
                driver.findElementById(Constants.btnApplyPromoCode).click();
                waitUntilAndroidElementVisibleByID(driver, Constants.txtDiscount);
                logger.log(Status.INFO,"PromoCode applied");
                logger.log(Status.INFO, "Discount: " + driver.findElementById(Constants.txtDiscount).getText());
            }
            driver.findElementById(Constants.btnPay).click();
            validatePopScreen(driver, Constants.paymentSuccessMsg);
            waitUntilAndroidElementVisibleByID(driver, Constants.btnAccessNow);
            Assert.assertEquals(driver.findElementById(Constants.btnAccessNow).getText(),"Access Now");
            logger.log(Status.INFO,"Verified: Button changed to Access Now");
            waitUntilAndroidElementVisibleByxPath(driver, Constants.btnBack);
            driver.findElementByXPath(Constants.btnBack).click();
            waitUntilAndroidElementVisibleByID(driver, Constants.eduDetailsPage);
            waitUntilAndroidElementVisibleByID(driver, Constants.btnBack2);
            driver.findElementById(Constants.btnBack2).click();
            waitUntilAndroidElementVisibleByID(driver, Constants.btnHomeTB);
        }
    }

    public void registerInOnlinePaidOffering(AndroidDriver driver, ExtentTest logger) throws InterruptedException, UnsupportedEncodingException {
        waitUntilAndroidElementVisibleByID(driver, Constants.btnAccessNow);
        String popMSG = "We\\u2019re sending you an email to provide you with more info on " + driver.findElementById(Constants.txtOfferingName).getText() + ". Go check it out!";
        if(driver.findElementById(Constants.btnAccessNow).getText().equals("I'm Interested") && !(driver.findElementById(Constants.priceOffering).getText().equals("FREE"))) {
            driver.findElementById(Constants.btnAccessNow).click();
            logger.log(Status.INFO,"I'm Interested Button clicked");
            validatePopScreen(driver, popMSG);
            logger.log(Status.INFO,"Mail sent");
            waitUntilAndroidElementVisibleByxPath(driver, Constants.btnBack);
            driver.findElementByXPath(Constants.btnBack).click();
            waitUntilAndroidElementVisibleByID(driver, Constants.eduDetailsPage);
            waitUntilAndroidElementVisibleByID(driver, Constants.btnBack2);
            driver.findElementById(Constants.btnBack2).click();
            waitUntilAndroidElementVisibleByID(driver, Constants.btnHomeTB);
        }
    }

}
