package pages;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import others.Constants;
import others.leftMenu;
import testBase.TestBase;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.zip.CheckedOutputStream;

public class ExplorePage extends TestBase {

    NotificationPage nPage = new NotificationPage();
    leftMenu lMenu = new leftMenu();

    public MobileElement getEducatorFromExplorePage(AndroidDriver driver, ExtentTest logger, String companyName) throws InterruptedException {
        waitUntilAndroidElementVisibleByID(driver, Constants.businessRefExp);
        driver.findElementById(Constants.businessRefExp).click();
        waitUntilAndroidElementVisibleByID(driver, Constants.expEdus);
        logger.log(Status.INFO, "Explore Page opened");
        scrollToElement(driver, companyName);
        java.util.List<MobileElement> educatorsList = (List<MobileElement>) driver.findElementsById(Constants.expEdus);
        System.out.println(educatorsList.size());
        for (MobileElement sl : educatorsList) {
            if (sl.findElementsById(Constants.txtCompanyName).size() > 0) {
                if (sl.findElementById(Constants.txtCompanyName).getText().trim().equals(companyName)) {
                    logger.log(Status.INFO, "Educator found");
                    return (MobileElement) sl;
                }
            }
        }
        return null;
    }

    public void sentSupportRequest_ExplorePage(AndroidDriver driver, ExtentTest logger) throws InterruptedException, UnsupportedEncodingException {
        String companyName = "Breathe Me";
        String date = "13/05/2020";
        String srTitle = "test";
        String srDesc = "test";
        waitUntilAndroidElementVisibleByID(driver, Constants.businessRefExp);
        driver.findElementById(Constants.businessRefExp).click();
        waitUntilAndroidElementVisibleByID(driver, Constants.expEdus);
        logger.log(Status.INFO, "Explore Page opened");
        scrollToElement(driver, companyName);
        java.util.List<MobileElement> educatorsList = (List<MobileElement>) driver.findElementsById(Constants.expEdus);
        System.out.println(educatorsList.size());
        for (MobileElement sl : educatorsList) {
            if (sl.findElementsById(Constants.txtCompanyName).size() > 0) {
                if (sl.findElementById(Constants.txtCompanyName).getText().trim().equals(companyName)) {
                    logger.log(Status.INFO, "Educator found");
                    sl.findElementById(Constants.btnSentSupportRequest).click();
                    waitUntilAndroidElementVisibleByID(driver, Constants.supportReqTitle);
                    logger.log(Status.INFO,"Adding support request");
                    driver.findElementById(Constants.supportReqTitle).sendKeys(srTitle);
                    driver.findElementById(Constants.supportReqDesc).sendKeys(srDesc);
                    driver.findElementById(Constants.btnSRSubmit).click();
                    logger.log(Status.INFO,"Support request Added");
                    validatePopScreen(driver, Constants.sentSupportRequestMsg);
                    waitUntilAndroidElementVisibleByxPath(driver, Constants.btnLeftMenu);
                    lMenu.displayLeftMenu(driver, logger);
                    lMenu.displayNotification(driver, logger);
                    logger.log(Status.INFO,"Verify: Support request added or not?");
                    nPage.isSupportRequestSent(driver, logger, companyName, date, srTitle, srDesc);
                    break;
                }
            }
        }
    }

}
