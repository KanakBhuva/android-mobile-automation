package pages;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import org.testng.Assert;
import others.Constants;
import testBase.TestBase;

import java.io.UnsupportedEncodingException;
import java.util.List;

public class MessagesPage extends TestBase {

    public void sendMessage(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        waitUntilAndroidElementVisibleByID(driver, Constants.btnMessage);
        String userName = "Kanak Bhuva";
        String message = "Hi! How are you?";
        String sentMessage = "";
        driver.findElementById(Constants.btnMessage).click();
        logger.log(Status.INFO,"Message screen opened");
        if (driver.findElementsById(Constants.txtNoData).size() != 1) {
            waitUntilAndroidElementVisibleByID(driver, Constants.userNameSec);
            MobileElement cl = (MobileElement) driver.findElementById(Constants.userNameSec);
            List<MobileElement> crUserList = cl.findElementsById(Constants.EduMessageList);
            //System.out.println(crUserList.size());
            for (MobileElement ul : crUserList) {
                //System.out.println(ul.findElementById(Constants.eduName).getText());
                if (ul.findElementById(Constants.eduName).getText().equals(userName)) {
                    ul.findElementById(Constants.eduName).click();
                    waitUntilAndroidElementVisibleByID(driver, Constants.btnSendMessage);
                    logger.log(Status.INFO,"Chat screen opened");
                    driver.findElementById(Constants.txtMessage).sendKeys(message);
                    driver.findElementById(Constants.btnSendMessage).click();
                    waitUntilAndroidElementVisibleByID(driver, Constants.btnBack2);
                    logger.log(Status.INFO,"Sent button clicked");
                    MobileElement ml = (MobileElement) driver.findElementById(Constants.chatScreen);
                    List<MobileElement> msgList = ml.findElementsByXPath(Constants.chatScreenLastMessage);
                    sentMessage = msgList.get(msgList.size() - 1).findElementById(Constants.txtMessageInScreen).getText();
                    Assert.assertEquals(sentMessage, message);
                    driver.findElementById(Constants.btnBack2).click();
                    break;
                }
            }
        }
    }

    public MobileElement findConnectionRequest(AndroidDriver driver, ExtentTest logger, String userName) throws InterruptedException {
        waitUntilAndroidElementVisibleByID(driver, Constants.btnRequest);
        driver.findElementById(Constants.btnRequest).click();
        logger.log(Status.INFO,"Connection request screen opened");
        waitInSeconds(2);
        if (driver.findElementsById(Constants.txtNoData).size() != 1) {
            waitUntilAndroidElementVisibleByID(driver, Constants.userNameSec);
            MobileElement cl = (MobileElement) driver.findElementById(Constants.userNameSec);
            List<MobileElement> crUserList = cl.findElementsByXPath(Constants.reqUserNameList);
            for (MobileElement ul : crUserList) {
                System.out.println(driver.findElementById(Constants.reqUserName).getText());
                if (driver.findElementById(Constants.reqUserName).getText().equals(userName)) {
                    logger.log(Status.INFO,"Connection request found from " + userName);
                    return ul;
                }
            }
        }
        return null;
    }

    public void acceptConnectionRequest(AndroidDriver driver, ExtentTest logger) throws InterruptedException, UnsupportedEncodingException {
        String userName = "Bhavin Bhuva";
        MobileElement uEle = findConnectionRequest(driver, logger, userName);
        if (uEle != null) {
            uEle.findElementById(Constants.requestAccept).click();
            validatePopScreen(driver, Constants.requestAccepted);
            logger.log(Status.INFO,"Accept Button clicked");
        }
    }

    public void deleteConnectionRequest(AndroidDriver driver, ExtentTest logger) throws InterruptedException, UnsupportedEncodingException {
        String userName = "Bhavin Bhuva";
        MobileElement uEle = findConnectionRequest(driver, logger, userName);
        if (uEle != null) {
            uEle.findElementById(Constants.requestDelete).click();
            validatePopScreen(driver, Constants.requestDeleted);
            logger.log(Status.INFO,"Deleted Button clicked");
        }
    }
}
