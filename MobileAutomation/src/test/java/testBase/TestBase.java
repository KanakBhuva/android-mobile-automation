package testBase;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import fileUtility.ReadWriteJson;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import org.apache.commons.io.FileUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.json.simple.JSONObject;
import org.openqa.selenium.*;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import others.Constants;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.UUID;

public class TestBase {

    //System.out.println(driver.currentActivity().toLowerCase());

    public TestBase() {
    }

    public DesiredCapabilities setDesiredCapabilities() {
        DesiredCapabilities dc = new DesiredCapabilities();
        //dc.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 6000);
        dc.setCapability(AndroidMobileCapabilityType.AUTO_GRANT_PERMISSIONS, true);
        dc.setCapability(MobileCapabilityType.AUTOMATION_NAME, "Appium");
        dc.setCapability(MobileCapabilityType.DEVICE_NAME, "KNK"); // Android Emulator
        dc.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
        dc.setCapability(MobileCapabilityType.PLATFORM_VERSION, "9.0");
        dc.setCapability("udid", "49cbb0b8");
        dc.setCapability("appPackage", "com.mvp1ventures.masterdojo");
        dc.setCapability("appActivity", "com.mvp1ventures.coachlink.view.SplashActivity");
        return dc;
    }

    public WebDriver initializeChromeDriver() throws MalformedURLException, InterruptedException {
        DesiredCapabilities dc = setDesiredCapabilities();
        URL url = new URL("http://127.0.0.1:4372/wd/hub");
        AndroidDriver driver = new AndroidDriver(url, dc);
        Thread.sleep(5000);
        return driver;
    }

    public JSONObject readJsonFile() {
        ReadWriteJson json = new ReadWriteJson();
        Object obj = json.readJson(Constants.jsonPath);
        JSONObject wEJsonObj = (JSONObject) obj;
        return wEJsonObj;
    }

    public void openProfile(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        waitUntilAndroidElementVisibleByID(driver, Constants.btnSetting);
        driver.findElementById(Constants.btnSetting).click();
        waitInSeconds(1);
        waitUntilAndroidElementVisibleByxPath(driver, Constants.btnLeftMenu);
        logger.log(Status.INFO, "Profile screen displayed");
    }

    public void goToHomeInAndroid(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        waitUntilAndroidElementVisibleByID(driver, Constants.btnHomeTB);
        driver.findElementById(Constants.btnHomeTB).click();
        waitInSeconds(1);
        waitUntilAndroidElementVisibleByxPath(driver, Constants.btnLeftMenu);
        logger.log(Status.INFO, "Home screen displayed");
    }

    public void backFromEducatorScreen(AndroidDriver driver, ExtentTest logger) throws InterruptedException {
        waitUntilAndroidElementVisibleByID(driver, Constants.btnBack2);
        driver.findElementById(Constants.btnBack2).click();
        logger.log(Status.INFO, "Back button clicked");
    }

    public void scrollAndClick(AndroidDriver driver, String visibleText) {
        driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().text(\"" + visibleText + "\").instance(0))").click();
    }

    public void scrollToElement(AndroidDriver driver, String visibleText) {
        driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().text(\"" + visibleText + "\").instance(0))");
    }

    public void scrollAndSendKeyById(AndroidDriver driver, String txtLabel, String txtValue) {
        driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().resourceId(\"" + txtLabel + "\").instance(0))").sendKeys(txtValue);
    }

    public void scrollById(AndroidDriver driver, String txtLabel) {
        driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().resourceId(\"" + txtLabel + "\").instance(0))");
    }

    public void waitUntilAndroidElementVisibleByID(AndroidDriver driver, String id) throws InterruptedException {
        do {
            Thread.sleep(1000);
            //System.out.println(driver.findElementsById(id).size());
        } while (driver.findElementsById(id).size() == 0);
    }

    public void waitUntilAndroidElementVisibleByxPath(AndroidDriver driver, String xPath) throws InterruptedException {
        do {
            Thread.sleep(1000);
            //System.out.println(driver.findElementsByXPath(xPath).size());
        } while (driver.findElementsByXPath(xPath).size() == 0);
    }

    public void validatePopScreen(AndroidDriver driver, String message) throws InterruptedException, UnsupportedEncodingException {
        waitUntilAndroidElementVisibleByID(driver, Constants.messagePopUp);
        String str = driver.findElementById(Constants.messagePopUp).getText().trim();
        String subject = new String(str.getBytes("UTF-8"), "UTF-8");
        message = new String(message.getBytes("UTF-8"), "UTF-8");
        subject = StringEscapeUtils.escapeJson(subject);
        System.out.println("Actual: " + subject);
        System.out.println("Expected: " + message);
        Assert.assertEquals(subject, message);
        driver.findElementById(Constants.btnOK).click();
    }

    public void searchAndClick(AndroidDriver driver, String sListxPath, String searchID, String visibleText) {
        Boolean isFound = false;
        java.util.List<MobileElement> searchList = (List<MobileElement>) driver.findElementsByXPath(sListxPath);
        System.out.println("Search items found: " + searchList.size());
        for (MobileElement sl : searchList) {
            if (sl.findElementsById(searchID).size() > 0) {
                if (sl.findElementById(searchID).getText().trim().equals(visibleText)) {
                    isFound = true;
                    sl.findElementById(searchID).click();
                    break;
                }
            }
        }
        if (!isFound) {
            scrollAndClick(driver, visibleText);
        }
    }

    public void maximizeBrowser(WebDriver driver, ExtentTest logger) {
        driver.manage().window().maximize();
        logger.log(Status.INFO, "Browser maximized successfully.");
    }

    public void gotoURL(WebDriver driver, ExtentTest logger) {
        JSONObject wEJsonObj = readJsonFile();
        //driver.navigate().to((String) wEJsonObj.get("URL"));
        driver.get((String) wEJsonObj.get("URL"));
        logger.log(Status.INFO, (String) wEJsonObj.get("URL") + " opened successfully.");
    }

    public void closeBrowser(WebDriver driver, ExtentTest logger) {
        driver.quit();
        logger.log(Status.INFO, "browser closed successfully.");
    }

    public void waitInSeconds(long sec) throws InterruptedException {
        Thread.sleep((sec * (long) 1000));
    }

    public void waitInMilliSeconds(long sec) throws InterruptedException {
        Thread.sleep(sec);
    }

    public void gotoSpecificWindow(WebDriver driver, String windowName, ExtentTest logger) {
        driver.switchTo().window(windowName);
        logger.log(Status.INFO, windowName + " Opened successfully.");
    }

    public void gotoPreviousPage(WebDriver driver, ExtentTest logger) throws InterruptedException {
        driver.navigate().back();
        logger.log(Status.INFO, "Back button clicked.");
        waitInSeconds(1);
    }

    public void refreshPage(WebDriver driver, ExtentTest logger) {
        driver.navigate().refresh();
        logger.log(Status.INFO, "Page refreshed successfully.");
    }

    public void scrollToElement(WebDriver driver, By element) {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("arguments[0].scrollIntoView();", driver.findElement(element));
    }

    public void scrollToWebElement(WebDriver driver, WebElement element) {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("arguments[0].scrollIntoView();", element);
    }

    public void waitUntilElementVisible(WebDriver driver, By element) {
        WebDriverWait wait = new WebDriverWait(driver, 150, 1000);
        wait.until(ExpectedConditions.visibilityOfElementLocated(element));
    }

    public void waitUntilAtLeastOneElement(WebDriver driver, By element) {
        WebDriverWait wait = new WebDriverWait(driver, 150, 1000);
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(element, 0));
    }

    public void waitUntilAtLeastTenElement(WebDriver driver, By element) {
        WebDriverWait wait = new WebDriverWait(driver, 150, 1000);
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(element, 9));
    }

    public void waitUntilAllElementsVisible(WebDriver driver, By element) {
        WebDriverWait wait = new WebDriverWait(driver, 150, 1000);
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(element));
    }

    public void waitUntilUrlToBe(WebDriver driver, String url) {
        WebDriverWait wait = new WebDriverWait(driver, 150, 1000);
        wait.until(ExpectedConditions.urlToBe(url));
    }

    public void waitUntilUrlContains(WebDriver driver, String url) {
        WebDriverWait wait = new WebDriverWait(driver, 150, 1000);
        wait.until(ExpectedConditions.urlContains(url));
    }

    public void waitUntilElementClickable(WebDriver driver, By element) {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    public void highLightElement(WebDriver driver, WebElement we) throws InterruptedException {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;');", we);
        Thread.sleep(1000);
    }

    public void checkPageIsReady(WebDriver driver) {

        JavascriptExecutor js = (JavascriptExecutor) driver;
        String str = js.executeScript("return (window.angular !== undefined)  && (angular.element(document).injector() !== undefined) && (angular.element(document).injector().get('$http').pendingRequests.length === 0)").toString();
        if (js.executeScript("return document.readyState").toString().equals("complete")) {
            System.out.println("Page Is loaded.");
            return;
        }

        for (int i = 0; i < 300; i++) {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
            }
            //To check page ready state.
            if (js.executeScript("return return typeof(window.TestReady) !== 'undefined' && window.TestReady === true").toString().equals("complete")) {
                System.out.println("Page loaded in " + i + " Seconds");
                System.out.println(str);
                break;
            }
        }
        System.out.println(str + "********************************************************************************");
    }

    public void uploadFileWithRobot(String imagePath) {
        StringSelection stringSelection = new StringSelection(imagePath);
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipboard.setContents(stringSelection, null);

        Robot robot = null;

        try {
            robot = new Robot();
        } catch (AWTException e) {
            e.printStackTrace();
        }

        robot.delay(250);
        robot.keyPress(KeyEvent.VK_ENTER);
        robot.keyRelease(KeyEvent.VK_ENTER);
        robot.keyPress(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_ENTER);
        robot.delay(150);
        robot.keyRelease(KeyEvent.VK_ENTER);
    }

    public void takeScreenshot_Android(AndroidDriver driver, String path_screenshot) {
        try {
            File srcFile = driver.getScreenshotAs(OutputType.FILE);
            String uuid = UUID.randomUUID().toString();
            File targetFile = new File(path_screenshot + ".jpg");
            FileUtils.copyFile(srcFile, targetFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
