package others;

import com.assertthat.selenium_shutterbug.core.Shutterbug;
import com.assertthat.selenium_shutterbug.utils.web.ScrollStrategy;
import org.openqa.selenium.*;
import org.openqa.selenium.io.FileHandler;
import org.openqa.selenium.remote.Augmenter;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;

import javax.imageio.ImageIO;
import java.awt.Rectangle;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ScreenshotUtility {
    private static int scrollTimeout = 0;

    public ScreenshotUtility(int timeout) {
        scrollTimeout = timeout;
    }

    private static String getFullHeight(WebDriver driver) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        return js.executeScript("return document.body.scrollHeight").toString();
    }

    private static int getFullWidth(WebDriver driver) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        return ((Long) js.executeScript("return window.innerWidth",
                new Object[0])).intValue();
    }

    private static int getWindowHeight(WebDriver driver) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        return ((Long) js.executeScript("return window.innerHeight",
                new Object[0])).intValue();
    }

    private static void waitForScrolling() {
        try {
            Thread.sleep(scrollTimeout);
        } catch (InterruptedException ignored) {
        }
    }

    private static BufferedImage getScreenshotNative(WebDriver wd) {
        ByteArrayInputStream imageArrayStream = null;
        TakesScreenshot takesScreenshot = (TakesScreenshot) new Augmenter().augment(wd);
        try {
            imageArrayStream = new ByteArrayInputStream(takesScreenshot.getScreenshotAs(OutputType.BYTES));
            return ImageIO.read(imageArrayStream);
        } catch (IOException e) {
            throw new RuntimeException("Can not parse screenshot data", e);
        } finally {
            try {
                if (imageArrayStream != null) {
                    imageArrayStream.close();
                }
            } catch (IOException ignored) {
            }
        }
    }

    public static BufferedImage getScreenshot(WebDriver wd) {
        JavascriptExecutor js = (JavascriptExecutor) wd;
        int allH = Integer.parseInt(getFullHeight(wd));
        int allW = getFullWidth(wd);
        int winH = getWindowHeight(wd);
        int scrollTimes = allH / winH;
        int tail = allH - winH * scrollTimes;
        BufferedImage finalImage = new BufferedImage(allW, allH, BufferedImage.TYPE_4BYTE_ABGR);
        Graphics2D graphics = finalImage.createGraphics();
        for (int n = 0; n < scrollTimes ;n++){
            js.executeScript("scrollTo(0, arguments[0])", winH * n);
            waitForScrolling();
            BufferedImage part = getScreenshotNative(wd);
            graphics.drawImage(part, 0, n * winH, null);
        }
        if ( tail > 0){
            js.executeScript("scrollTo(0, document.body.scrollHeight)");
            waitForScrolling();
            BufferedImage last = getScreenshotNative(wd);
            BufferedImage tailImage = last.getSubimage(0, last.getHeight() - tail, last.getWidth(), tail);
            graphics.drawImage(tailImage, 0, scrollTimes * winH, null);
        }
        graphics.dispose();
        return finalImage;
    }

    public static void takeScreenShotOfFullPage(WebDriver wd, String filename) {
        try {
            ImageIO.write(getScreenshot(wd), "PNG", new File(filename));
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    public static void takeScreenShot(WebDriver driver, String storeImgFilePath, String fileFormat, String imgSize) throws InterruptedException {
        Thread.sleep(3000);
        Screenshot screenshot = null;
        try {
            if (imgSize.equals("ScreenSize")) {
                screenshot = new AShot().takeScreenshot(driver); //driver.findElement(By.id("myVideo"))
            } else if (imgSize.equals("FullPage")) {
                screenshot = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(1000)).takeScreenshot(driver);
            }
            ImageIO.write(screenshot.getImage(), fileFormat, new File(storeImgFilePath));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void takeScreenShotOfSpecificElement(WebDriver driver, String storeImgFilePath, String fileFormat, WebElement wElement) throws InterruptedException {
        Thread.sleep(3000);
        try {
            Screenshot screenshot = new AShot().takeScreenshot(driver, wElement);
            ImageIO.write(screenshot.getImage(), fileFormat, new File(storeImgFilePath));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void takeScreenShot2(WebDriver driver, String storeImgFilePath) throws IOException, AWTException, InterruptedException {
        Thread.sleep(3000);
        File src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE.FILE);
        try {
            FileHandler.copy(src, new File(storeImgFilePath));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void takeScreenShot3(String storeImgFilePath) throws InterruptedException {
        Thread.sleep(3000);
        try {
            BufferedImage image = new Robot().createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
            ImageIO.write(image, "png", new File(storeImgFilePath));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (AWTException e) {
            e.printStackTrace();
        }
    }

    public static void takeScreenShot4(WebDriver driver, String fileName) throws IOException, AWTException, InterruptedException {
        Thread.sleep(3000);
        Shutterbug.shootPage(driver, ScrollStrategy.BOTH_DIRECTIONS,500,true).withName(fileName).save();
    }

    public static String getDateInSimpleFormat(){
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyyhhmmssz");
        String dateString = sdf.format(date);
        return dateString;
    }

    public static void main(String argc[]) throws InterruptedException {

        /*System.setProperty("webdriver.chrome.driver", "E:\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();

        driver.manage().window().maximize();
        String baseUrl = "https://test.manage.masterdojo.io/pages/login";
        driver.navigate().to(baseUrl);
        Thread.sleep(3000);
        //   ************************* Login *************************************
        driver.findElement(By.id("inputEmail")).sendKeys("mary@gmail.com");
        driver.findElement(By.id("inputPass")).sendKeys("1234567890");
        driver.findElement(By.xpath("//*[@type='button' and text()='Login']")).click();
        Thread.sleep(3000);
        driver.findElement(By.className("ft-inbox")).click();
        Thread.sleep(5000);
        //driver.get("https://test.manage.masterdojo.io/offering/display");
        Screenshots.EShot(driver, "E:\\" + "test.png");
        driver.quit();*/
    }
}

